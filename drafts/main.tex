\documentclass[journal,a4paper]{IEEEtran}%,draftcls,onecolumn
\usepackage[utf8]{inputenc}

\usepackage{eurosym}

\usepackage[backend=biber]{biblatex}
\addbibresource{../refs.bib}

\usepackage[]{hyperref}
\hypersetup{colorlinks=true}

\usepackage{pgfplots}
\pgfplotsset{compat=1.13}

\usepackage{graphicx}
% \usepackage{tikz}
% \usetikzlibrary{positioning}

\usepackage{subfigure}

\usepackage{float}

\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{nicefrac}

\newtheorem{definition}{Définition}
\newtheorem{theorem}{Théorème}
\newtheorem{problem}{Problème}
\newtheorem{protocol}{Protocole}

\DeclareMathOperator{\jsd}{JSD}
\DeclareMathOperator{\jsdm}{JSD_m}
\DeclareMathOperator{\dkl}{D_{KL}}
\DeclareMathOperator{\lap}{Lap}
\DeclareMathOperator{\enc}{Enc}

\title{Task Assignment in Crowdsourcing under Differential Privacy}
\author{Louis~Béziaud,~Tristan~Allard, and~David~Gross-Amblard}


\begin{document}

\maketitle

\begin{abstract}
\end{abstract}

\section{Introduction}

Le \textit{crowdsourcing} est la distribution de tâches à un large groupe de personnes dans le but de résoudre un problème de façon distribuée. Cette pratique s'est récemment largement développé avec des services comme Amazon Mechanical Turk\footnote{https://www.mturk.com} regroupant plus de 500000 travailleurs, ou Microworkers\footnote{https://microworkers.com/} avec 795000 travailleurs.

Les tâches partagées peuvent nécessiter des compétences particulières de la part des participants qui doivent donc fournir à la plate-forme l'ensemble de leurs compétences. Des données peut être les horaires de disponibilité, la localisation, ou les compétences du travailleur. Il s'agit de quasi-identifiants et elles représentent donc une intrusion dans la vie privée des participants comme de celle des tâches. Ce facteur prend de plus en plus d'importance pour les utilisateurs et peut limiter le développement de ce type de service~\cite{doi:10.1509/jppm.19.1.62.16949}.

La \textit{differential privacy}~\cite{Dwork2006} est une formalisation du concept de confidentialité qui vise à minimiser les risques d'identification des entités contenues dans une requête tout en maximisant la pertinence des résultats. Cette notion est largement adoptée\footnote{``differential privacy'' renvoie 2860000 résultats sur Google Scholar (15 juin 2016)} comme une référence aujourd'hui et est mise en pratique par de nombreux services comme Apple~\cite{WWDC16} ou Google~\cite{42852} pour leur gestion des données des utilisateurs.

On présente ici une approche permettant d'accomplir le processus d'assignation en respectant la vie privée des participants comme celle des tâches.

Notre méthode utilise la \textit{differential privacy} puisque \cite{VanDijk:2010:ICA:1924931.1924934}

\section{Travaux connexes}

Une méthode permettant l'assignation de tâches sans fuites de données est présentée par \citeauthor{Kajino2016}~\cite{Kajino2016}. Elle repose sur un système distribué au sein d'un cryptosystème de Pallier utilisant un chiffrement homomorphe additif permettant d'exécuter un algorithme de maximisation de flot. Une présentation détaillée de la méthode est faite en annexe~\ref{appendix:kajino_descr}. Le chiffrement de l'intégralité des échanges rend cependant le processus très long et non-applicable. Ce point est détaillé en annexe~\ref{appendix:kajino_time}

Bien que le problème de l'assignation respectueuse de la vie privée n'ait pas été abordé, de nombreux travaux traitent des sujets connexes.

Dans le cadre particulier du \textit{spatial crowdsourcing} ou du \textit{mobile crowdsourcing}, les participants partagent leur localisation à la plate-forme. La protection de cette information est étudiée par de nombreux articles, en particulier \citeauthor{ToGhinitaShahabi2014}~\cite{ToGhinitaShahabi2014} qui utilise la \textit{differential privacy}.

Quelques travaux s'intéressent au problème de marriage, un cas particulier de celui de l'assignation : \citeauthor{HsuHuangRothEtAl2014}~\cite{HsuHuangRothEtAl2014}, \citeauthor{DBLP:journals/corr/KannanMRW14}~\cite{DBLP:journals/corr/KannanMRW14}.

Le problème s'apparente aussi à celui du \textit{private record matching} dans lequel on cherche à identifier les éléments communs à deux bases de données de façon privée. \citeauthor{InanKantarciogluGhinitaEtAl2010}~\cite{InanKantarciogluGhinitaEtAl2010} utilise la \textit{differential privacy}.

Systèmes de recommandations !

\section{Définitions \& modèle}

\subsection{Confidentialité}

La \textit{differential privacy}~\cite{Dwork2006} (``intimité différentielle'' en Français) est une formalisation du concept de confidentialité qui vise à maximiser l'utilité de données tout en minimisant les risques d'identification. 

\begin{definition}[Differential privacy]
  Soit $\epsilon$ un réel positif, $\mathcal{M}$ un algorithme probabiliste prenant en entrée un jeu de données, et ${im}\mathcal{M}$ l'image de $\mathcal{M}$. L'algorithme $\mathcal{M}$ est $\epsilon$-\textit{differentially private} si pour tout jeux de données $D_1$ et $D_2$ ne différent que d'un élément, et pour tout sous-ensemble $S$ de ${im}\mathcal{M}$, \[Pr(\mathcal{M}(D_1)\in S) \leq e^\epsilon \times Pr(\mathcal{M}(D_2)\in S).\]
\end{definition}

\subsection{Cryptosystème}

\begin{definition}[Chiffrement homomorphe]
  Un système de chiffrement homomorphe est un cryptosystème permettant de faire des calculs sur les données chiffrées. Formellement, si $c_1$ (respectivement $c_2$) est un chiffré de $m_1$ (respectivement $m_2$) il existe deux opérations $\diamond$ et $\circ$ telles que \[Dec(c_1 \diamond c2) = Dec(c_1) \circ Dec(c_2) = m_1 \circ m_2 .\]
  Typiquement, $\circ$ sera une addition ou une multiplication modulaire, mais ce n’est pas toujours le cas. On utilisera ici un chiffrement homomorphe additif, c'est à dire où $\diamond$ est une addition.
\end{definition}

\subsection{Affectation}

Le problème d'affectation consiste à distribuer des tâches à des agents. Chaque agent peut réaliser une unique tâche et chaque tâche doit être réalisée par un unique agent. Les affectations ont un coût. Le but est de minimiser le coût total des affectations en réalisant toutes les tâches.

\begin{problem}[Problème d'affectation]
  Soient deux ensembles, $A$ et $T$, de même cardinal, ainsi qu'une fonction poids $c\colon A\times T\to\mathbb{R}$. Le problème d'affectation consiste à trouver une bijection $f:A\to T$ telle que la fonction objectif $\sum_{a\in A}c(a, f(a))$ est minimisée.
\end{problem}

\section{Méthode D}

Une première méthode d'affectation naïve se base sur une perturbation des données d'entrée : les vecteurs de compétences.

On considère que les participants $p_i \in P$ (resp. les tâches $t_j \in T$) sont associés à un vecteur de compétences $s_i \in \{0, 1\}^D$ (resp. $r_j$). Chaque élément du vecteur indique si la compétence est possédée (resp. exigée) par le participant (resp. par la tâche). Pour les compétences $\langle Java, Jardinage, Anglais \rangle$, le vecteur $s_i = \langle 1, 0, 1\rangle$ indique que les compétences $Java$ et $Anglais$ sont possédées par le participant $p_i$.

On mesurera la distance entre un participant et une tâche par la fonction $d\colon \{0, 1\}^D\times \{0, 1\}^D\to [0,1]$ telle que $d(s, r) = \nicefrac{1}{D} \sum_{k=1}^D |s_k - r_k|$. Plus cette distance est faible, plus les compétences d'un participant et d'une tâche sont similaires. La qualité du processus sera évaluée par la distance moyenne des affectations.

Le procédé basique de \textit{Randomized Response}~\cite{Warner1965} permet de réponde à des questions sensibles tout en protégeant la confidentialité. Il consiste à choisir selon une probabilité $p$ si la réponse donnée à une question question dichotomique (càd. à laquelle il n'existe que deux réponses possibles) correspond à la vérité ou à une réponse fixée. Formellement, un vecteur de compétences anonymisé $s_i' = \langle s_{i1}', \dots, s_{iD}' \rangle$ sera construit à partir de $s_i = \langle s_{i1}, \dots, s_{iD} \rangle$ tel que $s_{ik}' = 1$ ou $s_{ik}' = s_{ik}$ avec probabilité $\nicefrac{1}{2}$.

\begin{figure}[H]%
  \centering
  \subfigure[$D=100$]{%
    \centering
    \label{fig:methd_n}
    \resizebox{0.45\linewidth}{!}{%
      \begin{tikzpicture}
        \begin{axis}[
            ymin=0,
            ymax=1,
            xlabel={number of participants},
            ylabel={normalized cumulative distance},
            legend pos=north east,
            legend columns=1,
            legend style={draw=none, nodes={right}},
          ]
          \addplot table [x index=0, y index=1, col sep=space] {../methodD/methodD_n.dat};
          \addlegendentry{original}
          \addplot table [x index=0, y index=2, col sep=space] {../methodD/methodD_n.dat};
          \addlegendentry{private}
        \end{axis}
      \end{tikzpicture}%
    }%
  }%
  ~
  \subfigure[$N=1000$]{%
    \centering
    \label{fig:methd_n}
    \resizebox{0.45\linewidth}{!}{%
      \begin{tikzpicture}
        \begin{axis}[
            ymin=0,
            ymax=1,
            xlabel={number of skills},
            ylabel={normalized cumulative distance},
            legend pos=north east,
            legend columns=1,
            legend style={draw=none, nodes={right}},
          ]
          \addplot table [x index=0, y index=1, col sep=space] {../methodD/methodD_d.dat};
          \addlegendentry{original}
          \addplot table [x index=0, y index=2, col sep=space] {../methodD/methodD_d.dat};
          \addlegendentry{private}
        \end{axis}
      \end{tikzpicture}%
    }%
  }%
  \caption{Qualité de l'affectation pour le procédé \textit{Randomized Response} (average of 8 runs)}
\end{figure}

\section{Méthode C}

\begin{protocol}

  On suppose que les plate-formes $\mathcal{C}$ et $\mathcal{H}$ ne sont pas en connivence. De plus, un cryptosystème a été  initialisé par la plate-forme $\mathcal{H}$ qui possède la clé secrète alors que la clé publique a été diffusée à l'ensemble des entités.

  \begin{enumerate}
  \item Chaque participant (respectivement tâche) envoie son vecteur de compétences chiffré $Enc(p_i)$ (respectivement $Enc(t_j)$) à la plate-forme $\mathcal{C}$.
  \item La plate-forme $\mathcal{C}$ calcule la matrice de coûts $C = (c_{ij})\in [0,1]^{n\times n}$ où $c_{ij} = {Enc(d(p_i, t_j))} = {\sum_{k = 0}^D |Enc(p_{ik}) - Enc(t_{jk})|}$.
  \item La plate-forme $\mathcal{C}$ perturbe $C$ avec un bruit issue d'une distribution de Laplace de position 0 et d'échelle $\lambda$ pour obtenir une matrice $C'$.
  \item La plate-forme $\mathcal{C}$ envoie $C'$ à la plate-forme $\mathcal{H}$.
  \item La plate-forme $\mathcal{H}$ déchiffre $C'$.
  \item La plate-forme $\mathcal{H}$ applique l'algorithme Hongrois à $C'$ pour obtenir des affectations.
  \item La plate-forme $\mathcal{H}$ envoie à chaque paire $(i,j)\in P\times T$ une notification l'informant de son partenaire.
  \end{enumerate}
\end{protocol}

\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{schema.png}
  \label{fig:schema}
  \caption{Schéma global du protocole. (à remplacer par du tikz)}
\end{figure}

\section{Méthode A}

% Les participants sont répartis en groupes de taille $k$ auxquels un histogramme de la répartition des compétences est associé. Cet ensemble d'histogrammes est partitionné selon leur similarité pour obtenir un ensemble de groupes de compétences $\mathcal{C}_P$. Le même procédé est appliqué aux tâches pour obtenir $\mathcal{C}_T$. Un couplage parfait $\mathcal{A}_\mathcal{C} = \{(C_{P_i}, C_{T_j})\}^n$ est ensuite déterminé entre $\mathcal{C}_P$ et $\mathcal{C}_T$. Pour chaque affectation $(C_{P_i}, C_{T_j})$ on procède à ???

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\linewidth]{unnamed0.png}
  \caption{Organigramme de la méthode A}
  \label{fig:methoda}
\end{figure}

On propose un processus d'assignation reposant sur un partitionnement des participants et des tâches en groupes de compétences.

\begin{protocol}
  \begin{enumerate}
  \item L'ensemble des participants P est partitionné en blocs $P_i$ de $k$ participants.
  \item Chaque participant diffuse son vecteur de compétences chiffré $Enc(skills \in Skill^D)$.
  \item Chaque partition $P_i$ est associé à l'histogramme des compétences présentes $H_i\colon Skill \to \mathbb{R}_+$ chiffré tel que $H_i(skill) = \max(0, count_{P_i}(skill) + Lap(0, \nicefrac{1}{\epsilon}))$. (ou un autre mécanisme ? Wishart ?)
  \item Un partitionnement des histogrammes est réalisé par l'algorithme de clustering DBSCAN en utilisant la mesure de Jensen-Shannon.
  \end{enumerate}
\end{protocol}

\subsection{Construction des histogrammes}

% \subsection{Construction des histogrammes}
% Chaque participant $p_i \in \mathcal{P}$ envoie son vecteur de compétences chiffré $\enc(p_{i_{skills}})$ associé à son identifiant $p_{i_{id}}$ à la plate-forme $\mathrm{PH}$. La plate-forme $\mathrm{PH}$ divise arbitrairement les participants en groupes de taille $k$ et calcule leur histogramme chiffré $\enc(H_i)$. Chaque histogramme est perturbé avec un bruit tiré d'une distribution de Laplace $Lap(0, \frac{1}{\epsilon})$. Les histogrammes chiffrés et perturbés sont envoyés -- associés à leur ensemble de participants respectif -- à la plate-forme $\mathrm{PD}$ qui déchiffre les histogrammes avant de les envoyer -- associés à leur ensemble de participants respectif -- à la plate-forme $\mathrm{PC}$.

Le \textit{decryptor} initialise un cryptosystème homomorphe additif et diffuse la clé publique aux participants. Les participants chiffrent leur vecteur de compétences et l'envoient au \textit{counter}. Le \textit{counter} créé des groupes arbitraires de $k$ vecteurs de compétences chiffrés. Le \textit{counter} calcule l'histogramme chiffré associé à chaque groupe. Chaque histogramme chiffré est perturbé avec un bruit tiré d'une distribution de Laplace de position 0 et d'échelle $1/\epsilon$. Les partitions de l'histogramme sont disjointes donc l'ajout ou suppression d'un unique élément ne modifiera qu'une seule partition : la sensibilité des \textit{histogram queries} est donc de 1 et le bruit appliqué $\lap(0,\nicefrac{1}{\epsilon})$ est suffisant. Les histogrammes chiffrés et perturbés sont envoyés au \textit{decryptor} qui les déchiffre avant de les envoyer au \textit{partitioner}.

\subsection{Construction des partitions}

Le \textit{partitioner} applique l'algorithme de clustering DBSCAN aux histogrammes pour créer des groupes de compétences. La distance de Jensen–Shannon est utilisée pour comparer les histogrammes. Les partitions résultantes sont envoyées à \textit{hungarian}.

Le codomaine d'une distribution appartient à $\mathbb{R}_+$ ($\dkl$ n'est pas défini autrement) et il faut donc ramener les histogrammes dans $[0, +\infty[$.

    \begin{definition}[Divergence de Kullback–Leibler]
      C'est une mesure standard, en théorie de l'information, de la dissimilarité entre deux distributions de probabilité.
      \[\dkl(P \| Q) = \sum_{i} P(i) \log\frac{P(i)}{Q(i)}.\]
    \end{definition}

    \begin{definition}[Divergence de Jensen–Shannon]
      Elle mesure l'inefficacité moyenne à remplacer une distribution par une autre.
      \[\jsd(P \| Q) = \frac{1}{2} \dkl(P \| M) + \frac{1}{2} \dkl(Q \| M)\] où $M = \frac{1}{2}(P + Q)$.
    \end{definition}

    \begin{definition}[Distance de Jensen–Shannon]
      \[\jsdm(P \| Q) = \sqrt{\jsd(P \| Q)}.\]
    \end{definition}

    \subsection{Construction des affectations de partitions}

    % \subsection{Construction des partitions}
    % L'algorithme de partitionnement DBSCAN est appliqué à l'ensemble des histogrammes par $\mathrm{PC}$. Il utilise la distance de Jensen–Shannon pour comparer les histogrammes. La plate-forme $\mathrm{PC}$ produit un ensemble de partitions $C_i$ contenant des histogrammes. Chaque histogramme étant associé à son ensemble de participants, les partitions sont considérés comme des ensembles de participants de compétences proches.

    \textit{Hungarian} utilise l'algorithme Hongrois pour déterminer un couplage parfait minimisant le coût d'affectation d'un groupe de tâches à un groupe de participants selon une distance donnée.

    \subsection{Constructions des affectations de tâches}

    Pour chaque affectation d'un groupe de tâches à un groupe de participants, on peut procéder de différentes façons afin d'affecter chaque tâche à un participant.

    L'affectation peut se faire de façon aléatoire. L'appartenance à un groupe de compétence apporte en effet une garantie sur la probabilité que le participant possède les compétences nécessaires à la tâches.

    On peut utiliser la méthode proposée par Kajino. Les dimensions sont en effet réduites dans le groupe.

    La méthode de \textit{pull} peut être utilisée pour laisser les participants choisir leur tâche.

    L'algorithme Hongrois à partir d'une distance perturbée peut être efficace, la perturbation étant réduite par rapport au groupe original.

    On peut aussi appliquer la méthode décrite ici récursivement. Il faudra alors poser une condition d'arrêt qui pourra être l'impossibilité de diviser un groupe.

    % \begin{tikzpicture}
    % \node[inner sep=0pt] (A) {\includegraphics[width=.25\textwidth]{a.png}};
    % \node[inner sep=0pt, below left=1cm and -1cm of A] (B) {\includegraphics[width=.25\textwidth]{b.png}};
    % \node[inner sep=0pt, below right=1cm and -1cm of A] (C) {\includegraphics[width=.25\textwidth]{c.png}};
    % \draw[->] (A.south) -- (B.north);
    % \draw[->] (A.south) -- (C.north);
    % \end{tikzpicture}

    \section{Méthode B}

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.9\linewidth]{unnamed2.png}
      \caption{Organigramme de la méthode B}
      \label{fig:methodb}
    \end{figure}

    Cette méthode est similaire à la méthode A mais utilise une taxonomie de compétences pour créer les partitions.

    Le \textit{decryptor} initialise un cryptosystème homomorphe additif et diffuse la clé publique aux participants ainsi qu'au \textit{partitioner}.

    Le \textit{partitioner} découpe la taxonomie en sous-arbres de façon arbitraire. Chaque noeud du sous arbre est associé à une valeur chiffrée, initialisée à 0. Les sous-arbres chiffrés sont envoyés aux participants.

    Les participants choisissent parmi les sous-arbres celui qui est le plus proche de leurs compétences et ajoutent à chaque noeud sa compétence chiffrée. Les autres sous-arbres sont re-chiffrés. L'ensemble des sous-arbres est envoyé au \textit{partitioner} qui met à jour ses sous-arbres généraux.

    Une fois que l'ensemble des participants ont participé au processus, les sous-arbres sont envoyés au \textit{decryptor}. Le \textit{decryptor} déchiffre les sous-arbres pour obtenir des groupes de compétences. Si ces groupes ne sont pas satisfaisant, le \textit{decryptor} propose un nouveau découpage au \textit{partitioner} et le processus de \textit{clustering} est relancé. Si le découpage est bon, les groupes de compétences sont envoyés à \textit{hungarian} qui applique l'algorithme Hongrois afin d'affecter chaque groupe de tâches à un groupe de participants.

    \printbibliography

    \begin{appendices}

      \section{Kajino}

      \subsection{Description}
      \label{appendix:kajino_descr}

      \subsection{Time}
      \label{appendix:kajino_time}

      \section{$\mathcal{DDP}$~\cite{Guerraoui:2015:DPD:2757807.2757811}}

      L'idée est de créer des profils anonymes -- appelés AlterEgo -- qui seront utilisés pour construire les recommandations. Chaque élément noté par l'utilisateur est remplacé selon une probabilité $p$ par un item aléatoire et $p-1$ par un item proche selon une distance $\Gamma < \lambda$.

      Cette méthode, pensée pour les systèmes de recommandations par filtrage collaboratif, peut être adaptée à notre problème. On utilise la notion de distance entre deux éléments proposée par \cite{MavridisGross-AmblardMiklos2016}.

    \end{appendices}

\end{document}
