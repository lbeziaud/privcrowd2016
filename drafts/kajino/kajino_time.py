import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpmath import mpf, plot, splot, log, ceil, sqrt


def t(d=mpf(100), w=mpf(100), t=mpf(100), l=mpf(100), k=10, par=False):
    t_keygen = mpf(45.2)
    t_enc = mpf(10.14)
    t_dec = mpf(9.63)
    t_add = mpf(0.01)
    t_sub = mpf(0.52)
    t_eqtest = mpf(19.98)
    t_ineq = (l - 1) * t_eqtest
    if par:
        t_cond = t_ineq / k
    else:
        t_cond = 4 * t_enc + t_ineq
    t_min = t_cond + t_sub
    t_init = t_keygen + (d + 1) * (w + t) * t_enc
    v = w + t + 2
    t_pnc = w * t * t_cond + (2 * v ** 2 + 2 * v) * t_enc
    t_push = t_min + 2 * t_cond + 5 * t_sub
    if par:
        t_relabel = ((w + 1) / k + 2) * t_cond + (3 / 2) * (w / k) * t_min +  ((w + 1) / k + 1) * t_sub
    else:
        t_relabel = (w + 3) * t_cond + w * t_min + (w + 2) * t_sub
    t_pta_it = (2 * w * t + w + t) * t_push + (w + t) * t_relabel
    n_pta_it = w * t * (ceil((v - 1) / 2) + 1) * (v + 2) + \
        w * (2 * v + 1) + t * (2 * v + 3)
    # n_pta_it = sqrt(v) * log(v)
    t_pta = t_pta_it * n_pta_it

    # return t_pta + t_pnc + t_init
    return t_pta_it

ax = plt.subplot(111)
ax.set_yscale('log')
xs = range(1, 1001)
ys = [t(w=x, t=x, d=100, k=x/10, l=100, par=False) /1000/60/60 for x in xs]
ax.plot(xs, ys, label='sequential')

ys = [t(w=x, t=x, d=100, k=x/10, l=100, par=True) /1000/60/60 for x in xs]
ax.plot(xs, ys, label='parallel')

#ax.set_title('PTA #iterations')

ax.set_xlabel('#participants')
ax.set_ylabel('time (hours)')

plt.legend(loc=4)

plt.show()
