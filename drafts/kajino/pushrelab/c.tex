\documentclass[9pt]{beamer}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage[latin1]{inputenc}
\tikzstyle{every state}=[fill=blue,draw=none,text=white]
\usepackage{adjustbox}
\usetheme{Boadilla}
\usefonttheme[onlymath]{serif}
\setbeamertemplate{blocks}[rounded][shadow=false]
\usepackage{ulem}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\algnewcommand\algorithmicforeach{\textbf{foreach}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo}

\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\usepackage{caption}
\captionsetup[figure]{labelformat=empty}

\title{Crowdguard, Druid Team}
\subtitle{from Kajino, 2015}
\author{Louis B\'eziaud}
\date{June 2, 2016}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}

\begin{document}
\addtobeamertemplate{block begin}{\setlength\abovedisplayskip{0pt}}

\frame{\titlepage}

\begin{frame}
  \tableofcontents
\end{frame}

\section{Maximum Flow}

\begin{frame}{Maximum Flow}

  \begin{block}{}
    A \textbf{flow network} is a directed graph $G = (V, E)$ with a source $s \in V$, a sink $t \in V$, and a capacity function $c\colon E\to \mathbb{R}$.
  \end{block}

  \begin{block}{}
    The amount of \textbf{flow} between two vertices is described by a function $f\colon V\times V \to \mathbb{R}$. The flow function $f$ has the following properties:
    \begin{description}
    \item[Capacity] $f(v, w) \leq c(v, w)$ for all $v, w \in V$
    \item[Antisymmetry] $f(v, w) = - f(w, v)$ for all $v, w \in V$
    \item[Conservation] $\sum_{w\in V}f(v,w)=0$ for all $v \in V\setminus\{s,t\}$
    \end{description}
  \end{block}

  \begin{block}{}
    The \textbf{total flow} $|f|$ of a flow network is the amount of flow going into the sink. Formally, $|f|=\sum_{v\in V} f(v, t)$.
  \end{block}

\end{frame}

%\begin{frame}{Maximum Flow -- Algorithms}
%  \centering
%  \begin{tabular}{l|l}
%    Algorithm & Complexity \\\hline\hline
%    Ford-Fulkerson algorithm & $O(E \max|f|)$ \\\hline
%    Edmonds-Karp algorithm   & $O(VE^2)$      \\\hline
%    Dinic's algorithm        & $O(VE \log(V))$ \\\hline
%    General push-relabel     & $O(V^2E)$, $O(V^2\sqrt{E})$, $O(V^3)$ \\\hline
%    \multicolumn{2}{c}{\dots}
%  \end{tabular}
%\end{frame}

\section{Push-Relabel}

\begin{frame}{Preflow}

  \begin{block}{}
    \sout{conservation constraint} $\Longrightarrow$ non-negative constraint: ${\sum_{w\in V} f(w,v)\geq 0}\quad {\forall v\in V\setminus\{s\}}$
  \end{block}

  \begin{block}{}
    \textbf{excess} of $v\in V$ defined by $e\colon V\to\mathbb{R}$ with $e(v) = \sum_{w\in V} f(w,v)$.
  \end{block}

  \begin{block}{}
    node $v\in V\setminus \{s,t\}$ \textbf{active} \textit{iff} $e(v)>0$
  \end{block}

  \begin{block}{}
    \textbf{residual Capacity} for a preflow is defined as $r(v,w) = c(v,w) - f(v, w)$
  \end{block}

  \begin{block}{}
    \textbf{residual edges} for a preflow is defined as the set of edges with positive residual capacity, $E_r = \{(v,w) \mid r(v,w) > 0 \}$.
  \end{block}

  \begin{block}{}
    \textbf{valid labeling} $h\colon V\to \mathbb{Z}$
    \begin{itemize}
    \item $h(s)=|V|$\quad (source condition)
    \item $h(t)=0$\quad (sink condition)
    \item $h(v) \leq h(w) + 1 \quad \forall (v,w)\in E_r$
    \end{itemize}
  \end{block}

\end{frame}

%\begin{frame}{Push-Relabel -- Overview}
%  \begin{itemize}
%  \item The algorithm pushes local flow excess starting at the source, $s$, along the vertices towards the sink, $t$.
%  \item The algorithm maintains a compatible vertex labeling function, $h$, to the preflow $f$.
%  \item The labeling is used to determine where to push the flow excess.
%  \item The algorithm repeatedly performs either a push operation or a relabel operation so long as there is an active vertex in $G_f$.
%  \end{itemize}
%\end{frame}

\begin{frame}{Push-Relabel -- Push operation}
  \begin{block}{}
    The push operation is used to move flow excess from one vertex to another.
  \end{block}

  \begin{block}{}
    The transfer of excess can be performed across a vertex pair $(v,w) \in E_f$ if: \begin{itemize}
    \item $v$ is an active vertex
    \item The edge has a positive residual capacity
    \item Label distance is $h(v) = h(w) + 1$
    \end{itemize}
  \end{block}

  \begin{block}{}
    This allows the algorithm to move an excess flow, $\min(e(v), r(v,w))$, from v to w.
  \end{block}
\end{frame}

\begin{frame}{Push-Relabel -- Relabel operation}
  \begin{block}{}
    The relabel operation is used to increase the label value of a single active vertex so that excess flow can be pushed out of the active vertex.
  \end{block}

  \begin{block}{}
    The relabel operation is performed when all residual edges out of the active vertex have a positive residual capacity, $r(v, w) > 0$.
  \end{block}

  \begin{block}{}
    The relabel operation for some active vertex $v$ selects the smallest label from the vertices with positive residual edges, $r(v, w) > 0$.

    The active vertex is then assigned this smallest label value + 1 such that $h(v) = \min\{ h(w) + 1 \mid (v, w) \in E_f\}$.
  \end{block}
\end{frame}

\begin{frame}{Push-Relabel\footnote{Goldberg \& Tarjan} -- Outline}
  \begin{exampleblock}{Push}
    For an active $v\in V$ where there exists a $w\in V$ with $r(v,w)>0$ and $h(v)=h(w)+1$, we push $\min(e(v), r(v,w))$ flow along the edge $(v,w)$.
  \end{exampleblock}

  \begin{exampleblock}{Relabel}
    For an active $v\in V$ where for all other $w\in V$, $r(v,w)>0 \implies h(v)\leq h(w)$, we can relabel $h(v)=\min\{h(w)+1\mid (v,w)\in E_R\}$.
  \end{exampleblock}

  \begin{exampleblock}{}
    \begin{algorithmic}[0]
      \State $h(s)\gets n$\Comment{initialize labels}
      \ForEach{$v\in V\setminus\{s\}$} $h(v)\gets 0$
      \EndFor

      \ForEach{$(s,v)\in E$} $f(s,v)\gets c(s,v)$\Comment{initialize preflow}
      \EndFor

      \While{there exists an active vertex}\Comment{perform operations}
      \State perform an applicable operation
      \EndWhile

      \State\Return{$f$}
    \end{algorithmic}
  \end{exampleblock}

\end{frame}

\begin{frame}{Push-Relabel -- Example}

  \only<1>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label=left:{\tiny \phantom{$h=0, e=0$}}] (s) {$s$};
          \node[state,label=above:{\tiny \phantom{$h=0$}}] (a) [above right of=s] {$a$};
          \node[state,label=below:{\tiny \phantom{$h=0$}}] (b) [below right of=s] {$b$};
          \node[state,label=right:{\tiny \phantom{$h=0$, $e=0$}}] (t) [below right of=a] {$t$};
          \path (s) edge node {4} (a);
          \path (s) edge node {2} (b);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (b) edge node {1} (t);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{original graph}
    \end{figure}
  }

  \only<2>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={[red]left:{\tiny \phantom{, $e=0$}$h=2$}}] (s) {$s$};
          \node[state,label={[red]above:{\tiny $h=1$}}] (a) [above right of=s] {$a$};
          \node[state,label={[red]below:{\tiny $h=1$}}] (b) [below right of=s] {$b$};
          \node[state,label={[red]right:{\tiny $h=0$\phantom{, $e=0$}}}] (t) [below right of=a] {$t$};
          \path (s) edge node {4} (a);
          \path (s) edge node {2} (b);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (b) edge node {1} (t);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{compute distance labels}
    \end{figure}
  }

  \only<3>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=2$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, \color{red}{$e=4$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny $h=1$, \color{red}{$e=2$}}}] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$\phantom{, $e=0$}}}] (t) [below right of=a] {$t$};
          \path[red] (a) edge node {4} (s);
          \path[red] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (b) edge node {1} (t);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{saturate edges from s}
    \end{figure}
  }

  \only<4>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={[red]left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=4$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny $h=1$, {$e=2$}}}] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$\phantom{, $e=0$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (b) edge node {1} (t);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{set h(s) = n}
    \end{figure}
  }

  \only<5>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=4$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny $h=1$, {$e=2$}}},fill=red] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$\phantom{, $e=0$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (b) edge node {1} (t);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{select an active node: b}
    \end{figure}
  }

  \only<6>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=4$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny $h=1$, \color{red}{$e=1$}}},fill=red] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, \color{red}{$e=1$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path[red] (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{edge (b,t) is admissible: push(1) on edge(b,t)}
    \end{figure}
  }

  \only<7>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=4$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny {\color{red}{$h=2$}}, $e=1$}},fill=red] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, {$e=1$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path (b) edge node {3} (a);
          \path (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{b has no admissible edge: relabel(b)}
    \end{figure}
  }

  \only<8>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, \color{red}{$e=5$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny {{$h=2$}}, \color{red}{$e=0$}}},fill=red] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, {$e=1$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path[red,,transform canvas={xshift=-0.3em}] (b) edge node {2} (a);
          \path[red,transform canvas={xshift=0.3em}] (a) edge node {1} (b);
          \path (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{edge (b,a) is now admissible; push(1) on edge(b,a)}
    \end{figure}
  }

  \only<9>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=5$}}},fill=red] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny {{$h=2$}}, {$e=0$}}}] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, {$e=1$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path (a) edge node {5} (t);
          \path[transform canvas={xshift=-0.3em}] (b) edge node {2} (a);
          \path[transform canvas={xshift=0.3em}] (a) edge node {1} (b);
          \path (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{b is not active; select node a}
    \end{figure}
  }

  \only<10>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, \color{red}{$e=0$}}},fill=red] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny {{$h=2$}}, {$e=0$}}}] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, \color{red}{$e=6$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path[red] (t) edge node {5} (a);
          \path[transform canvas={xshift=-0.3em}] (b) edge node {2} (a);
          \path[transform canvas={xshift=0.3em}] (a) edge node {1} (b);
          \path (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{edge (a, t) is admissible; push(5) on edge (a,t)}
    \end{figure}
  }

  \only<11>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state,label={left:{\tiny \phantom{, $e=0$}$h=4$}}] (s) {$s$};
          \node[state,label={above:{\tiny $h=1$, {$e=0$}}}] (a) [above right of=s] {$a$};
          \node[state,label={below:{\tiny {{$h=2$}}, {$e=0$}}}] (b) [below right of=s] {$b$};
          \node[state,label={right:{\tiny $h=0$, {$e=6$}}}] (t) [below right of=a] {$t$};
          \path[] (a) edge node {4} (s);
          \path[] (b) edge node {2} (s);
          \path[] (t) edge node {5} (a);
          \path[transform canvas={xshift=-0.3em}] (b) edge node {2} (a);
          \path[transform canvas={xshift=0.3em}] (a) edge node {1} (b);
          \path (t) edge node {1} (b);
        \end{tikzpicture}
      \end{adjustbox}
      \caption{no more active nodes}
    \end{figure}
  }

\end{frame}

\section{Task assignment}

\begin{frame}{Assignment network}
  \begin{block}{}
    \begin{itemize}
    \item \textbf{task} $t_i\in\mathcal{T} \Rightarrow$ requirement vector $r_i \in \{0, 1\}^D$ and capacity $L_i \in \mathbb{Z}_+$
    \item \textbf{worker} $w_j\in\mathcal{W} \Rightarrow$ feature vector $s_j \in \{0, 1\}^D$ and capacity $M_j \in \mathbb{Z}_+$
    \end{itemize}
  \end{block}

  \begin{block}{}
    worker $w_j$ can complete task $t_i$ if $s_j \geq r_i$ holds element-wise.
  \end{block}

  \begin{block}{}
    $\forall w_j\in\mathcal{W},\ t_i\in\mathcal{L}$,
    \begin{itemize}
    \item $c(s,w_j) = M_j$
    \item $c(t_i,t) = L_i$
    \item $c(w_j,t_i) = \begin{cases}
      M_T &(s_j \geq r_i)\\
      0 &(s_j < r_i)
    \end{cases}$ where $M_T = \max(\max_{i\in[I]} L_i, \max_{j\in[J]} M_j)$
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Assignment network -- Example}

  \only<1>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state] (s) {$s$};

          \node[state,label={above:{\tiny $s_1=[1\,1\,0], M_1=5$}},fill=purple] (w1) [above right of=s] {$w_1$};
          \node[state,label={below:{\tiny $s_2=[1\,0\,1], M_2=2$}},fill=purple] (w2) [below right of=s] {$w_2$};

          \node[state,label={below:{\tiny $r_2=[0\,1\,0], L_2=1$}},fill=black!60!green] (t2) [below right of=w1] {$t_2$};
          \node[state,label={above:{\tiny $r_1=[1\,0\,0], L_1=1$}},fill=black!60!green] (t1) [above of=t2] {$t_1$};
          \node[state,label={below:{\tiny $r_3=[0\,0\,1], L_3=5$}},fill=black!60!green] (t3) [below of=t2] {$t_3$};

          \node[state] (t) [right of=t2] {$t$};
        \end{tikzpicture}
      \end{adjustbox}
    \end{figure}
  }

  \only<2>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state] (s) {$s$};

          \node[state,label={above:{\tiny $s_1=[1\,1\,0]$}},fill=purple] (w1) [above right of=s] {$w_1$};
          \node[state,label={below:{\tiny $s_2=[1\,0\,1]$}},fill=purple] (w2) [below right of=s] {$w_2$};

          \node[state,label={below:{\tiny $r_2=[0\,1\,0]$}},fill=black!60!green] (t2) [below right of=w1] {$t_2$};
          \node[state,label={above:{\tiny $r_1=[1\,0\,0]$}},fill=black!60!green] (t1) [above of=t2] {$t_1$};
          \node[state,label={below:{\tiny $r_3=[0\,0\,1]$}},fill=black!60!green] (t3) [below of=t2] {$t_3$};

          \node[state] (t) [right of=t2] {$t$};
          
          \path (s) edge node {\scriptsize $M_1=5$} (w1);
          \path (s) edge node {\scriptsize $M_2=2$} (w2);
          
          \path (t1) edge node {\scriptsize $L_1=1$} (t);
          \path (t2) edge node {\scriptsize $L_2=1$} (t);
          \path (t3) edge node {\scriptsize $L_3=5$} (t);
          
        \end{tikzpicture}
      \end{adjustbox}
    \end{figure}
  }

  \only<3>{
    \begin{figure}
      \begin{adjustbox}{max totalsize={.9\textwidth}{.7\textheight},center}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
          \node[state] (s) {$s$};

          \node[state,label={above:{\tiny $s_1=[1\,1\,0]$}},fill=purple] (w1) [above right of=s] {$w_1$};
          \node[state,label={below:{\tiny $s_2=[1\,0\,1]$}},fill=purple] (w2) [below right of=s] {$w_2$};

          \node[state,label={below:{\tiny $r_2=[0\,1\,0]$}},fill=black!60!green] (t2) [below right of=w1] {$t_2$};
          \node[state,label={above:{\tiny $r_1=[1\,0\,0]$}},fill=black!60!green] (t1) [above of=t2] {$t_1$};
          \node[state,label={below:{\tiny $r_3=[0\,0\,1]$}},fill=black!60!green] (t3) [below of=t2] {$t_3$};

          \node[state] (t) [right of=t2] {$t$};
          
          \path (s) edge node {\scriptsize $M_1=5$} (w1);
          \path (s) edge node {\scriptsize $M_2=2$} (w2);
          
          \path (t1) edge node {\scriptsize $L_1=1$} (t);
          \path (t2) edge node {\scriptsize $L_2=1$} (t);
          \path (t3) edge node {\scriptsize $L_3=5$} (t);
          
          \path (w1) edge node {\scriptsize 5} (t1);
          \path (w1) edge node {\scriptsize 5} (t2);
          \path[dashed] (w1) edge node {\scriptsize 0} (t3);
          
          \path (w2) edge node {\scriptsize 5} (t1);
          \path[dashed] (w2) edge node {\scriptsize 0} (t2);
          \path (w2) edge node {\scriptsize 5} (t3);
        \end{tikzpicture}
      \end{adjustbox}
    \end{figure}
  }

\end{frame}

\begin{frame}{Reduction}

  \begin{exampleblock}{}
    \begin{algorithmic}[0]
      \State $\mathcal{A} \gets \emptyset$
      \ForAll{$w_j \in \mathcal{W}$ and $t_i \in \mathcal{T}$}
      \If{$\smash{f_{w_j, t_i} > 0}$}
      $\smash{\mathcal{A} \gets \mathcal{A} \cup \{(w_j, t_i)\}^{f_{w_j, t_i}}}$
      \EndIf
      \EndFor
      \State \Return{$\mathcal{A}$}
    \end{algorithmic}
  \end{exampleblock}

  \begin{columns}
    \begin{column}{0.7\textwidth}
      \begin{figure}
        \begin{adjustbox}{max totalsize={.9\textwidth}{.6\textheight},center}
          \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
            \node[state] (s) {$s$};

            \node[state,fill=purple] (w1) [above right of=s] {$w_1$};
            \node[state,fill=purple] (w2) [below right of=s] {$w_2$};

            \node[state,fill=black!60!green] (t2) [below right of=w1] {$t_2$};
            \node[state,fill=black!60!green] (t1) [above of=t2] {$t_1$};
            \node[state,fill=black!60!green] (t3) [below of=t2] {$t_3$};

            \node[state] (t) [right of=t2] {$t$};
            
            \path (s) edge node {\scriptsize $2/5$} (w1);
            \path (s) edge node {\scriptsize $2/2$} (w2);
            
            \path (t1) edge node {\scriptsize $1/1$} (t);
            \path (t2) edge node {\scriptsize $1/1$} (t);
            \path (t3) edge node {\scriptsize $2/5$} (t);
            
            \path (w1) edge node {\scriptsize $1/5$} (t1);
            \path (w1) edge node {\scriptsize $1/5$} (t2);
            
            \path (w2) edge node {\scriptsize $2/5$} (t3);
          \end{tikzpicture}
        \end{adjustbox}

      \end{figure}
    \end{column}

    \begin{column}{0.3\textwidth}
      \centering
      \begin{tabular}{r|l}
        $w_i$ & $t_i$ \\\hline\hline
        $w_1$ & $t_1$ \\\hline
        $w_1$ & $t_2$ \\\hline
        $w_2$ & $t_3$ \\\hline
        $w_2$ & $t_3$ \\
      \end{tabular}
    \end{column}
  \end{columns}

\end{frame}

\section{Private Task Assignment}

\begin{frame}{Crowdsourcing model}
  \begin{center}
    \includegraphics[height=0.7\textheight]{diagram}
  \end{center}
\end{frame}

\begin{frame}{Cryptographic building blocks}
  \begin{block}{COND \& MIN \& MAX}
    \begin{align*}
      \mathrm{COND}(\mathrm{Enc}(m_1), \mathrm{Enc}(m_2), \mathrm{Enc}(c)) = \begin{cases}
        \mathrm{Enc}(m_1) & (c > 0) \\
        \mathrm{Enc}(m_2) & (c \leq 0)
      \end{cases}
    \end{align*}
    %set $\mathrm{Enc}(c) = \mathrm{Enc}(m_2 - m_1)$ for $\mathrm{MIN}$ and $\mathrm{Enc}(c) = \mathrm{Enc}(m_1 - m_2)$ for $\mathrm{MAX}$.
  \end{block}

  \begin{block}{INEQ \& EQTEST\footnotemark}
    \begin{align*}
      \mathrm{INEQ}(\mathrm{Enc}(m_1), \mathrm{Enc}(m_2)) = \begin{cases}
        1 & (m_1 = m_2) \\
        0 & (m_1 \neq m_2)
      \end{cases}
    \end{align*}
    %sends $\mathrm{Enc}(s(m_1 - m_2))$ to decryptor for $\mathrm{EQTEST}$.
  \end{block}

  \only<2>{
    \begin{alertblock}{}
      \begin{center}
        \begin{tabular}{ | c | c |}\hline
          & Time [ms]\\\hline\hline
          Key generation & 45.2\\\hline
          Encryption & 10.14\\\hline
          Decryption & 9.63\\\hline
          Addition & 0.01\\\hline
          Substraction & 0.52\\\hline
        \end{tabular}
        \quad
        \begin{tabular}{ | c | c |}\hline
          & Time [ms]\\\hline\hline
          Equality test & 19.98\\\hline
          Inequality test & $19.98(L-1)$\\\hline
          Conditional test & $40.56+19.98(L-1)$\\\hline
          Min, Max & $41.08+19.98(L-1)$\\\hline
        \end{tabular}
      \end{center}
    \end{alertblock}
  }
  
  \footnotetext{Jakobsson and Schnorr, 1999; Lipmaa 2003}
\end{frame}

\begin{frame}{PTA protocol}
  \begin{block}{}
    \begin{enumerate}
    \item The platform (= decryptor) initialize the cryptosystem and recruits an operator and a mixer using crowdsourcing.
    \item All the parties construct the encrypted network assignment.
    \item The operator, the mixer and the platform run a private push-relabel protocol to obtain a maximum flow, and the platform extracts a maximum task assignment.
    \end{enumerate}
  \end{block}

  \only<2>{
    \begin{figure}
      \centering
      \includegraphics[height=0.5\textheight]{time_it}
      \includegraphics[height=0.5\textheight]{num_it}
      \caption{$D=100$, $L=100$, $k=10\%$}
    \end{figure}
  }
\end{frame}

\end{document}
