#  Privacy-preserving crowdsourcing platform

Louis Béziaud supervised by Tristan Allard and David Gross-Amblard. 2-months internship at IRISA Rennes, DRUID team, 2016

see [report](report/privcrowd_report.pdf) and [slides](slides/privcrowd_slides.pdf)

A [version of this work](https://gitlab.com/lbeziaud/crowdguard-rndresp) was published at the 28th international conference on Database and Expert Systems Applications (DEXA 2017).

## Abstract

Crowdsourcing is a technique that engages individuals in the act of completing outsourced tasks. Crowdsourcing platforms propose more and more tasks requiring specific skills. However, current task assignment solutions requires that the participant discloses its skills to untrustworthy entities. In this paper, we introduce a framework to compute task assignments in a privacy-preserving way. We investigate multiples strategies based on the randomized response perturbation scheme to provide differential privacy.