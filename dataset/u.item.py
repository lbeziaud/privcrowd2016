from collections import Counter

d = []
with open('u.item', encoding='ISO-8859-1') as f:
  for l in f:
    d.append(l.split('|'))

items = {}
for i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i22, i23, i24  in d:
  cs = list(map(int, [i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i22, i23, i24]))
  if sum(cs) == 1:
    c = next((i for i, x in enumerate(cs) if x != 0), None)
    items[int(i1)] = c

d = []
with open('ub.test', encoding='ISO-8859-1') as f:
  for l in f:
    d.append(l.strip().split('\t'))

#with open('ub.test', encoding='ISO-8859-1') as f:
#  for l in f:
#    i1, i2, i3, i4 = l.strip().split('\t')
#    i1 = int(i1) + 943
#    d.append((i1, i2, i3, i4))

users = {}
for i1, i2, i3, i4 in d:
  if int(i2) in items:
    if int(i1) not in users:
      users[int(i1)] = {}
    if items[int(i2)] not in users[int(i1)]:
      users[int(i1)][items[int(i2)]] = 0
    users[int(i1)][items[int(i2)]] += int(i3)

aa = []
for user, ratings in users.items():
  a = []
  for item in range(19):
    print(ratings.get(item, 0), end=' ')
  print()
