*** préfixes
#+begin_src sparql
prefix wine: <http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#+end_src
*** récupérer la classe d'une prop
#+begin_src sparql
SELECT * WHERE { 
	wine:Sweet rdf:type ?type .
	?type rdfs:subClassOf wine:WineTaste .
}
#+end_src
*** récupérer les enfants d'une prop
#+begin_src sparql
SELECT * WHERE { 
	?type a wine:WineSugar .
}
#+end_src
*** poser une question
#+begin_src sparql
SELECT * WHERE { 
	?wine wine:locatedIn ?located .
	?wine wine:hasMaker ?maker .
	?wine wine:hasSugar wine:Sweet .
	?wine wine:hasFlavor wine:Moderate .
	?wine wine:hasBody ?body .
}
#+end_src
** algo
   - In  :: SPARQL-query
   - Out :: SPARQL-query set
   - S <- map set, Q <- set
   - for each property in the query
     - S[prop] <- range(prop)
     - shuffle(S[prop])
   - for each combination of instances in S
     - q <- query where prop is the instance instead of the var/uri
     - Q <+ q
   - return Q
