import nltk
sentence = "What is the shoe's color of Louis Beziaud ?"
tokens = nltk.word_tokenize(sentence)
tagged = nltk.pos_tag(tokens)
entities = nltk.chunk.ne_chunk(tagged)
entities.draw()
