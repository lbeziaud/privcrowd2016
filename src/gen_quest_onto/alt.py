from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://dbpedia.org/sparql")

#select distinct ?l where {
#    ?s dcterms:subject ?c .
#    ?c rdfs:label ?l
#    filter langMatches(lang(?l),"en")
#} limit 10

sparql.setQuery("""
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX dcterms: <http://purl.org/dc/terms/>

select distinct ?l where {
    ?p <http://www.w3.org/2000/01/rdf-schema#domain> <http://dbpedia.org/ontology/Person> . 
    ?p rdfs:label ?l
    filter langMatches(lang(?l),"en")
} limit 100
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

for result in results["results"]["bindings"]:
    print("What is the {} of Alan Turing?".format(result['l']['value']))
