BOLD = '\033[1m'
UNDERLINE = '\033[4m'
END = '\033[0m'

from SPARQLWrapper import SPARQLWrapper, JSON
from random import shuffle

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)
sparql.setQuery("""
SELECT * WHERE {
  :Ken_Loach dc:description ?o filter langMatches(lang(?o), "EN")
}
""")
results = sparql.query().convert()
entities = [result['subjectLabel']['value'] for result in results['results']['bindings']]

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)
sparql.setQuery("""
select distinct ?propertyLabel ?rangeLabel where { 
  ?property <http://www.w3.org/2000/01/rdf-schema#domain> <http://dbpedia.org/ontology/Person> . 

  ?property rdfs:label ?propertyLabel 
  filter langMatches(lang(?propertyLabel), "EN")

  ?property rdfs:range ?range .
  ?range rdfs:label ?rangeLabel 
  filter langMatches(lang(?rangeLabel), "EN")
} limit 10
""")
results = sparql.query().convert()
properties = [(result['propertyLabel']['value'], result['rangeLabel']['value']) for result in results['results']['bindings']]

shuffle(entities)
shuffle(properties)

entities.append("Ken Loach")
properties.append("birth date")

for i, (property, entity)  in enumerate(zip(properties, entities)):
    property = property[0]
    print("{i}) What is the {bold}{prop}{end} of {bold}{entity}{end}?"
          .format(prop =property.encode('utf-8'),
                  entity =entity.encode('utf-8'),
                  bold = BOLD, end = END, i = i))
