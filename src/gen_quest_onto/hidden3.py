from rdflib import Graph

g = Graph()
g.load("wine.rdf")

result = g.query("""
prefix wine: <http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?wine ?located ?maker ?flavor ?body
WHERE { 
	?wine wine:locatedIn ?located .
	?wine wine:hasMaker ?maker .
	?wine wine:hasSugar wine:Sweet .
	?wine wine:hasFlavor wine:Moderate .
	?wine wine:hasBody ?body .
}
""")

for row in result:
    print("%s %s %s %s %s" % row)

result = g.query("""
prefix wine: <http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT *
""")
