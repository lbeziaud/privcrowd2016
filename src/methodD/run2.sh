#!/bin/bash

export SHELL=$(type -p bash)

export DATE=$(date +%Y%m%d%H%M%S)

run () {
    python3 -u main2.py >> "$DATE.$1.data"
}

export -f run

parallel run ::: {1..8}

#awk -F' ' '{a[$1]+=$4;b[$1]+=$5}END{for(i in a)for(j in a[i])for(k in a[i][j])print i" "j" "k" "a[i][j][k]/8" " b[i][j][k]/8;}' | sort -g > $DATE.data
