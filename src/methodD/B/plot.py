#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

my_data = np.genfromtxt('20160616143416.1.data', delimiter=' ')
X = my_data[:,0]
Y1 = my_data[:,1]
Y2 = my_data[:,2]

fig, ax = plt.subplots()

ax.plot(X, Y1, label='original')
ax.plot(X, Y2, label='randomized')

ax.set_xlabel('$K$')
ax.set_ylabel('$d(s,C)$')

plt.legend()

plt.tight_layout()
plt.show()
