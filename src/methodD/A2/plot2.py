#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm

my_data = np.genfromtxt('20160616163958.3.data', delimiter=' ')
X = my_data[:,0]
Y = my_data[:,1]
Z1 = my_data[:,3]
Z2 = my_data[:,4]
