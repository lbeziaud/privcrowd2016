#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm

my_data = np.genfromtxt('20160616163958.3.data', delimiter=' ')
X = my_data[:,0]
Y = my_data[:,1]
Z1 = my_data[:,3]
Z2 = my_data[:,4]

Z = (Z2 - Z1) / Z2

xi = np.linspace(X.min(),X.max(),11)
yi = np.linspace(Y.min(),Y.max(),11)
zi1 = griddata((X, Y), Z1, (xi[None,:], yi[:,None]), method='nearest')
zi2 = griddata((X, Y), Z2, (xi[None,:], yi[:,None]), method='nearest')

fig = plt.figure()

ax = fig.add_subplot(221, projection='3d')

xig, yig = np.meshgrid(xi, yi)

ax.plot_wireframe(xig, yig, zi1, linewidth=1, color='b', label='$Q_1$')
ax.plot_wireframe(xig, yig, zi2, linewidth=1, color='r', label='$Q_2$')

ax.set_xlabel('$N$')
ax.set_ylabel('$D$')
ax.set_zlabel('$Q$')
plt.legend()

#plt.show()

#fig, ax = plt.subplots()
ax = fig.add_subplot(222)

cax = ax.imshow(Z.reshape((11,11)), interpolation='none',
                extent=[X.min(),X.max(),Y.min(),Y.max()],
                aspect=11, cmap='viridis')

ax.set_xlabel('$N$')
ax.set_ylabel('$D$') # ($B=D/5$)

cbar = fig.colorbar(cax, fraction=0.05, pad=0.04, label='$\Delta Q$')
#cbar.set_clim(0, 0.5)

ax = fig.add_subplot(223)
cax = ax.imshow(Z1.reshape((11,11)), interpolation='none',
                extent=[X.min(),X.max(),Y.min(),Y.max()],
                aspect=11, cmap='viridis')
ax.set_xlabel('$N$')
ax.set_ylabel('$D$') # ($B=D/5$)
cbar = fig.colorbar(cax, fraction=0.05, pad=0.04, label='$Q_1$')
#cbar.set_clim(0.1, 0.3)

ax = fig.add_subplot(224)
cax = ax.imshow(Z2.reshape((11,11)), interpolation='none',
                extent=[X.min(),X.max(),Y.min(),Y.max()],
                aspect=11, cmap='viridis')
ax.set_xlabel('$N$')
ax.set_ylabel('$D$') # ($B=D/5$)
cbar = fig.colorbar(cax, fraction=0.05, pad=0.04, label='$Q_2$')
#cbar.set_clim(0.1, 0.3)

plt.tight_layout()
plt.show()
