#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm

fig = plt.figure()

# D=300, B=D/5
X, Y1, Y2 = np.genfromtxt('N.20160616174519.1.data', delimiter=' ', usecols=(0, 3, 4), unpack=True)
for i in range(2, 9):
    _X, _Y1, _Y2 = np.genfromtxt('N.20160616174519.{}.data'.format(i), delimiter=' ', usecols=(0, 3, 4), unpack=True)
    X = X + _X
    Y1 = Y1 + _Y2
    Y2 = Y2 + _Y2

X = X/8
Y1 = Y1/8
Y2 = Y2/8
ax = fig.add_subplot(121)
ax.plot(X, Y1, color='k', linestyle='-', marker='o', label='$A$')
ax.plot(X, Y2, color='k', linestyle='-', marker='x', label='$A\'$')
ax.set_xlabel('$N$')
ax.set_ylabel('$Q$')

# N=3000, B=D/5
ax = fig.add_subplot(122)
X, Y1, Y2 = np.genfromtxt('D.20160616174519.1.data', delimiter=' ', usecols=(1, 3, 4), unpack=True)
ax.plot(X, Y1, color='k', linestyle='-', marker='o', label='$A$')
ax.plot(X, Y2, color='k', linestyle='-', marker='x', label='$A\'$')
ax.set_xlabel('$D$')
ax.set_ylabel('$Q$')

plt.legend()
plt.tight_layout()
plt.show()
