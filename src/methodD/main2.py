#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.cluster.vq import kmeans,vq

import scipy

#for N in $(seq 1000 200 3000); do
#for D in $(seq 100 20 300); do

N = 1000
D = 100

for K in range(20,21):
    P = np.random.choice([0.0, 1.0], size=(N,D))
    P2 = np.empty((N,D))
    for i in range(N):
        for j in range(D):
            P2[i][j] = 1.0 if np.random.randint(2) else P[i][j]

    centroids,_ = kmeans(P, 1001)
    idx,_ = vq(P,centroids)
    d1 = sum([scipy.spatial.distance.euclidean(s, centroids[i]) / D for i,s in zip(idx,P)]) / len(idx)

    centroids,_ = kmeans(P2, 1001)
    idx,_ = vq(P2,centroids)
    d2 = sum([scipy.spatial.distance.euclidean(s, centroids[i]) / D for i,s in zip(idx,P)]) / len(idx)

    print("{} {} {}".format(K, d1, d2))
