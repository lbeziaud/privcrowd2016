#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm

my_data = np.genfromtxt('20160616000243.1.data', delimiter=' ')
X = my_data[:,0]
Y = my_data[:,1]
Z1 = my_data[:,3]
Z2 = my_data[:,4]

Z = (Z2 - Z1) / Z2

xi = np.linspace(X.min(),X.max(),11)
yi = np.linspace(Y.min(),Y.max(),11)
zi1 = griddata((X, Y), Z1, (xi[None,:], yi[:,None]), method='nearest')
zi2 = griddata((X, Y), Z2, (xi[None,:], yi[:,None]), method='nearest')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

xig, yig = np.meshgrid(xi, yi)

ax.plot_wireframe(xig, yig, zi1, linewidth=1, color='b', label='original')
ax.plot_wireframe(xig, yig, zi2, linewidth=1, color='r', label='randomized')

ax.set_xlabel('$N$')
ax.set_ylabel('$D$ ($B=D/5$)')
ax.set_zlabel('$Q$')
plt.legend()

plt.show()

fig, ax = plt.subplots()

cax = ax.imshow(Z.reshape((11,11)), interpolation='none',
                extent=[X.min(),X.max(),Y.min(),Y.max()],
                aspect=11, cmap='viridis')

ax.set_xlabel('$N$')
ax.set_ylabel('$D$ ($B=D/5$)')

cbar = fig.colorbar(cax, fraction=0.05, pad=0.04, label='$\Delta Q$')

plt.tight_layout()
plt.show()
