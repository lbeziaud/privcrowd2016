#!/bin/bash

export SHELL=$(type -p bash)

export DATE=$(date +%Y%m%d%H%M%S)

run () {
    for N in $(seq 1000 200 3000); do
	D=300
	B=$((D/5))
	./main $N $D $B >> "N.$DATE.$1.data"
    done
    for D in $(seq 100 20 300); do
	B=$((D/5))
	N=3000
	./main $N $D $B >> "D.$DATE.$1.data"
    done
}

export -f run

parallel run ::: {1..8}

#awk -F' ' '{a[$1]+=$4;b[$1]+=$5}END{for(i in a)for(j in a[i])for(k in a[i][j])print i" "j" "k" "a[i][j][k]/8" " b[i][j][k]/8;}' | sort -g > $DATE.data
