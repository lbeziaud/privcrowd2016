#include <vector>
#include <random>
#include <algorithm>
#include "multiarray.h"
#include "hungarian.h"

double dist(uint32_t D, std::vector<uint32_t> p, std::vector<uint32_t> t) {
  double d = 0.0;
  for (size_t i = 0; i < D; i++) {
    d += abs(p[i] - t[i]);
  }
  d /= (double)D;
  return d;
}

int main(int argc, char **argv) {
  uint32_t N, D, B;

  N = std::atoi(argv[1]);
  D = std::atoi(argv[2]);
  B = std::atoi(argv[3]);

  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> gen(0, 1);
  std::uniform_int_distribution<int> genD(0, D - 1);

  std::vector<std::vector<uint32_t> > P(N, std::vector<uint32_t>(D)),
      T(N, std::vector<uint32_t>(D)), P2(N, std::vector<uint32_t>(D)),
      T2(N, std::vector<uint32_t>(D));

  for (size_t i = 0; i < N; i++) {
    for (size_t j = 0; j < B; j++) {
      P[i][genD(rng)] = 1;
      T[i][genD(rng)] = 1;
    }
    for (size_t j = 0; j < D; j++) {
      P2[i][j] = (gen(rng)) ? (gen(rng)) : P[i][j];
      T2[i][j] = (gen(rng)) ? (gen(rng)) : T[i][j];
    }
  }

  MultiArray<double, 2> C(N, N), C2(N, N);

  for (size_t i = 0; i < N; i++) {
    for (size_t j = 0; j < N; j++) {
      C[i][j] = dist(D, P[i], T[j]);
      C2[i][j] = dist(D, P2[i], T2[j]);
    }
  }

  Hungarian H(C), H2(C2);
  uint32_t A[N], A2[N];

  H.execute(A);
  H2.execute(A2);

  double Q = 0.0, Q2 = 0.0;
  for (size_t i = 0; i < N; i++) {
    Q += dist(D, P[i], T[A[i]]);
    Q2 += dist(D, P[i], T[A2[i]]);
  }
  Q /= (double)N;
  Q2 /= (double)N;

  std::cout << N << " " << D << " " << B << " " << Q << " " << Q2 << std::endl;

  return 0;
}
