import numpy as np
from math import log, sqrt

N = 1000
D = 20
P = np.random.randint(2, size=(N,D))
k = 100
Pi = np.split(P, k)
Hi = np.array([sum(pi) for pi in Pi])
eps = 1
L = np.random.laplace(0., 1./eps, size=(k,D))
L[L < 0] = 0.0000000001
Hi = Hi + L

X = Hi

from sklearn.cluster import DBSCAN

def dlk(p, q):
    return sum([pi * log(pi/qi) for pi, qi in zip(p, q)])

def jsd(p, q):
    m = 0.5 * (p + q)
    d = 0.5 * dlk(p, m) + 0.5 * dlk(q, m)
    return sqrt(d)

metric = np.zeros((len(X), len(X)))
for i in range(len(X)):
    for j in range(len(X)):
        metric[i][j] = jsd(X[i], X[j])

print(metric)
        
db = DBSCAN(eps=0.01, min_samples=10, metric='precomputed', algorithm='brute').fit(metric)

print(db.labels_)
