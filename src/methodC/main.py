import numpy as np
from munkres import Munkres

NN = [100, 1000]
DD = [10, 100]
eps = [0.01, 0.1, 1]

def d(p,t):
    return sum([abs(pi - ti) for pi,ti in zip(p,t)]) / D

for N in NN:
    for D in DD:
        P = np.random.random_sample(size=(N,D))
        T = np.random.random_sample(size=(N,D))

        print(N, end=' ')
        print(D, end=' ')

        C = np.empty((N,N))
        for i,p in enumerate(P):
            for j,t in enumerate(T):
                C[i][j] = d(p,t)
        m = Munkres()
        A = m.compute(C)
        c = sum([d(P[i],T[j]) for i,j in A]) / N
        print(c, end=' ')

        for e in eps:
            C = C + np.random.laplace(0., float(N)/e, size=C.shape)
            m = Munkres()
            A = m.compute(C)
            c = sum([d(P[i],T[j]) for i,j in A]) / N
            print(c, end=' ')

        print()
