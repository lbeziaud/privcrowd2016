import numpy as np
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist

N = 500

@np.vectorize
def flip(i):
    return i if np.random.binomial(n=1, p=0.5) else np.random.binomial(n=1, p=0.5)

def dist(w, t):
    cwt = np.dot(w, t)
    ct = max(np.count_nonzero(t), 1)
    return (ct - cwt) / ct

for D in range(100, 300, 50):
    W1 = np.random.binomial(n=1, p=0.5, size=(N,D))
    T1 = np.random.binomial(n=1, p=0.5, size=(N,D))

    W2 = flip(W1)
    T2 = flip(T1)
    
    C1 = cdist(W1, T1, metric=lambda w, t: dist(w, t))
    C2 = cdist(W2, T2, metric=lambda w, t: dist(w, t))

    Ri1, Ci1 = linear_sum_assignment(C1)
    Q1 = C1[Ri1, Ci1].sum() / N

    Ri2, Ci2 = linear_sum_assignment(C2)
    Q2 = C1[Ri2, Ci2].sum() / N

    print(D, Q1, Q2)
