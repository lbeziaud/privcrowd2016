#!/usr/bin/python3

from math import ceil, log, floor
import matplotlib.pyplot as plt
import numpy as np

taxo_h = taxo_k = 4
taxo_n = int(ceil((pow(taxo_k, taxo_h + 1) - 1) / (taxo_k - 1)))

dgeo_p = taxo_n / float(sum(range(1, taxo_n)))

zi = np.random.geometric(p=dgeo_p*4, size=1000)
zmax = max(zi)
plt.hist(zi, facecolor='green', alpha=0.75)

zh = [z if np.random.randint(2) else np.random.randint(zmax+1) for z in zi]
plt.hist(zh, facecolor='blue', alpha=0.75)
plt.show()
