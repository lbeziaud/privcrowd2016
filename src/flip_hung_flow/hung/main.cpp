#include <limits>
#include <vector>
#include <random>
#include <time.h>
#include <stdlib.h>
#include "multiarray.h"
#include "hungarian.h"
#include "taxonomy.h"

#define k 10 // childs in the taxonomy
#define d 10 // depth of the taxonomy

int main(int argc, char **argv) {
  srand(time(NULL));

  uint32_t n = atoi(argv[1]); // number of participants
  uint32_t b = atoi(argv[2]); // number of skills

  Taxonomy taxo(k, d);
  
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> gen_b(0, 1);
  std::uniform_int_distribution<uint32_t> gen_n(1, taxo.size());
  std::uniform_real_distribution<> gen_d(0, 1);
  
  std::vector<std::vector<uint32_t> > agents(n);
  std::vector<uint32_t> tasks(n);

  for (size_t i = 0; i < n; i++) {
    tasks[i] = gen_n(rng);
    for (size_t j = 0; j < b; j++) {
      agents[i].push_back(gen_n(rng));
    }
  }

  MultiArray<double, 2> cost_matrix(n, n);
  MultiArray<double, 2> cost_matrix_p(n, n);

  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      //std::vector<uint32_t> agent(agents[i].size());
      //for (size_t s = 0; s < agents[i].size(); s++) {
      //  agent[s] = (gen_b(rng)) ? agents[i][s] : (1 + (rand() % (uint32_t)(taxo.size() - 1 + 1)));
      //}
      //uint32_t task = (gen_b(rng)) ? tasks[j] : (1 + (rand() % (uint32_t)(taxo.size() - 1 + 1)));
      //cost_matrix_p[i][j] = taxo.dist(agent, task);
      cost_matrix_p[i][j] = (gen_b(rng)) ? taxo.dist(agents[i], tasks[j]) : 0.0;//gen_d(rng);
      cost_matrix[i][j] = taxo.dist(agents[i], tasks[j]);
    }
  }

  Hungarian hung(cost_matrix);
  uint32_t match[n];
  hung.execute(match);

  Hungarian hung_p(cost_matrix_p);
  uint32_t match_p[n];
  hung_p.execute(match_p);

  double quality = 0.0;
  for (size_t i = 0; i < n; i++) {
    quality += taxo.dist(agents[i], tasks[match[i]]);
  }
  quality /= n;

  double quality_p = 0.0;
  double num_bad_p = 0.0;
  for (size_t i = 0; i < n; i++) {
    quality_p += taxo.dist(agents[i], tasks[match_p[i]]);
    if (taxo.dist(agents[i], tasks[match_p[i]]) > 0.5) num_bad_p += 1.0;
    //std::cout << taxo.dist(agents[i], tasks[match_p[i]]) << std::endl;
  }
  quality_p /= n;
  num_bad_p /= n;

  std::cout << n << "," << b << "," << quality << "," << quality_p << "," << num_bad_p << std::endl;

  return 0;
}
