#!/usr/bin/python3

import numpy as np
import networkx as nx
from networkx.algorithms.flow import preflow_push
import sys

N = int(sys.argv[1])
D = 20 #int(sys.argv[2])
C = 2 # => C = 1

p_G = nx.DiGraph()

Ws, Wc = np.random.choice([True, False], size=(N,D)), np.random.randint(low=1, high=C, size=N)
Ts, Tc = np.random.choice([True, False], size=(N,D)), np.random.randint(low=1, high=C, size=N)

M = max(max(Wc), max(Tc))

flip = np.vectorize(lambda s: s if np.random.randint(2) else np.random.randint(2))

p_Ws = Ws#flip(Ws)
p_Ts = Ts#flip(Ts)
#p_Wc = flip(Wc)
#p_Tc = flip(Tc)

#p_M = max(max(p_Wc), max(p_Tc))

for i, c in enumerate(Wc):
    p_G.add_edge('s', 'w'+str(i), capacity=c)

for i, c in enumerate(Tc):
    p_G.add_edge('t'+str(i), 't', capacity=c)

for i1, s1 in enumerate(p_Ws):
    for i2, s2 in enumerate(p_Ts):
        c = M if all(s1 >= s2) else 0
        c = M if np.random.randint(2) else np.random.randint(2)
        p_G.add_edge('w'+str(i1), 't'+str(i2), capacity=c)


_, p_flow_dict = nx.maximum_flow(p_G, 's', 't', flow_func=preflow_push)

p_num_assign = 0
num_assign = 0

p_bad_assign = 0

p_missing_skills = 0

for i1, s1 in enumerate(Ws):
    for i2, s2 in enumerate(Ts):
        p_c = p_flow_dict['w'+str(i1)]['t'+str(i2)]

        if p_c > 0:
            p_num_assign += p_c

            p_missing_skills += sum([s2 > s1 for s1, s2 in zip(s1, s2)]) / max(1, sum(s2))

            if not all(s1 >= s2):
                p_bad_assign += p_c

print(N, p_bad_assign / max(1, p_num_assign), p_missing_skills / max(1, p_num_assign))
