import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

public class Main2 {
	final static RandomGenerator rng = new MersenneTwister(123);

	public static void main(String[] args) {
		int n2 = 10;
		double p = 0.26894;

		double[][] skills = d2skills(Jester.ua);
		double[][] fskills = d2fskills(Jester.ua, n2, p);

		double[][] costs = new double[skills.length][fskills.length];
		for (int i = 0; i < skills.length; i++) {
			for (int j = 0; j < fskills.length; j++) {
				costs[i][j] = jsd(skills[i], fskills[j]);
			}
		}

		HungarianAlgorithm hung = new HungarianAlgorithm(costs);
		int[] match = hung.execute();
		double cost = 0, min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < costs.length; i++) {
			if (match[i] == -1) {
				continue;
			}
			cost += costs[i][match[i]];
			min = Math.min(min, costs[i][match[i]]);
			max = Math.max(max, costs[i][match[i]]);
		}
		cost /= costs.length;

		System.out.println(cost + " " + min + " " + max);

	}

	static double[][] d2skills(int[][] hs) {
		int n = hs.length;
		int n1 = hs[0].length;
		double[][] skills = new double[n][n1];
		for (int i = 0; i < n; i++) {
			skills[i] = d2skills(hs[i]);
		}
		return skills;
	}

	static double[][] d2fskills(int[][] hs, int n2, double p) {
		int n = hs.length;
		int n1 = hs[0].length;
		double[][] skills = new double[n][n1];
		for (int i = 0; i < n; i++) {
			skills[i] = d2fskills(hs[i], n2, p);
		}
		return skills;
	}

	static double[] d2skills(int[] h) {
		int n1 = h.length;
		double[] skills = new double[n1];
		int c = 0;
		for (int i = 0; i < n1; i++) {
			skills[i] = h[i];
			c += h[i];
		}
		for (int i = 0; i < n1; i++) {
			skills[i] /= c;
		}
		return skills;
	}

	static double[] d2fskills(int[] h, int n2, double p) {
		int n1 = h.length;
		double[] fskills = new double[n1];
		double c = 0;
		for (int i = 0; i < n1; i++) {
			double cy = 0, cn = 0;
			for (int j = 0; j < n2; j++) {
				int s = (j < h[i]) ? 1 : 0;
				s = (rng.nextDouble() > p) ? s : 1 - s;
				cy += s;
			}
			cn = (n2 - cy);
			fskills[i] = (p * cy + (1 - p) * cn - (1 - p) * cy - p * cn);
			fskills[i] /= (p * (cy + cn) - (1 - p) * (cy + cn));
			fskills[i] = (fskills[i] + 1) / 2;
			c += fskills[i];
		}
		for (int i = 0; i < n1; i++) {
			fskills[i] /= c;
		}
		return fskills;
	}

	public static double kld(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	public static double jsd(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}
}