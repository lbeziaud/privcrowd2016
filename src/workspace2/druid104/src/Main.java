import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

public class Main {
	public static void main(String[] args) {
		// double p = 0.26894;

		RandomGenerator rng = new MersenneTwister(123456789);

		List<int[]> A0 = Arrays.asList(Jester.ua);
		List<SkillsWrapper> A1 = new ArrayList<SkillsWrapper>(A0.size());
		List<FlippedSkillsWrapper> A2 = new ArrayList<FlippedSkillsWrapper>(A0.size());

		DistanceMeasure distance = new JensenShannonDistance();
		
		KMeansPlusPlusClusterer<SkillsWrapper> clustererA1 = new KMeansPlusPlusClusterer<SkillsWrapper>(10, 1000,
				distance, rng);
		KMeansPlusPlusClusterer<FlippedSkillsWrapper> clustererA2 = new KMeansPlusPlusClusterer<FlippedSkillsWrapper>(
				10, 1000, distance, rng);

		for (double p = 0; p <= 0.5; p += 0.1) {

			for (int[] s : A0) {
				A1.add(new SkillsWrapper(s));
				A2.add(new FlippedSkillsWrapper(s, 20, p));
			}

			List<CentroidCluster<SkillsWrapper>> clusterResultsA1 = clustererA1.cluster(A1);
			List<CentroidCluster<FlippedSkillsWrapper>> clusterResultsA2 = clustererA2.cluster(A2);

			double[][] cost = new double[10][10];
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					cost[i][j] = distance.compute(
							histogram2(clusterResultsA1.get(i).getPoints().stream().map(e -> e.getPoint())
									.collect(Collectors.toList())),
							histogram2(clusterResultsA2.get(i).getPoints().stream().map(e -> e.getPoint())
									.collect(Collectors.toList())));
				}
			}
			HungarianAlgorithm hungarian = new HungarianAlgorithm(cost);
			int[] assignment = hungarian.execute();

			double globcost = 0;
			for (int i = 0; i < assignment.length; i++) {
				double clustercost = 0;
				List<double[]> pts1 = clusterResultsA1.get(i).getPoints().stream().map(e -> e.getPoint())
						.collect(Collectors.toList());
				List<double[]> pts2 = clusterResultsA2.get(assignment[i]).getPoints().stream().map(e -> e.original)
						.collect(Collectors.toList());
				cost = new double[pts1.size()][pts2.size()];
				for (int i1 = 0; i1 < pts1.size(); i1++) {
					for (int i2 = 0; i2 < pts2.size(); i2++) {
						cost[i1][i2] = distance.compute(pts1.get(i1), pts2.get(i2));
					}
				}
				
				hungarian = new HungarianAlgorithm(cost);
				int[] assignment2 = hungarian.execute();
				int numclustassign = 0;
				for (int j = 0; j < assignment2.length; j++) {
					if (assignment2[j] == -1)
						continue;
					numclustassign += 1;
					clustercost += cost[j][assignment2[j]];
					//System.out.println(cost[j][assignment2[j]]);
				}
				globcost += clustercost / numclustassign;
			}
			globcost /= 10;
			System.out.println(p + " " + globcost);
		}

	}

	private static double[] histogram2(List<double[]> data) {
		double[] histogram2 = new double[data.get(0).length];
		for (int i = 0; i < data.size(); i++) {
			double[] point = data.get(i);
			for (int j = 0; j < point.length; j++) {
				histogram2[j] += point[j];
			}
		}
		double c = 0;
		for (int j = 0; j < histogram2.length; j++) {
			c += histogram2[j];
		}
		for (int j = 0; j < histogram2.length; j++) {
			histogram2[j] /= c;
		}
		return histogram2;
	}

	public static double[] histogram(List<? extends Clusterable> data) {
		double[] histogram2 = new double[data.get(0).getPoint().length];
		for (int i = 0; i < data.size(); i++) {
			double[] point = data.get(i).getPoint();
			for (int j = 0; j < point.length; j++) {
				histogram2[j] += point[j];
			}
		}
		double c = 0;
		for (int j = 0; j < histogram2.length; j++) {
			c += histogram2[j];
		}
		for (int j = 0; j < histogram2.length; j++) {
			histogram2[j] /= c;
		}
		return histogram2;
	}

}

class JensenShannonDistance implements DistanceMeasure {

	@Override
	public double compute(double[] arg0, double[] arg1) throws DimensionMismatchException {
		return jsd(arg0, arg1);
	}
	
	public static double kld(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	public static double jsd(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}
	
}

class SkillsWrapper implements Clusterable {
	private double[] points;

	public SkillsWrapper(int[] points) {
		this.points = new double[points.length];
		int c = 0;
		for (int i = 0; i < points.length; i++) {
			this.points[i] = points[i];
			c += points[i];
		}
		for (int i = 0; i < points.length; i++) {
			this.points[i] /= c;
		}
	}

	public double[] getPoint() {
		return points;
	}

}

class FlippedSkillsWrapper implements Clusterable {
	final static Random rng = new Random(123);

	private double[] points;
	public double[] original;

	public FlippedSkillsWrapper(int[] pts, int n2, double p) {
		int n1 = pts.length;
		points = new double[n1];
		original = new double[n1];
		double original_total = 0;
		for (int i = 0; i < n1; i++) {
			original[i] = pts[i];
			original_total += pts[i];
		}
		for (int i = 0; i < n1; i++) {
			original[i] /= original_total;
		}

		for (int i = 0; i < n1; i++) {
			for (int j = 0; j < n2; j++) {
				int s = (j < pts[i]) ? 1 : 0;
				s = (rng.nextDouble() > p) ? s : 1 - s;
				points[i] += s;
			}
		}
		for (int i = 0; i < n1; i++) {
			double c1 = points[i];
			double c0 = n2 - c1;

			double c = p * c1 + (1 - p) * c0 - (1 - p) * c1 - p * c0;
			c /= p * (c1 + c0) - (1 - p) * (c1 + c0);
			c = (c + 1) / 2;
			points[i] = c;
		}
		double sum = 0;
		for (int i = 0; i < n1; i++) {
			sum += points[i];
		}
		for (int i = 0; i < n1; i++) {
			points[i] /= sum;
		}

		// c0 = (n2 - c1);
		// points[i] = (p * c1 + (1 - p) * c0 - (1 - p) * c1 - p * c0);
		// points[i] /= (p * (c1 + c0) - (1 - p) * (c1 + c0));
		// points[i] = (points[i] + 1) / 2;
		// c += points[i];

		// double avg = 0;
		// for (int i = 0; i < n1; i++) {
		// avg += points[i];
		// }
		// avg /= n1;
		//
		// for (int i = 0; i < n1; i++) {
		// points[i] -= avg;
		// //c += points[i];
		// }

		// for (int i = 0; i < n1; i++) {
		// points[i] /= c;
		// }
		// System.out.println(Arrays.toString(points));
	}

	public double[] getPoint() {
		return points;
	}

}