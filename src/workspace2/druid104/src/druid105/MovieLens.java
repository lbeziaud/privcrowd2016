package druid105;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class MovieLens {

	final static int[][] movielens = new int[247753][18];

	public static void main(String[] args) throws IOException {
		LineIterator it = FileUtils.lineIterator(new File("src/druid105/movielens-latest.data"), "UTF-8");
		try {
			for (int i = 0; i < movielens.length; i++) {
				String[] line = it.nextLine().split(" ");
				for (int j = 0; j < movielens[0].length; j++) {
					movielens[i][j] = Integer.parseInt(line[j]);
				}
			}
		} finally {
			LineIterator.closeQuietly(it);
		}
		
		System.out.println(Arrays.deepToString(movielens));
	}

}
