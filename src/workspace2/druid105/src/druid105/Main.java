package druid105;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

public class Main {

	public static void main(String[] args) {
		RandomGenerator rng = new MersenneTwister(123456789);
		int n_agents = 1000;
		int n_skills = 100;
		double skill_p = 0.05;
		// double flip_p = 0.3;

		for (double flip_p : new double[] { 0.000, 0.005, 0.010, 0.015, 0.020, 0.025, 0.030, 0.035, 0.040, 0.045, 0.050,
				0.055, 0.060, 0.065, 0.070, 0.075, 0.080, 0.085, 0.090, 0.095, 0.100, 0.105, 0.110, 0.115, 0.120, 0.125,
				0.130, 0.135, 0.140, 0.145, 0.150, 0.155, 0.160, 0.165, 0.170, 0.175, 0.180, 0.185, 0.190, 0.195, 0.200,
				0.205, 0.210, 0.215, 0.220, 0.225, 0.230, 0.235, 0.240, 0.245, 0.250, 0.255, 0.260, 0.265, 0.270, 0.275,
				0.280, 0.285, 0.290, 0.295, 0.300, 0.305, 0.310, 0.315, 0.320, 0.325, 0.330, 0.335, 0.340, 0.345, 0.350,
				0.355, 0.360, 0.365, 0.370, 0.375, 0.380, 0.385, 0.390, 0.395, 0.400, 0.405, 0.410, 0.415, 0.420, 0.425,
				0.430, 0.435, 0.440, 0.445, 0.450, 0.455, 0.460, 0.465, 0.470, 0.475, 0.480, 0.485, 0.490, 0.495,
				0.500, }) {
			System.out.print(flip_p + ",");

			ArrayList<Agent> jester_dataset = jester_agents(flip_p, rng);
			jester_match(n_agents, flip_p, jester_dataset, rng);

			// bernoulli_match(n_agents, n_skills, flip_p, skill_p, rng);
		}

		// for (int n_agents : new int[] { 1000, 2000, 3000, 4000, 5000 }) {
		// System.out.print(n_agents + ",");
		// bernoulli_match(n_agents, n_skills, flip_p, skill_p, rng);
		// }
	}

	private static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	static void match(ArrayList<Agent> agents) {
		int n_agents = agents.size();

		double[][] costs12 = new double[n_agents][n_agents];
		for (int i = 0; i < n_agents; i++) {
			for (int j = 0; j < n_agents; j++) {
				Agent agent1 = agents.get(i);
				Agent agent2 = agents.get(j);
				costs12[i][j] = matching_distance(agent1.skills, agent2.f_skills);
			}
		}

		HungarianAlgorithm hungarian = new HungarianAlgorithm(costs12);
		int[] assignment = hungarian.execute();

		double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
		for (int i = 0; i < n_agents; i++) {
			double cost = costs12[i][assignment[i]];
			min_cost = Math.min(min_cost, cost);
			max_cost = Math.max(max_cost, cost);
			mean_cost += cost;
		}
		mean_cost /= n_agents;

		System.out.println(mean_cost + "," + min_cost + "," + max_cost + ",");
	}

	static void bernoulli_match(int n_agents, int n_skills, double flip_p, double skill_p, RandomGenerator rng) {
		BinomialDistribution d = new BinomialDistribution(rng, 1, skill_p);
		ArrayList<Agent> agents = new ArrayList<Agent>(n_agents);
		for (int i = 0; i < n_agents; i++) {
			boolean[] skills = new boolean[n_skills];
			for (int j = 0; j < n_skills; j++) {
				skills[j] = d.sample() == 1;
			}
			agents.add(new Agent(skills, flip_p, rng));
		}

		match(agents);
	}

	static void jester_match(int n_agents, double flip_p, ArrayList<Agent> agents, RandomGenerator rng) {
		n_agents = n_agents > 24938 ? 24938 : n_agents;

		agents = new ArrayList<Agent>(agents.subList(0, n_agents));

		match(agents);
	}

	static ArrayList<Agent> jester_agents(double flip_p, RandomGenerator rng) {
		File file = new File("jester-data-3.csv");

		int n_agents = 24938;
		int n_skills = 100;
		ArrayList<Agent> agents = new ArrayList<Agent>(n_agents);

		LineIterator it;
		try {
			it = FileUtils.lineIterator(file, "UTF-8");
			try {
				while (it.hasNext()) {
					String[] line = it.nextLine().split(" ");
					boolean[] skills = new boolean[n_skills];
					for (int i = 0; i < n_skills; i++) {
						skills[i] = Double.parseDouble(line[i]) < 99;
					}
					agents.add(new Agent(skills, flip_p, rng));
				}
			} finally {
				it.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return agents;
	}

}

class Agent {

	boolean[] skills, f_skills;

	public Agent(boolean[] _skills, double flip_p, RandomGenerator rng) {
		int n_skills = _skills.length;
		skills = _skills;
		f_skills = new boolean[n_skills];
		for (int i = 0; i < n_skills; i++) {
			f_skills[i] = rng.nextDouble() > flip_p ? true : false;
			// f_skills[i] = rng.nextDouble() > flip_p ? skills[i] : !skills[i];
			// f_skills[i] = rng.nextBoolean() ? skills[i] : rng.nextBoolean();
		}
	}

}
