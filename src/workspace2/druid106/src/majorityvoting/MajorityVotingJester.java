package majorityvoting;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;
import jester13.Jester;

public class MajorityVotingJester {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {		
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* number of skills */
		int d = Jester.jester.get(0).length;
		/* flipping probability */
		double p_flip = 0.5;
		/* number of assignments */
		int k = 3;

		/* original skills */
		boolean[][] S = new boolean[n][d];
		boolean[][] R = new boolean[n][d];

		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
			R[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
		}

		/* flipped skills */
		boolean[][] fS = new boolean[n][d];
		boolean[][] fR = new boolean[n][d];

		/* flipping */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
				fR[i][j] = rng.nextDouble() > p_flip ? R[i][j] : !R[i][j];
			}
		}

		/* computes distances */
		double[][] costs = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				costs[i][j] = matching_distance_excl(fR[j], fS[i]);
			}
		}
				
		/* computes assignment */
		int[][] kassignment = new int[n][k];
		
		for (int i = 0; i < k; i++) {
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			for (int j = 0; j < n; j++) {
				kassignment[j][i] = assignment[j];
				// remove worker
				costs[j][assignment[j]] = Double.POSITIVE_INFINITY;
			}
		}
		
		double quality = 0;
		for (int i = 0; i < n; i++) {
			double good = 0;
			double bad = 0; 
			for (int j = 0; j < k; j++) {
				if (matching_distance_excl(R[i], S[kassignment[i][j]]) > 0.5) {
					bad += 1;
				} else {
					good += 1;
				}
			}
			quality += (good > bad) ? 1 : 0;
		}
		quality /= n;
		System.out.println(quality);
		
		/* computes cost */
		/*		double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
		for (int i = 0; i < n; i++) {
			double cost = matching_distance(S[i], R[assignment[i]]);
			min_cost = Math.min(min_cost, cost);
			max_cost = Math.max(max_cost, cost);
			mean_cost += cost;
		}
		mean_cost /= n;
*/
		/* output */
		//System.out.println(mean_cost + "," + min_cost + "," + max_cost);

	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}
	
	static double matching_distance_excl(boolean[] task, boolean[] worker) {
		int n_diff = 0;
		int n_req = 0;
		for (int i = 0; i < task.length; i++) {
			n_diff += ((!worker[i]) && task[i]) ? 1 : 0;
			n_req += task[i] ? 1 : 0;
		}

		return ((double) n_diff) / ((double) n_req);
	}
}
