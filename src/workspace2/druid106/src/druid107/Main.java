package druid107;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;
import jester13.Jester;

/*
 * simple matching with post-correction
 */
public class Main {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* workers skills */
		boolean[][] S = new boolean[n][100];
		/* requesters skills */
		boolean[][] R = new boolean[n][100];
		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = Jester.jester.get(rng.nextInt(n));
			R[i] = Jester.jester.get(rng.nextInt(n));
		}
		/* flipped skills */
		boolean[][] fS = new boolean[n][100];
		boolean[][] fR = new boolean[n][100];
		/* looping p_flip */
		for (double p_flip = 0; p_flip <= 0.5; p_flip += 10.1) {
			/* flipping */
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < 100; j++) {
					fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
					fR[i][j] = rng.nextDouble() > p_flip ? R[i][j] : !R[i][j];
				}
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = matching_distance(fS[i], fR[j]);
				}
			}

			int k = 5;
			int[][] kassignment = new int[n][k];
			for (int i = 0; i < k; i++) {
				/* computes assignment */
				HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
				int[] assignment = hungarian.execute();
				for (int j = 0; j < n; j++) {
					kassignment[j][i] = assignment[i];
					// remove edge
					costs[i][assignment[i]] = Double.POSITIVE_INFINITY;
				}
			}
			int miss = 0;
			double cost = 0;
			for (int i = 0; i < n; i++) {
				double kcost = 0;
				int knum = 0;

				boolean[] task = fS[i];
				for (int j = 0; j < k; j++) {
					boolean[] worker = fR[kassignment[i][j]];

					// can do the task
					kcost += matching_distance(worker, task);
					knum += 1;
				}
			}

			//if (knum > 0) {
			//	cost += kcost / knum;
			//} else {
			//	miss += 1;
			//}
		}

		/* computes cost */
		double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
		for (int i = 0; i < n; i++) {
			// double cost = matching_distance(S[i], R[assignment[i]]);
			// min_cost = Math.min(min_cost, cost);
			// max_cost = Math.max(max_cost, cost);
			// mean_cost += cost;
		}
		//mean_cost = cost;
		mean_cost /= n;
		/* output */
		//System.out.println(p_flip + "," + mean_cost + "," + miss + "," + min_cost + "," + max_cost);
	}

	//}

	static boolean matching_geq(boolean[] skills1, boolean[] skills2) {
		// skills1 > skills2 <=> (skills2[i] => skills1[i]) forall i
		for (int i = 0; i < skills1.length; i++) {
			if (skills2[i] && (!skills1[i])) {
				return false;
			}
		}
		return true;
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

}
