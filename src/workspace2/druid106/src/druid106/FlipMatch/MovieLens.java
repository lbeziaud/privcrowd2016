package druid106.FlipMatch;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;

public class MovieLens {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 943; // <= 943
		/* workers skills */
		boolean[][] S = new boolean[n][1682];
		/* requesters skills */
		boolean[][] R = new boolean[n][1682];
		/* load movielens */
		for (int i = 0; i < n; i++) {
			List<Boolean> s = ml100k.MovieLens.ml100k.get(rng.nextInt(ml100k.MovieLens.ml100k.size())).stream()
					.flatMap(l -> l.stream()).collect(Collectors.toList());
			List<Boolean> r = ml100k.MovieLens.ml100k.get(rng.nextInt(ml100k.MovieLens.ml100k.size())).stream()
					.flatMap(l -> l.stream()).collect(Collectors.toList());
			for (int j = 0; j < s.size(); j++) {
				S[i][j] = s.get(j);
				R[i][j] = r.get(j);
			}
		}
		/* flipped skills */
		boolean[][] fS = new boolean[n][100];
		boolean[][] fR = new boolean[n][100];
		/* looping p_flip */
		for (double p_flip = 0; p_flip <= 0.5; p_flip += 0.01) {
			/* flipping */
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < 100; j++) {
					fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
					fR[i][j] = rng.nextDouble() > p_flip ? R[i][j] : !R[i][j];
				}
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = matching_distance(S[i], R[j]);
				}
			}
			/* computes assignment */
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			/* computes cost */
			double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
			for (int i = 0; i < n; i++) {
				double cost = matching_distance(S[i], R[assignment[i]]);
				min_cost = Math.min(min_cost, cost);
				max_cost = Math.max(max_cost, cost);
				mean_cost += cost;
			}
			mean_cost /= n;
			/* output */
			System.out.println(p_flip + "," + mean_cost + "," + min_cost + "," + max_cost);
		}
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

}
