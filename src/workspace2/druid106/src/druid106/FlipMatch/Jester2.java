package druid106.FlipMatch;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;

public class Jester2 {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* workers skills */
		boolean[][] S = new boolean[n][100];
		/* requesters skills */
		boolean[][] R = new boolean[n][100];
		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
			R[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
		}
		/* flipped skills */
		boolean[][] fS = new boolean[n][100];
		boolean[][] fR = new boolean[n][100];
		/* looping p_flip */
		for (double p_flip : new double[] { 0.00, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.10, 0.11,
				0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.20, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28,
				0.29, 0.30, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.40, 0.41, 0.42, 0.43, 0.44, 0.45,
				0.46, 0.47, 0.48, 0.49, 0.50, }) {
			/* flipping */
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < 100; j++) {
					fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
					fR[i][j] = rng.nextDouble() > p_flip ? R[i][j] : !R[i][j];
					// fS[i][j] = rng.nextBoolean() ? S[i][j] :
					// rng.nextBoolean();
					// fR[i][j] = rng.nextBoolean() ? R[i][j] :
					// rng.nextBoolean();
				}
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = matching_distance_plus(fS[i], fR[j]);
				}
			}
			/* computes assignment */
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			/* randomize assignment */
			// shuffleArray(assignment);
			/* computes cost */
			double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
			for (int i = 0; i < n; i++) {
				double cost = matching_distance(S[i], R[assignment[i]]);
				// System.out.println(Arrays.toString(S[i]) + " -> " +
				// Arrays.toString(R[assignment[i]]));
				min_cost = Math.min(min_cost, cost);
				max_cost = Math.max(max_cost, cost);
				mean_cost += cost;
			}
			mean_cost /= n;
			/* output */
			double epsilon = Math.log((1 - p_flip) / p_flip);
			System.out.println(epsilon * 100 + "," + mean_cost + "," + min_cost + "," + max_cost);
		}
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	static double matching_distance_plus(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i] && skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	public static void shuffleArray(int[] ar) {
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

}
