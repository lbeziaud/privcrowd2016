package druid106.FlipMatch;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;

public class Bernoulli {

	static RandomGenerator rng = new MersenneTwister(123);
	static BinomialDistribution be = new BinomialDistribution(rng, 1, 0.05);

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000;
		/* number of skills */
		int d = 100;
		/* workers skills */
		boolean[][] S = new boolean[n][d];
		/* requesters skills */
		boolean[][] R = new boolean[n][d];
		/* bernoulli */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				S[i][j] = be.sample() < 1;
				R[i][j] = be.sample() < 1;
			}
		}
		/* flipped workers skills */
		boolean[][] fS = new boolean[n][d];
		/* flipped requesters skills */
		boolean[][] fR = new boolean[n][d];
		/* looping p_flip */
		for (double p_flip = 0; p_flip <= 0.5; p_flip += 0.01) {
			/* flipping */
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < d; j++) {
					fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
					fR[i][j] = rng.nextDouble() > p_flip ? R[i][j] : !R[i][j];
				}
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = matching_distance(fS[i], fR[j]);
				}
			}
			/* computes assignment */
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			/* computes cost */
			double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
			for (int i = 0; i < n; i++) {
				double cost = matching_distance(S[i], R[assignment[i]]);
				//System.out.println(Arrays.toString(S[i]) + " " + Arrays.toString(R[assignment[i]]));
				min_cost = Math.min(min_cost, cost);
				max_cost = Math.max(max_cost, cost);
				mean_cost += cost;
			}
			mean_cost /= n;
			/* output */
			System.out.println(p_flip + "," + mean_cost + "," + min_cost + "," + max_cost);
		}
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i] && skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}
}
