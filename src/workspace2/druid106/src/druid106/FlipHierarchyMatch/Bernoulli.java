package druid106.FlipHierarchyMatch;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;

public class Bernoulli {

	static RandomGenerator rng = new MersenneTwister(123);
	static BinomialDistribution be = new BinomialDistribution(rng, 1, 0.5);

	public static void main(String[] args) {
		/* number of participants */
		int n = 10;
		/* number of skills */
		int d = 100;
		/* number of categories */
		int d2 = 19;
		/* number of children */
		int d1 = 20;
		/* hierarchical original skills */
		double[][] hS = new double[n][d2];
		/* hierarchical flipped skills */
		double[][] hR = new double[n][d2];
		/* movielens to hierarchical original skills */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d2; j++) {
				for (int k = 0; k < d1; k++) {
					hS[i][j] += be.sample();
					hR[i][j] += be.sample();
				}
			}
		}
		/* hierarchical flipped skills */
		double[][] fhS = new double[n][d2];
		double[][] fhR = new double[n][d2];
		/* looping p_flip */
		for (double p_flip = 0.2; p_flip < 0.45; p_flip += 0.1) {
			/* flipping */
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < d2; j++) {
					for (int k = 0; k < d1; k++) {
						boolean os = k < hS[i][j];
						boolean fs = rng.nextDouble() > p_flip ? os : !os;
						fhS[i][j] += fs ? 1 : 0;

						os = k < hR[i][j];
						fs = rng.nextDouble() > p_flip ? os : !os;
						fhR[i][j] += fs ? 1 : 0;
					}
					/* correction */
					/*
					 * System.out.println((fhS[i][j] / d1)); fhS[i][j] =
					 * ((fhS[i][j] / d1) - p_flip) / (1 - (2 * p_flip));
					 * fhR[i][j] = ((fhR[i][j] / d1) - p_flip) / (1 - (2 *
					 * p_flip)); System.out.println(fhR[i][j]);
					 */
					/*
					 * double fc1 = fhS[i][j]; double fc0 = d1 - fc1; double c1
					 * = p_flip * fc1 + (1 - p_flip) * fc0; double c0 = p_flip *
					 * fc0 + (1 - p_flip) * fc1; double c = (c1) / (c1+c0); //c
					 * = (c + 1) / 2; fhS[i][j] = c1;
					 * 
					 * fc1 = fhR[i][j]; fc0 = d1 - fc1; c1 = p_flip * fc1 + (1 -
					 * p_flip) * fc0; c0 = p_flip * fc0 + (1 - p_flip) * fc1; c
					 * = (c1) / (c1+c0); //c = (c + 1) / 2; fhR[i][j] = c1;
					 */
				}
			}
			/* normalization */
			for (int i = 0; i < n; i++) {
				hS[i] = histogram(hS[i]);
				hR[i] = histogram(hR[i]);
				fhS[i] = histogram(fhS[i]);
				fhR[i] = histogram(fhR[i]);
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = jensen_shannon_distance(fhS[i], fhR[j]);
				}
			}
			/* computes assignment */
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			/* computes cost */
			double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
			for (int i = 0; i < n; i++) {
				double cost = jensen_shannon_distance(hS[i], hR[assignment[i]]);
				min_cost = Math.min(min_cost, cost);
				max_cost = Math.max(max_cost, cost);
				mean_cost += cost;
			}
			mean_cost /= n;
			/* output */
			System.out.println(p_flip + "," + mean_cost + "," + min_cost + "," + max_cost);
		}
	}

	static double jensen_shannon_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kullback_leibler_distance(p, average) + kullback_leibler_distance(q, average)) / 2;
	}

	public static double kullback_leibler_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	static double[] histogram(double[] skills) {
		double total = 0;
		for (int i = 0; i < skills.length; i++) {
			total += skills[i];
		}
		for (int i = 0; i < skills.length; i++) {
			skills[i] /= total;
		}
		return skills;
	}
}
