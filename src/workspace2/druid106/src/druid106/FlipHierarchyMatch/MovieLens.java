package druid106.FlipHierarchyMatch;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;

public class MovieLens {

	static RandomGenerator rng = new MersenneTwister(123);

	public static void main(String[] args) {
		/* number of participants */
		int n = 943; // <= 943
		/* hierarchical original skills */
		double[][] hS = new double[n][19];
		/* hierarchical flipped skills */
		double[][] hR = new double[n][19];
		/* movielens to hierarchical original skills */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 19; j++) {
				for (int k = 0; k < ml100k.MovieLens.ml100k.get(i).get(j).size(); k++) {
					hS[i][j] += ml100k.MovieLens.ml100k.get(i).get(j).get(k) ? 1 : 0;
					hR[i][j] += ml100k.MovieLens.ml100k.get(i).get(j).get(k) ? 1 : 0;
				}
			}
		}
		/* hierarchical flipped skills */
		double[][] fhS = new double[n][19];
		double[][] fhR = new double[n][19];
		/* looping p_flip */
		for (double p_flip = 0; p_flip <= 0.5; p_flip += 0.1) {
			/* flipping */
			for (int i = 0; i < 943; i++) {
				for (int j = 0; j < 19; j++) {
					for (int k = 0; k < ml100k.MovieLens.ml100k.get(i).get(j).size(); k++) {
						boolean os = ml100k.MovieLens.ml100k.get(i).get(j).get(k);
						boolean fs = rng.nextDouble() > p_flip ? os : !os;
						fhS[i][j] += fs ? 1 : 0;

						os = ml100k.MovieLens.ml100k.get(i).get(j).get(k);
						fs = rng.nextDouble() > p_flip ? os : !os;
						fhR[i][j] += fs ? 1 : 0;
					}
					/* correction */
					// double c1 = hR[i][j];
					// double c0 = MovieLens.ml100k.get(i).get(j).size() - c1;
					// hR[i][j] = p_flip * c1 + (1 - p_flip) * c0 - (1 - p_flip)
					// * c1 - p_flip * c0;
					// hR[i][j] /= p_flip * (c1 + c0) - (1 - p_flip) * (c1 +
					// c0);
					// hR[i][j] = (hR[i][j] + 1) / 2;
				}
			}
			/* normalization */
			for (int i = 0; i < n; i++) {
				hS[i] = histogram(hS[i]);
				hR[i] = histogram(hR[i]);
				fhS[i] = histogram(fhS[i]);
				fhR[i] = histogram(fhR[i]);
			}
			/* computes distances */
			double[][] costs = new double[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					costs[i][j] = jensen_shannon_distance(fhS[i], fhR[j]);
				}
			}
			/* computes assignment */
			HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
			int[] assignment = hungarian.execute();
			///druid106.FlipMatch.Jester.shuffleArray(assignment);
			
			/* computes cost */
			double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
			for (int i = 0; i < n; i++) {
				double cost = jensen_shannon_distance(hS[i], hR[assignment[i]]);
				min_cost = Math.min(min_cost, cost);
				max_cost = Math.max(max_cost, cost);
				mean_cost += cost;
			}
			mean_cost /= n;
			/* output */
			System.out.println(p_flip + "," + mean_cost + "," + min_cost + "," + max_cost);
		}
	}

	static double jensen_shannon_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kullback_leibler_distance(p, average) + kullback_leibler_distance(q, average)) / 2;
	}

	public static double kullback_leibler_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	static double[] histogram(double[] skills) {
		double total = 0;
		for (int i = 0; i < skills.length; i++) {
			total += skills[i];
		}
		for (int i = 0; i < skills.length; i++) {
			skills[i] /= total;
		}
		return skills;
	}
}
