package confparam;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;
import jester13.Jester;

public class RandomThreshold {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* number of skills */
		int d = Jester.jester.get(0).length;
		/* anonymization maximum threshold */
		double delta = 0.0;

		/* original skills */
		boolean[][] S = new boolean[n][d];
		boolean[][] R = new boolean[n][d];

		/* anonymization threshold */
		double[] pS = new double[n];
		double[] pR = new double[n];

		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
			R[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
			pS[i] = Math.max(rng.nextDouble() - delta, 0);
			pR[i] = Math.max(rng.nextDouble() - delta, 0);
		}

		/* flipped skills */
		boolean[][] fS = new boolean[n][d];
		boolean[][] fR = new boolean[n][d];

		/* flipping */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 100; j++) {
				fS[i][j] = rng.nextDouble() > (pS[i] / 2) ? S[i][j] : !S[i][j];
				fR[i][j] = rng.nextDouble() > (pR[i] / 2) ? R[i][j] : !R[i][j];
			}
		}

		/* computes distances */
		double[][] costs = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				double faith = 1;// + (pS[i] + pR[i]) / 2;
				costs[i][j] = matching_distance(fS[i], fR[j]) * faith;
			}
		}

		/* computes assignment */
		HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
		int[] assignment = hungarian.execute();

		/* computes cost */
		double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
		for (int i = 0; i < n; i++) {
			double cost = matching_distance(S[i], R[assignment[i]]);
			min_cost = Math.min(min_cost, cost);
			max_cost = Math.max(max_cost, cost);
			mean_cost += cost;
		}
		mean_cost /= n;

		/* output */
		System.out.println(mean_cost + "," + min_cost + "," + max_cost);
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	static double matching_distance_plus(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i] && skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}
}
