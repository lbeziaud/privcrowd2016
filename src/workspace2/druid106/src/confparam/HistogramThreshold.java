package confparam;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import druid106.HungarianAlgorithm;
import jester13.Jester;

public class HistogramThreshold {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* number of skills */
		int d = Jester.jester.get(0).length;

		/* original skills */
		boolean[][] S = new boolean[n][d];
		boolean[][] R = new boolean[n][d];

		/* skills distributions */
		double[] hS = new double[d];
		double[] hR = new double[d];

		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
			R[i] = jester13.Jester.jester.get(rng.nextInt(jester13.Jester.jester.size()));
		}

		/* build histogram */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				hS[j] += S[i][j] ? 1 : 0;
				hR[j] += R[i][j] ? 1 : 0;
			}
		}
		hS = histogram(hS);
		hR = histogram(hR);

		/* anonymization thresold */
		double[] pS = new double[n];
		double[] pR = new double[n];

		for (int i = 0; i < n; i++) {
			pS[i] = jensen_shannon_distance(hS, histogram(S[i]));
			pS[i] = jensen_shannon_distance(hS, histogram(R[i]));
		}

		/* flipped skills */
		boolean[][] fS = new boolean[n][d];
		boolean[][] fR = new boolean[n][d];

		/* flipping */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 100; j++) {
				fS[i][j] = rng.nextDouble() > (pS[i] / 2) ? S[i][j] : !S[i][j];
				fR[i][j] = rng.nextDouble() > (pR[i] / 2) ? R[i][j] : !R[i][j];
			}
		}

		/* computes distances */
		double[][] costs = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				double faith = 1;// + (pS[i] + pR[i]) / 2;
				costs[i][j] = matching_distance(fS[i], fR[j]) * faith;
			}
		}

		/* computes assignment */
		HungarianAlgorithm hungarian = new HungarianAlgorithm(costs);
		int[] assignment = hungarian.execute();

		/* computes cost */
		double min_cost = Double.POSITIVE_INFINITY, max_cost = Double.NEGATIVE_INFINITY, mean_cost = 0;
		for (int i = 0; i < n; i++) {
			double cost = matching_distance(S[i], R[assignment[i]]);
			min_cost = Math.min(min_cost, cost);
			max_cost = Math.max(max_cost, cost);
			mean_cost += cost;
		}
		mean_cost /= n;

		/* output */
		System.out.println(mean_cost + "," + min_cost + "," + max_cost);
	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	static double jensen_shannon_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kullback_leibler_distance(p, average) + kullback_leibler_distance(q, average)) / 2;
	}

	public static double kullback_leibler_distance(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	static double[] histogram(boolean[] skills) {
		double total = 0;
		double[] histogram = new double[skills.length];
		for (int i = 0; i < skills.length; i++) {
			total += skills[i] ? 1 : 0;
		}
		for (int i = 0; i < skills.length; i++) {
			histogram[i] = (skills[i] ? 1 : 0) / total;
		}
		return histogram;
	}

	static double[] histogram(double[] skills) {
		double total = 0;
		double[] histogram = new double[skills.length];
		for (int i = 0; i < skills.length; i++) {
			total += skills[i];
		}
		for (int i = 0; i < skills.length; i++) {
			histogram[i] = skills[i] / total;
		}
		return histogram;
	}

}
