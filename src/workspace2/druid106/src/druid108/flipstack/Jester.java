package druid108.flipstack;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import com.scerra.graphs.Edge;
import com.scerra.graphs.FlowGraph;
import com.scerra.graphs.Graph;
import com.scerra.graphs.Node;

import druid106.HungarianAlgorithm;

public class Jester {

	static RandomGenerator rng = new MersenneTwister();

	public static void main(String[] args) {
		/* number of participants */
		int n = 1000; // Jester.jester.size()
		/* number of skills */
		int d = jester13.Jester.jester.get(0).length;
		/* flipping probability */
		double p_flip = 0.25;
		/* workers */
		boolean[][] S = new boolean[n][d];
		/* tasks */
		int[] R = new int[n];
		/* load jester */
		for (int i = 0; i < n; i++) {
			S[i] = jester13.Jester.jester.get(rng.nextInt(d));
			R[i] = rng.nextInt(d);
		}
		/* flipped skills */
		boolean[][] fS = new boolean[n][d];
		/* flipping */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				fS[i][j] = rng.nextDouble() > p_flip ? S[i][j] : !S[i][j];
			}
		}
		/* build stacks */
		ArrayList<ArrayList<Integer>> stacks = new ArrayList<ArrayList<Integer>>(d);
		for (int i = 0; i < d; i++) {
			stacks.add(new ArrayList<Integer>());
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				if (fS[i][j]) {
					stacks.get(j).add(i);
				}
			}
		}
		/* build network */
		Graph graph = new FlowGraph();

		Node source = new Node(0, "S");
		graph.addNode(source);
		Node sink = new Node(1, "T");
		graph.addNode(sink);

		for (int i = 0; i < n; i++) {
			Node wj = new Node(i + 2, "w" + (i + 2));
			graph.addNode(wj);
			graph.addEdge(new Edge(source, wj, 1));
		}
		
		for (int i = 0; i < n; i++) {
			Node wj = new Node(i + 2, "t" + (i + 2));
			graph.addNode(wj);
			graph.addEdge(new Edge(source, wj, 1));
		}
		

	}

	static double matching_distance(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	static double matching_distance_plus(boolean[] skills1, boolean[] skills2) {
		int n_diff = 0;
		for (int i = 0; i < skills1.length; i++) {
			n_diff += (skills1[i] != skills2[i] && skills2[i]) ? 1 : 0;
		}

		return ((double) n_diff) / skills1.length;
	}

	public static void shuffleArray(int[] ar) {
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

}
