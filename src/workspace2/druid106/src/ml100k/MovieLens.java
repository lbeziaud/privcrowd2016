package ml100k;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

public class MovieLens {

	static RandomGenerator rng = new MersenneTwister(123);
	public static ArrayList<ArrayList<ArrayList<Boolean>>> ml100k = parse();

	public static void main(String[] args) {
		for (int i = 0; i < ml100k.size(); i++) {
			int c = 0;
			for (int j = 0; j < 19; j++) {
				int c2 = 0;
				for (int k = 0; k < ml100k.get(i).get(j).size(); k++) {
					c2 += ml100k.get(i).get(j).get(k) ? 1 : 0;
				}
				c += c2;
			}
			System.out.print(c + ",");
		}
	}

	private static ArrayList<ArrayList<ArrayList<Boolean>>> parse() {
		int[][] udata = parse_data();
		ArrayList<ArrayList<Integer>> uitem = parse_item();

		ArrayList<ArrayList<ArrayList<Boolean>>> ratings = new ArrayList<ArrayList<ArrayList<Boolean>>>(943);

		for (int i = 0; i < 943; i++) {
			ArrayList<ArrayList<Boolean>> user_ratings = new ArrayList<ArrayList<Boolean>>(19);
			for (ArrayList<Integer> items : uitem) {
				ArrayList<Boolean> genre_ratings = new ArrayList<Boolean>();
				for (Integer item : items) {
					genre_ratings.add(udata[i][item - 1] > 2);
				}
				user_ratings.add(genre_ratings);
			}
			ratings.add(user_ratings);
		}

		return ratings;
	}

	private static int[][] parse_data() {
		File file = new File("src/ml100k/u.data");

		int[][] udata = new int[943][1682];
		LineIterator it;
		try {
			it = FileUtils.lineIterator(file, "UTF-8");
			try {
				while (it.hasNext()) {
					String[] line = it.nextLine().split("\t");
					int user_id = Integer.parseInt(line[0]) - 1;
					int movie_id = Integer.parseInt(line[1]) - 1;
					int rating = Integer.parseInt(line[2]);
					udata[user_id][movie_id] = rating;
				}
			} finally {
				it.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return udata;
	}

	private static ArrayList<ArrayList<Integer>> parse_item() {
		File file = new File("src/ml100k/u.item");

		ArrayList<ArrayList<Integer>> uitem = new ArrayList<ArrayList<Integer>>(19);
		for (int i = 0; i < 19; i++) {
			uitem.add(new ArrayList<Integer>());
		}

		LineIterator it;
		try {
			it = FileUtils.lineIterator(file, "UTF-8");
			try {
				while (it.hasNext()) {
					String[] line = it.nextLine().split("\\|");
					int movie_id = Integer.parseInt(line[0]);
					ArrayList<Integer> genres = new ArrayList<Integer>(19);
					for (int i = 0; i < 19; i++) {
						if (Integer.parseInt(line[i + 5]) > 0) {
							genres.add(i);
						}
					}
					int genre = 0;
					if (genres.size() > 0) {
						genre = genres.get(rng.nextInt(genres.size()));
					}
					uitem.get(genre).add(movie_id);
				}
			} finally {
				it.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return uitem;
	}
}
