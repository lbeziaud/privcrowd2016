package jester13;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class Jester {
	public static ArrayList<boolean[]> jester = jester();

	private static ArrayList<boolean[]> jester() {
		File file = new File("src/jester13/jester-data-3.csv");

		int n_agents = 24938;
		int n_skills = 100;
		ArrayList<boolean[]> jester = new ArrayList<boolean[]>(n_agents);

		LineIterator it;
		try {
			it = FileUtils.lineIterator(file, "UTF-8");
			try {
				while (it.hasNext()) {
					String[] line = it.nextLine().split(" ");
					boolean[] skills = new boolean[n_skills];
					for (int i = 0; i < n_skills; i++) {
						skills[i] = Double.parseDouble(line[i]) < 99;
					}
					jester.add(skills);
				}
			} finally {
				it.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jester;
	}

	public static void main(String[] args) {
		for (int i = 0; i < jester.size(); i++) {
			int c = 0;
			for (int j = 0; j < jester.get(0).length; j++) {
				c += jester.get(i)[j] ? 1 : 0;
			}
			System.out.print(c + ",");
		}
	}
}
