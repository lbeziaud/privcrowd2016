package druidT;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

public class Main {
	public static void main(String[] args) {
		Well19937c rng = new Well19937c();
		BinomialDistribution ps = new BinomialDistribution(rng, 1, 0.05);

		int num_lv1 = 10, num_lv2 = 10;

		for (num_lv2 = 5; num_lv2 <= 50; num_lv2 += 1) {

			double aaminus = 0, aaplus = 0;
			for (int repeat = 0; repeat < 10; repeat++) {

				int N = 1000;

				int[][] A1 = new int[N][num_lv1 * num_lv2], A2 = new int[N][num_lv1 * num_lv2];

				for (int i = 0; i < N; i++) {
					A1[i] = ps.sample(num_lv1 * num_lv2);
					for (int j = 0; j < num_lv1 * num_lv2; j++) {
						A2[i][j] = (rng.nextBoolean()) ? A1[i][j] : (rng.nextBoolean() ? 1 : 0);
					}
				}

				int[][] B1 = new int[N][num_lv1], B2 = new int[N][num_lv1];
				for (int i = 0; i < N; i++) {
					for (int j = 0; j < num_lv1 * num_lv2; j++) {
						B1[i][(int) Math.ceil((j - 1) / (num_lv2))] += A1[i][j];
						B2[i][(int) Math.ceil((j - 1) / (num_lv2))] += A2[i][j];
					}
					for (int j = 0; j < num_lv1; j++) {
						B1[i][j] = (B1[i][j] > 0) ? 1 : 0;
						// p(1s are all wrong + 0s are all true)
						B2[i][j] = (Math.pow(2, -2 * B2[i][j] + 1) < 0.5) ? 1 : 0;
						// p(1s are all wrong)
						// B2[i][j] = (Math.pow(4, -B2[i][j]) < 0.5) ? 1 : 0;
					}
				}

				double aminus = 0, aplus = 0;
				for (int i = 0; i < N; i++) {
					double plus = 0, minus = 0;
					for (int j = 0; j < num_lv1; j++) {
						if ((B1[i][j] == 1) && (B2[i][j] == 0)) {
							minus += 1;
						}
						if (B1[i][j] == 0 && B2[i][j] == 1) {
							plus += 1;
						}
					}
					aminus += minus / num_lv1;
					aplus += plus / num_lv1;
				}
				aminus /= N;
				aplus /= N;

				aaminus += aminus;
				aaplus += aplus;

			}

			aaminus /= 10;
			aaplus /= 10;

			System.out.println(num_lv2 + " " + aaminus + " " + aaplus);
		}

	}
}