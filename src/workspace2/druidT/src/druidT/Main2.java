package druidT;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;

public class Main2 {

	final static Well19937c rng = new Well19937c();
	final static BinomialDistribution ps = new BinomialDistribution(rng, 1, 0.05);

	public static void main(String[] args) {

		int N = 10;
		int num_lv1 = 10, num_lv2 = 20, D = num_lv1 * num_lv2;

		int[][] A1 = new int[N][D], A2 = new int[N][D];
		for (int i = 0; i < N; i++) {
			A1[i] = ps.sample(D);
			for (int j = 0; j < D; j++) {
				A2[i][j] = (rng.nextBoolean()) ? A1[i][j] : (rng.nextBoolean() ? 1 : 0);
			}
		}

//		double[][] B1 = new double[N][num_lv1], B2 = new double[N][num_lv1];
		int[][] B1 = new int[N][num_lv1], B2 = new int[N][num_lv1];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < num_lv1 * num_lv2; j++) {
				B1[i][(int) Math.ceil((j - 1) / (num_lv2))] += A1[i][j];
				B2[i][(int) Math.ceil((j - 1) / (num_lv2))] += A2[i][j];
			}

//			int s1 = 0, s2 = 0;
			for (int j = 0; j < num_lv1; j++) {
//				B1[i][j] = (double) B1[i][j];
//				s1 += B1[i][j];
//				B1[i][j] = (B1[i][j] > (num_lv2 / 10)) ? 1 : 0;
//				B2[i][j] = (0.75 * B2[i][j] + 0.25 * (num_lv2 - B2[i][j]));
//				B2[i][j] = (0.75 * B2[i][j] + 0.25 * (num_lv2 - B2[i][j])) > (num_lv2 / 10) ? 1 : 0;
//				s2 += B2[i][j];
			}

//			for (int j = 0; j < num_lv1; j++) {
//				B1[i][j] /= Math.max(1, s1);
//				B2[i][j] /= Math.max(1, s2);
//			}
		}

		double[][] C1 = new double[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				C1[i][j] = hamming(B1[i], B2[j]);//jsd(B1[i], B2[j]);
			}
		}

		HungarianAlgorithm H1 = new HungarianAlgorithm(C1);
		int[] M1 = H1.execute();

		double result = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < num_lv1; j++) {
				System.out.print(B1[i][j] + " ");
			}
			System.out.println();
			for (int j = 0; j < num_lv1; j++) {
				System.out.print(B2[i][j] + " ");
			}
			System.out.println();
			System.out.println();
			
			if (M1[i] == -1) {
				continue;
			}
			result += C1[i][M1[i]];
		}
		result /= N;

		System.out.println(result);

	}

	public static double hamming(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (ctf + cft) / n;
	}

	public static double kld(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	public static double jsd(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}
}
