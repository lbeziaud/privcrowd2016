package druid102;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import com.apporiented.algorithm.clustering.AverageLinkageStrategy;
import com.apporiented.algorithm.clustering.Cluster;
import com.apporiented.algorithm.clustering.ClusteringAlgorithm;
import com.apporiented.algorithm.clustering.DefaultClusteringAlgorithm;
import com.apporiented.algorithm.clustering.visualization.DendrogramPanel;

import ch.usi.inf.sape.hac.dendrogram.DendrogramNode;

public class Main2 {

	public static void main(String[] args) {
		a();
		System.out.println(".");
	}

	static void a() {
		RandomGenerator rng = new MersenneTwister(123456789);
		PoissonDistribution bin = new PoissonDistribution(4);

		int n = 1000, n1 = 10, n2 = 10;

		ArrayList<B> B = new ArrayList<B>();

		for (int i = 0; i < n; i++) {
			int[] s = new int[n1];
			for (int j = 0; j < n1; j++) {
				s[j] = (int) (100 * bin.probability(j));
			}

			B b = new B(s, n2, rng);
			B.add(b);
		}

		double[][] distances = new double[n][n];
		String[] names = new String[n];

		for (int i = 0; i < n; i++) {
			names[i] = "" + i;
			for (int j = 0; j < n; j++) {
				distances[i][j] = JensenShannon.jsd(B.get(i).v, B.get(j).v);
			}
		}

		ClusteringAlgorithm alg = new DefaultClusteringAlgorithm();
		Cluster clusterB = alg.performClustering(distances, names, new AverageLinkageStrategy());

		//clusterB.toConsole(0);
		toConsole(clusterB, 0);

	}

	public static void toConsole(Cluster cluster, int indent) {
		for (int i = 0; i < indent; i++) {
			System.out.print("  ");

		}
		System.out.println(cluster.countLeafs() + " " + cluster.getDistanceValue());
		for (Cluster child : cluster.getChildren()) {
			toConsole(child, indent + 1);
		}
	}
}

class B implements Clusterable {

	final double[] v;

	public B(int[] iv, int n2, RandomGenerator rng) {
		int n = iv.length;
		v = new double[n];
		double c = 0;
		for (int i = 0; i < n; i++) {
			int c2 = 0;
			for (int j = 0; j < n2; j++) {
				int a = j < iv[i] ? 1 : 0;
				int b = rng.nextBoolean() ? a : rng.nextInt(2);
				c2 += b;
			}
			double yes = c2 * 0.75 + (n2 - c2) * 0.25;
			double no = (n2 - c2) * 0.75 + c2 * 0.25;
			v[i] = (yes - no) / (0.75 * n2);
			v[i] = (v[i] + 1) / (double) 2;
			c += v[i];
		}
		for (int i = 0; i < n; i++) {
			v[i] /= c == 0 ? 1 : c;
		}
	}

	@Override
	public double[] getPoint() {
		return v;
	}

}

@SuppressWarnings("serial")
class JensenShannon implements DistanceMeasure {

	@Override
	public double compute(double[] a, double[] b) throws DimensionMismatchException {
		if (a.length != b.length) {
			throw new DimensionMismatchException(a.length, b.length);
		}

		return jsd(a, b);
	}

	static double kld(double[] p, double[] q) {
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	static double jsd(double[] p, double[] q) {
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}

}