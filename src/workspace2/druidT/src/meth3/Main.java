package meth3;

import java.util.ArrayList;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;
import ch.usi.inf.sape.hac.HierarchicalAgglomerativeClusterer;
import ch.usi.inf.sape.hac.agglomeration.AgglomerationMethod;
import ch.usi.inf.sape.hac.agglomeration.*;
import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import ch.usi.inf.sape.hac.dendrogram.DendrogramBuilder;
import ch.usi.inf.sape.hac.experiment.DissimilarityMeasure;
import ch.usi.inf.sape.hac.experiment.Experiment;

public class Main {
	public static void main(String[] args) {
		int N = 10000;
		int L1 = 5, L2 = 10;
		E e = new E(N, L1, L2);

		double[] A = new double[L1];
		double[] B = new double[L1];
		double[] C = new double[L1];

		for (int i = 0; i < N; i++) {
			double maxA = Double.NEGATIVE_INFINITY, maxB = Double.NEGATIVE_INFINITY, maxC = Double.NEGATIVE_INFINITY;
			double minA = Double.POSITIVE_INFINITY, minB = Double.POSITIVE_INFINITY, minC = Double.POSITIVE_INFINITY;
			for (int j = 0; j < L1; j++) {
				A[j] += e.AA[i][j];
				B[j] += e.BB[i][j];
				C[j] += e.CC[i][j];

				maxA = Math.max(maxA, e.AA[i][j]);
				maxB = Math.max(maxB, e.BB[i][j]);
				maxC = Math.max(maxC, e.CC[i][j]);

				minA = Math.min(minA, e.AA[i][j]);
				minB = Math.min(minB, e.BB[i][j]);
				minC = Math.min(minC, e.CC[i][j]);
			}
		}

		for (int j = 0; j < L1; j++) {
			A[j] /= (double) N;
			B[j] /= (double) N;
			C[j] /= (double) N;
		}

		for (int j = 0; j < L1; j++) {
			System.out.print(A[j] + " ");
		}
		System.out.println();
		for (int j = 0; j < L1; j++) {
			System.out.print(B[j] + " ");
		}
		System.out.println();
		for (int j = 0; j < L1; j++) {
			System.out.print(C[j] + " ");
		}
		System.out.println();
		/*
		 * double[][] C1 = new double[N][N]; double[][] C2 = new double[N][N];
		 * for (int i = 0; i < N; i++) { for (int j = 0; j < N; j++) { //
		 * C1[i][j] = jsd(e.AA[i], e.AA[j]); C2[i][j] = jsd(e.AA[i], e.BB[j]); }
		 * }
		 * 
		 * HungarianAlgorithm H = new HungarianAlgorithm(C2); int[] M =
		 * H.execute();
		 * 
		 * double result = 0; for (int i = 0; i < N; i++) { if (M[i] == -1) {
		 * continue; } result += C2[i][M[i]];
		 * 
		 * for (int j = 0; j < 10; j++) { System.out.print(e.AA[i][j] + " "); }
		 * System.out.println(); for (int j = 0; j < 10; j++) {
		 * System.out.print(e.BB[M[i]][j] + " "); } System.out.println();
		 * System.out.println();
		 * 
		 * } result /= N;
		 * 
		 * System.out.println(result);
		 */

	}

	public static double kld(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	public static double jsd(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}

}

class Node<T> {
	public ArrayList<Node<T>> children = new ArrayList<Node<T>>();
	public T data;
}

class E {

	int num;
	ArrayList<Node<Double>> A, B, C;
	double[][] AA, BB, CC;

	public E(int num, int num_level1, int num_level2) {
		Well19937c r = new Well19937c();

		AA = new double[num][num_level1];
		BB = new double[num][num_level1];
		CC = new double[num][num_level1];

		// A = new ArrayList<Node<Double>>(num);
		// B = new ArrayList<Node<Double>>(num);
		// C = new ArrayList<Node<Double>>(num);

		for (int i = 0; i < num; i++) { // for each participant
			Node<Double> a0 = new Node<Double>();
			Node<Double> b0 = new Node<Double>();
			for (int j = 0; j < num_level1; j++) { // for each group
				Node<Double> a1 = new Node<Double>();
				Node<Double> b1 = new Node<Double>();
				for (int k = 0; k < num_level2; k++) { // for each skill
					Node<Double> a2 = new Node<Double>();
					Node<Double> b2 = new Node<Double>();
					if (r.nextInt(num_level1) < num_level1) { // value
						if (r.nextInt(num_level2) < num_level2) {
							a2.data = (double) (r.nextBoolean() ? 1 : 0); // 1
						} else {
							a2.data = 0.0;
						}
					} else {
						a2.data = 0.0;
					}
					if (r.nextBoolean()) { // flip
						b2.data = a2.data;
					} else {
						b2.data = (double) (r.nextBoolean() ? 1 : 0);
					}
					a1.children.add(a2);
					b1.children.add(b2);
				}
				double ac = 0, bc = 0, cc = 0;
				for (int k = 0; k < num_level2; k++) {
					ac += a1.children.get(k).data;
					bc += b1.children.get(k).data;
					cc += b1.children.get(k).data;
				}
				ac = ac / num_level2;
				bc = (0.75 * bc + 0.25 * (num_level2 - bc)) / num_level2;
				cc = cc / num_level2;
				// a1.data = ac;
				// b1.data = bc;
				AA[i][j] = ac;
				BB[i][j] = bc;
				CC[i][j] = cc;
				// a0.children.add(a1);
				// b0.children.add(b1);

			}
			// A.add(a0);
			// B.add(b0);

			double ac = 0, bc = 0, cc = 0;
			for (int j = 0; j < num_level1; j++) {
				ac += AA[i][j];
				bc += BB[i][j];
				cc += CC[i][j];
			}
			for (int j = 0; j < num_level1; j++) {
				AA[i][j] /= Math.max(1, ac);
				BB[i][j] /= Math.max(1, bc);
				CC[i][j] /= Math.max(1, cc);
			}

		}

		this.num = num;
	}

}