package druid10;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.ml.clustering.FuzzyKMeansClusterer;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import com.apporiented.algorithm.clustering.AverageLinkageStrategy;
import com.apporiented.algorithm.clustering.ClusteringAlgorithm;
import com.apporiented.algorithm.clustering.CompleteLinkageStrategy;
import com.apporiented.algorithm.clustering.DefaultClusteringAlgorithm;
import com.apporiented.algorithm.clustering.SingleLinkageStrategy;
import com.apporiented.algorithm.clustering.visualization.DendrogramPanel;

@SuppressWarnings("unused")

public class Main {

	public static void main(String[] args) {
		RandomGenerator rng = new MersenneTwister(123456789);
		PoissonDistribution bin = new PoissonDistribution(4);

		int n = 1000, n1 = 10, n2 = 10;

		// int[] s = bin.sample(n1);

		// ArrayList<A> aaa = new ArrayList<A>();
		ArrayList<B> bbb = new ArrayList<B>();

		for (int i = 0; i < n; i++) {
			int[] s = new int[n1];
			for (int j = 0; j < n1; j++) {
				s[j] = (int) (100 * bin.probability(j));
			}

			// int[] s = bin.sample(n1);
			// A a = new A(s);
			// aaa.add(a);
			B b = new B(s, n2, rng);
			bbb.add(b);
		}

		// double[][] distancesA = new double[n][n];
		double[][] distancesB = new double[n][n];
		String[] names = new String[n];

		for (int i = 0; i < n; i++) {
			names[i] = "" + i;
			for (int j = 0; j < n; j++) {
				// distancesA[i][j] = JensenShannon.jsd(aaa.get(i).v,
				// aaa.get(j).v);
				distancesB[i][j] = JensenShannon.jsd(bbb.get(i).v, bbb.get(j).v);
			}
		}

		ClusteringAlgorithm alg = new DefaultClusteringAlgorithm();
		// com.apporiented.algorithm.clustering.Cluster clusterA =
		// alg.performClustering(distancesA, names,
		// new AverageLinkageStrategy());
		com.apporiented.algorithm.clustering.Cluster clusterB = alg.performClustering(distancesB, names,
				new AverageLinkageStrategy());

		DendrogramPanel dp = new DendrogramPanel();
		dp.setModel(clusterB);

		JFrame frame = new JFrame();
		frame.setSize(400, 300);
		frame.setLocation(400, 300);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		JPanel content = new JPanel();
		frame.setContentPane(content);
		content.setBackground(Color.red);
		content.setLayout(new BorderLayout());
		content.add(dp, BorderLayout.CENTER);
		dp.setBackground(Color.WHITE);
		dp.setLineColor(Color.BLACK);
		dp.setScaleValueDecimals(0);
		dp.setScaleValueInterval(1);
		dp.setShowDistances(false);

		frame.setVisible(true);

		double[] h = new double[n1];
		// for (int i = 0; i < n; i++) {
		// for (int j = 0; j < n1; j++) {
		// h[j] += aaa.get(i).v[j];
		// }
		// }

		for (int j = 0; j < n1; j++) {
			System.out.print(h[j] + " ");
		}
		System.out.println();
		h = new double[n1];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n1; j++) {
				h[j] += bbb.get(i).v[j];
			}
		}
		for (int j = 0; j < n1; j++) {
			System.out.print(h[j]/n + " ");
		}
		// System.out.println();
		// System.out.println();
		// h = new double[n1];
		// for (int i = 0; i < n; i++) {
		// for (int j = 0; j < n1; j++) {
		// h[j] += ccc.get(i).v[j];
		// }
		// }
		// for (int j = 0; j < n1; j++) {
		// System.out.print(h[j] + " ");
		// }
		// System.out.println();
		// System.out.println();
		/*
		 * DistanceMeasure jsd = new JensenShannon();
		 * 
		 * System.out.println("DBSCANClusterer"); System.out.println();
		 * 
		 * DBSCANClusterer<A> clustererA = new DBSCANClusterer<A>(0.01, 5, jsd);
		 * // FuzzyKMeansClusterer fuzzy = new FuzzyKMeansClusterer<A>(5, 10,
		 * 100, // jsd); DBSCANClusterer<B> clustererB = new
		 * DBSCANClusterer<B>(0.01, 5, jsd);
		 * 
		 * List<Cluster<A>> clustersA = clustererA.cluster(aaa);
		 * List<Cluster<B>> clustersB = clustererB.cluster(bbb);
		 * 
		 * for (Cluster<A> cl : clustersA) {
		 * System.out.print(cl.getPoints().size() + " "); double[] pt = new
		 * double[n1]; for (A a : cl.getPoints()) { for (int i = 0; i < n1; i++)
		 * { pt[i] += a.v[i]; } } for (int i = 0; i < pt.length; i++) {
		 * System.out.print(pt[i] + " "); } System.out.println(); }
		 * 
		 * System.out.println();
		 * 
		 * for (Cluster<B> cl : clustersB) {
		 * System.out.print(cl.getPoints().size() + " "); double[] pt = new
		 * double[n1]; for (B a : cl.getPoints()) { for (int i = 0; i < n1; i++)
		 * { pt[i] += a.v[i]; } } for (int i = 0; i < pt.length; i++) {
		 * System.out.print(pt[i] + " "); } System.out.println(); }
		 * 
		 * System.out.println(); System.out.println("FuzzyKMeansClusterer");
		 * System.out.println();
		 * 
		 * FuzzyKMeansClusterer<A> clusterer2A = new FuzzyKMeansClusterer<A>(5,
		 * 100, 100, jsd); FuzzyKMeansClusterer<B> clusterer2B = new
		 * FuzzyKMeansClusterer<B>(5, 100, 100, jsd);
		 * 
		 * List<CentroidCluster<A>> clusters2A = clusterer2A.cluster(aaa);
		 * List<CentroidCluster<B>> clusters2B = clusterer2B.cluster(bbb);
		 * 
		 * for (Cluster<A> cl : clusters2A) {
		 * System.out.print(cl.getPoints().size() + " "); double[] pt = new
		 * double[n1]; for (A a : cl.getPoints()) { for (int i = 0; i < n1; i++)
		 * { pt[i] += a.v[i]; } } for (int i = 0; i < pt.length; i++) {
		 * System.out.print(pt[i] + " "); } System.out.println(); }
		 * 
		 * System.out.println(); System.out.println();
		 * 
		 * for (Cluster<B> cl : clusters2B) {
		 * System.out.print(cl.getPoints().size() + " "); double[] pt = new
		 * double[n1]; for (B a : cl.getPoints()) { for (int i = 0; i < n1; i++)
		 * { pt[i] += a.v[i]; } } for (int i = 0; i < pt.length; i++) {
		 * System.out.print(pt[i] + " "); } System.out.println(); }
		 * 
		 */

	}

}

class A implements Clusterable {

	String uniqueID = "A" + UUID.randomUUID().toString();

	final double[] v;

	public A(int[] iv) {
		int n = iv.length;
		int c = 0;
		for (int i = 0; i < n; i++) {
			c += iv[i];
		}
		v = new double[n];
		for (int i = 0; i < n; i++) {
			v[i] = ((double) iv[i]) / (c == 0 ? 1 : c);
		}
	}

	@Override
	public double[] getPoint() {
		return v;
	}

}

class B implements Clusterable {

	String uniqueID = "B" + UUID.randomUUID().toString();

	final double[] v;

	public B(int[] iv, int n2, RandomGenerator rng) {
		int n = iv.length;
		v = new double[n];
		double c = 0;
		for (int i = 0; i < n; i++) {
			int c2 = 0;
			for (int j = 0; j < n2; j++) {
				int a = j < iv[i] ? 1 : 0;
				int b = rng.nextBoolean() ? a : rng.nextInt(2);
				c2 += b;
			}
			double yes = c2 * 0.75 + (n2 - c2) * 0.25;
			double no = (n2 - c2) * 0.75 + c2 * 0.25;
			v[i] = (yes - no) / (0.75 * n2);
			v[i] = (v[i] + 1) / (double)2;
			c += v[i];
		}
		for (int i = 0; i < n; i++) {
			v[i] /= c == 0 ? 1 : c;
		}
	}

	@Override
	public double[] getPoint() {
		return v;
	}

}

@SuppressWarnings("serial")
class JensenShannon implements DistanceMeasure {

	@Override
	public double compute(double[] a, double[] b) throws DimensionMismatchException {
		if (a.length != b.length) {
			throw new DimensionMismatchException(a.length, b.length);
		}

		return jsd(a, b);
	}

	static double kld(double[] p, double[] q) {
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	static double jsd(double[] p, double[] q) {
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}

}