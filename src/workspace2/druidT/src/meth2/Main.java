package meth2;

import java.util.ArrayList;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

import ch.usi.inf.sape.hac.HierarchicalAgglomerativeClusterer;
import ch.usi.inf.sape.hac.agglomeration.AgglomerationMethod;
import ch.usi.inf.sape.hac.agglomeration.*;
import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import ch.usi.inf.sape.hac.dendrogram.DendrogramBuilder;
import ch.usi.inf.sape.hac.experiment.DissimilarityMeasure;
import ch.usi.inf.sape.hac.experiment.Experiment;

public class Main {
	public static void main(String[] args) {
		MyExperiment experiment = new MyExperiment(100, 10, 10, 0.05);
		MyDissimilarityMeasure dissimilarityMeasure = new MyDissimilarityMeasure();
		AgglomerationMethod agglomerationMethod = new AverageLinkage();
		DendrogramBuilder dendrogramBuilder = new DendrogramBuilder(experiment.getNumberOfObservations());
		HierarchicalAgglomerativeClusterer clusterer = new HierarchicalAgglomerativeClusterer(experiment,
				dissimilarityMeasure, agglomerationMethod);
		clusterer.cluster(dendrogramBuilder);
		Dendrogram dendrogram = dendrogramBuilder.getDendrogram();
		System.out.println(dendrogram.getRoot().getRight().getObservationCount());
	}
}

class Node<T> {
	public ArrayList<Node<T>> children = new ArrayList<Node<T>>();
	public T data;
}

class MyExperiment implements Experiment {

	int num;
	ArrayList<Node<Double>> A, B;
	double[][] C;

	public MyExperiment(int num, int num_level1, int num_level2, double p) {
		Well19937c r = new Well19937c();

		BinomialDistribution prob1 = new BinomialDistribution(r, 1, 0.1);
		BinomialDistribution prob2 = new BinomialDistribution(r, 1, 0.3);
		BinomialDistribution prob3 = new BinomialDistribution(r, 1, 0.5);

		BinomialDistribution d = new BinomialDistribution(r, 1, p);

		A = new ArrayList<Node<Double>>(num);
		B = new ArrayList<Node<Double>>(num);
		C = new double[num][num_level1];

		for (int i = 0; i < num; i++) { // for each participant
			Node<Double> a0 = new Node<Double>();
			Node<Double> b0 = new Node<Double>();
			for (int j = 0; j < num_level1; j++) { // for each group
				Node<Double> a1 = new Node<Double>();
				Node<Double> b1 = new Node<Double>();
				for (int k = 0; k < num_level2; k++) { // for each skill
					Node<Double> a2 = new Node<Double>();
					Node<Double> b2 = new Node<Double>();
					if (prob1.sample() == 0) { // value
						if (prob2.sample() == 0) {
							a2.data = (double) prob3.sample(); // 1
						} else {
							a2.data = 0.0;
						}
					} else {
						a2.data = 0.0;
					}
					if (r.nextBoolean()) { // flip
						b2.data = a2.data;
					} else {
						b2.data = (double) (r.nextBoolean() ? 1 : 0);
					}
					a1.children.add(a2);
					b1.children.add(b2);
				}
				double ac = 0, bc = 0;
				for (int k = 0; k < num_level2; k++) {
					ac += a1.children.get(k).data;
					bc += b1.children.get(k).data;
				}
				ac = ac / num_level2;
				bc = (0.75 * bc + 0.25 * (num_level2 - bc)) / num_level2;
				a1.data = ac;
				b1.data = bc;
				a0.children.add(a1);
				b0.children.add(b1);
				C[i][j] = bc;
			}
			A.add(a0);
			B.add(b0);
		}

		this.num = num;
	}

	@Override
	public int getNumberOfObservations() {
		return num;
	}

	public double[] getObservation(int i) {
		return C[i];
	}

}

class MyDissimilarityMeasure implements DissimilarityMeasure {

	public double computeDissimilarity(Experiment experiment, int observation1, int observation2) {
		return jsd(((MyExperiment) experiment).getObservation(observation1),
				((MyExperiment) experiment).getObservation(observation2));
	}

	public static double kld(double[] p, double[] q) {
		assert (p.length == q.length);
		double div = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0) {
				continue;
			}
			if (q[i] == 0.0) {
				continue;
			}
			div += p[i] * Math.log(p[i] / q[i]);
		}
		return div / Math.log(2);
	}

	public static double jsd(double[] p, double[] q) {
		assert (p.length == q.length);
		double[] average = new double[p.length];
		for (int i = 0; i < p.length; ++i) {
			average[i] += (p[i] + q[i]) / 2;
		}
		return (kld(p, average) + kld(q, average)) / 2;
	}

}