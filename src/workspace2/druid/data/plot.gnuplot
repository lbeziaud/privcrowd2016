set terminal wxt persist enhanced font 'Verdana,6' 

set multiplot layout 2, 3 title "p_{flip}=0.5, p_{fskill}=0.5, epsilon=log(2)"

#set nokey
set key bottom right

set title "d=Custom, N=1000, p_{skill}=0.05"
plot 'a.dat' using 1:2 with line title 'original', \
     'a.dat' using 1:5 with line title 'flipped', \
     'a.dat' using 1:8 with line title 'laplace'

set title "d=Custom, N=1000, p_{skill}=0.10"
plot 'c.dat' using 1:2 with line title 'original', \
     'c.dat' using 1:5 with line title 'flipped', \
     'c.dat' using 1:8 with line title 'laplace'

set title "d=Custom, N=3000, p_{skill}=0.05"
plot 'e.dat' using 1:2 with line title 'original', \
     'e.dat' using 1:5 with line title 'flipped', \
     'e.dat' using 1:8 with line title 'laplace'

set title "d=Hamming, N=1000, p_{skill}=0.05"
plot 'b.dat' using 1:2 with line title 'original', \
     'b.dat' using 1:5 with line title 'flipped', \
     'b.dat' using 1:8 with line title 'laplace'

set title "d=Hamming, N=1000, p_{skill}=0.10"
plot 'd.dat' using 1:2 with line title 'original', \
     'd.dat' using 1:5 with line title 'flipped', \
     'd.dat' using 1:8 with line title 'laplace'

set title "d=Hamming, N=3000, p_{skill}=0.05"
plot 'f.dat' using 1:2 with line title 'original', \
     'f.dat' using 1:5 with line title 'flipped', \
     'f.dat' using 1:8 with line title 'laplace'



unset multiplot
