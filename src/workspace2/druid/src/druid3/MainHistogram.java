package druid3;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

public class MainHistogram {

	public static void main(String[] args) {
		Well19937c rng = new Well19937c();
		double ps = 0.05;
		BinomialDistribution ds = new BinomialDistribution(rng, 1, ps);

		int N = 1000;
		int D = 10;

		int[][] A = new int[N][D];
		for (int i = 0; i < N; i++) {
			A[i] = ds.sample(D);
		}

		/*
		 * int[] H = new int[D]; for (int i = 0; i < N; i++) { for (int j = 0; j
		 * < D; j++) { H[j] += A[i][j]; } }
		 * 
		 * for (int i = 0; i < D; i++) { System.out.print(H[i] + " "); }
		 * System.out.println(" ");
		 */

		int k = 10;
		int[][] Hi = new int[k][D];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < D; j++) {
				Hi[(int) (Math.floor(i / (N / k)))][j] += A[i][j];
			}
		}

		for (int i = 0; i < Hi.length; i++) {
			for (int j = 0; j < D; j++) {
				System.out.print(Hi[i][j] + " ");
			}
			System.out.println(" ");
		}

	}

}
