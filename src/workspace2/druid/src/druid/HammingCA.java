package druid;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;

public class HammingCA {

	public static void main(String[] args) {
		int num_participants = 3000;
		double p_skill = 0.05;
		double p_flip = 0.5;
		double p_fskill = 0.5;
		double epsilon = 0.1;

		for (int num_skills = 100; num_skills <= 300; num_skills += 10) {
			double cost1 = 0, min1 = 0, max1 = 0;
			double cost2 = 0, min2 = 0, max2 = 0;
			double cost3 = 0, min3 = 0, max3 = 0;

			for (int i = 0; i < 10; i++) {
				HammingCA hca = new HammingCA(num_participants, num_skills, p_skill, p_flip, p_fskill, epsilon);

				cost1 += hca.cost1;
				min1 += hca.min1;
				max1 += hca.max1;

				cost2 += hca.cost2;
				min2 += hca.min2;
				max2 += hca.max2;

				cost3 += hca.cost3;
				min3 += hca.min3;
				max3 += hca.max3;

			}

			cost1 /= 10;
			cost2 /= 10;
			cost3 /= 10;

			max1 /= 10;
			max2 /= 10;
			max3 /= 10;

			min1 /= 10;
			min2 /= 10;
			min3 /= 10;

			System.out.println(num_skills + " " + cost1 + " " + min1 + " " + max1 + " " + cost2 + " " + min2 + " "
					+ max2 + " " + cost3 + " " + min3 + " " + max3);
		}
	}

	public double cost1, max1, min1;
	public double cost2, max2, min2;
	public double cost3, max3, min3;

	public HammingCA(int num_participants, int num_skills, double p_skill, double p_flip, double p_fskill,
			double epsilon) {

		Well19937c rng = new Well19937c();
		BinomialDistribution d_skill = new BinomialDistribution(rng, 1, p_skill);
		BinomialDistribution d_flip = new BinomialDistribution(rng, 1, p_flip);
		BinomialDistribution d_fskill = new BinomialDistribution(rng, 1, p_fskill);
		LaplaceDistribution d_lap = new LaplaceDistribution(rng, 0, (double) num_participants / epsilon);

		int[][] W1 = new int[num_participants][num_skills];
		int[][] T1 = new int[num_participants][num_skills];
		for (int i = 0; i < num_participants; i++) {
			W1[i] = d_skill.sample(num_skills);
			T1[i] = d_skill.sample(num_skills);
		}

		double[][] C1 = new double[num_participants][num_participants];
		for (int i = 0; i < num_participants; i++) {
			for (int j = 0; j < num_participants; j++) {
				C1[i][j] = distance(W1[i], T1[j]);
			}
		}

		HungarianAlgorithm H1 = new HungarianAlgorithm(C1);
		int[] M1 = H1.execute();

		int[][] W2 = new int[num_participants][num_skills];
		int[][] T2 = new int[num_participants][num_skills];
		for (int i = 0; i < num_participants; i++) {
			for (int j = 0; j < num_skills; j++) {
				W2[i][j] = (d_flip.sample() > 0.5) ? W1[i][j] : d_fskill.sample();
				T2[i][j] = (d_flip.sample() > 0.5) ? T1[i][j] : d_fskill.sample();
			}
		}

		double[][] C2 = new double[num_participants][num_participants];
		for (int i = 0; i < num_participants; i++) {
			for (int j = 0; j < num_participants; j++) {
				C2[i][j] = distance(W2[i], T2[j]);
			}
		}

		HungarianAlgorithm H2 = new HungarianAlgorithm(C2);
		int[] M2 = H2.execute();

		double[][] C3 = new double[num_participants][num_participants];
		for (int i = 0; i < num_participants; i++) {
			for (int j = 0; j < num_participants; j++) {
				C3[i][j] = C1[i][j] + d_lap.sample();
			}
		}

		HungarianAlgorithm H3 = new HungarianAlgorithm(C3);
		int[] M3 = H3.execute();

		cost1 = 0;
		min1 = Double.POSITIVE_INFINITY;
		max1 = Double.NEGATIVE_INFINITY;
		cost2 = 0;
		min2 = Double.POSITIVE_INFINITY;
		max2 = Double.NEGATIVE_INFINITY;
		cost3 = 0;
		min3 = Double.POSITIVE_INFINITY;
		max3 = Double.NEGATIVE_INFINITY;

		for (int i = 0; i < num_participants; i++) {
			if (M1[i] != -1) {
				cost1 += C1[i][M1[i]];
				min1 = Math.min(min1, C1[i][M1[i]]);
				max1 = Math.max(max1, C1[i][M1[i]]);
			}

			if (M2[i] != -1) {
				cost2 += C1[i][M2[i]];
				min2 = Math.min(min2, C1[i][M2[i]]);
				max2 = Math.max(max2, C1[i][M2[i]]);
			}

			if (M3[i] != -1) {
				cost3 += C1[i][M3[i]];
				min3 = Math.min(min3, C1[i][M3[i]]);
				max3 = Math.max(max3, C1[i][M3[i]]);
			}
		}

		cost1 /= num_participants;
		cost2 /= num_participants;
		cost3 /= num_participants;
	}

	private double distance(int[] w, int[] t) {
		return Distance.hamming(w, t);
/*
		int missing = 0, required = 0;
		for (int i = 0; i < w.length; i++) {
			if (t[i] != 0) {
				required += 1;
				if (w[i] == 0) {
					missing += 1;
				}
			}
		}
		return (double) missing / ((required == 0) ? 1 : required);
*/
	}

}
