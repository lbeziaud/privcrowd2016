package druid;

public class Main {

	public static void main(String[] args) {
		int N = 1000;
		double p1 = 0.5;
		double p2 = 0.05;
		double p3 = 0.5;

		for (int D = 100; D <= 300; D += 50) {
			double[] q1 = new double[11];
			double[] q2 = new double[11];
			double[] q2min = new double[11];
			double[] q2max = new double[11];

//			DiceFCA diceFCA = new DiceFCA(N, D, p1, p2, p3);
//			JaccardFCA jaccardFCA = new JaccardFCA(N, D, p1, p2, p3);
//			KulsinskiFCA kulsinskiFCA = new KulsinskiFCA(N, D, p1, p2, p3);
//			HammingFCA hammingFCA = new HammingFCA(N, D, p1, p2, p3);
//			RogerstanimotoFCA rogerstanimotoFCA = new RogerstanimotoFCA(N, D, p1, p2, p3);
//			RussellraoFCA russellraoFCA = new RussellraoFCA(N, D, p1, p2, p3);
//			SokalmichenerFCA sokalmichenerFCA = new SokalmichenerFCA(N, D, p1, p2, p3);
//			SokalsneathFCA sokalsneathFCA = new SokalsneathFCA(N, D, p1, p2, p3);
//			YuleFCA yuleFCA = new YuleFCA(N, D, p1, p2, p3);
			Custom1FCA custom1FCA = new Custom1FCA(N, D, p1, p2, p3);
//			HammingLCA hammingLCA = new HammingLCA(N, D, p2, 0.1);

//			q1[0] = diceFCA.Q1;
//			q1[1] = jaccardFCA.Q1;
//			q1[2] = kulsinskiFCA.Q1;
//			q1[3] = hammingFCA.Q1;
//			q1[4] = rogerstanimotoFCA.Q1;
//			q1[5] = russellraoFCA.Q1;
//			q1[6] = sokalmichenerFCA.Q1;
//			q1[7] = sokalsneathFCA.Q1;
//			q1[8] = yuleFCA.Q1;
			q1[9] = custom1FCA.Q1;
//			q1[10] = hammingLCA.Q1;

//			q2[0] = diceFCA.Q2;
//			q2[1] = jaccardFCA.Q2;
//			q2[2] = kulsinskiFCA.Q2;
//			q2[3] = hammingFCA.Q2;
//			q2[4] = rogerstanimotoFCA.Q2;
//			q2[5] = russellraoFCA.Q2;
//			q2[6] = sokalmichenerFCA.Q2;
//			q2[7] = sokalsneathFCA.Q2;
//			q2[8] = yuleFCA.Q2;
			q2[9] = custom1FCA.Q2;
//			q2[10] = hammingLCA.Q2;

//			q2max[0] = diceFCA.Q2max;
//			q2max[1] = jaccardFCA.Q2max;
//			q2max[2] = kulsinskiFCA.Q2max;
//			q2max[3] = hammingFCA.Q2max;
//			q2max[4] = rogerstanimotoFCA.Q2max;
//			q2max[5] = russellraoFCA.Q2max;
//			q2max[6] = sokalmichenerFCA.Q2max;
//			q2max[7] = sokalsneathFCA.Q2max;
//			q2max[8] = yuleFCA.Q2max;
			q2max[9] = custom1FCA.Q2max;
//			q2max[10] = hammingLCA.Q2max;

//			q2min[0] = diceFCA.Q2min;
//			q2min[1] = jaccardFCA.Q2min;
//			q2min[2] = kulsinskiFCA.Q2min;
//			q2min[3] = hammingFCA.Q2min;
//			q2min[4] = rogerstanimotoFCA.Q2min;
//			q2min[5] = russellraoFCA.Q2min;
//			q2min[6] = sokalmichenerFCA.Q2min;
//			q2min[7] = sokalsneathFCA.Q2min;
//			q2min[8] = yuleFCA.Q2min;
			q2min[9] = custom1FCA.Q2min;
//			q2min[10] = hammingLCA.Q2min;

			System.out.print(D + " ");
			for (int i = 0; i < 11; i++) {
				System.out.print(q1[i] + " ");
			}
			System.out.println();

			System.out.print(D + " ");
			for (int i = 0; i < 11; i++) {
				System.out.print(q2[i] + " ");
			}
			System.out.println();

			System.out.print(D + " ");
			for (int i = 0; i < 11; i++) {
				System.out.print(q2min[i] + " ");
			}
			System.out.println();

			System.out.print(D + " ");
			for (int i = 0; i < 11; i++) {
				System.out.print(q2max[i] + " ");
			}
			System.out.println();

			System.out.println();
		}
	}

}

class DiceFCA extends CrowdFlipAssignment {

	public DiceFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.dice(w, t);
	}
}

class JaccardFCA extends CrowdFlipAssignment {

	public JaccardFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.jaccard(w, t);
	}
}

class KulsinskiFCA extends CrowdFlipAssignment {

	public KulsinskiFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.kulsinski(w, t);
	}
}

class HammingFCA extends CrowdFlipAssignment {

	public HammingFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.hamming(w, t);
	}
}

class RogerstanimotoFCA extends CrowdFlipAssignment {

	public RogerstanimotoFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.rogerstanimoto(w, t);
	}
}

class RussellraoFCA extends CrowdFlipAssignment {

	public RussellraoFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.russellrao(w, t);
	}
}

class SokalmichenerFCA extends CrowdFlipAssignment {

	public SokalmichenerFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.sokalmichener(w, t);
	}
}

class SokalsneathFCA extends CrowdFlipAssignment {

	public SokalsneathFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		// TODO Auto-generated method stub
		return 0;
	}
}

class YuleFCA extends CrowdFlipAssignment {

	public YuleFCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.yule(w, t);
	}
}

class Custom1FCA extends CrowdFlipAssignment {

	public Custom1FCA(int N, int D, double p1, double p2, double p3) {
		super(N, D, p1, p2, p3);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		double cft = 0, cxt = 0;
		for (int i = 0; i < w.length; i++) {
			if (w[i] == 0 && t[i] == 0) {
				cxt += 0.25;
				cft += 0.25 * 0.75;
			}
			if (w[i] == 0 && t[i] == 1) {
				cxt += 0.75;
				cft += 0.75 * 0.75;
			}
			if (w[i] == 1 && t[i] == 0) {
				cxt += 0.25;
				cft += 0.25 * 0.25;
			}
			if (w[i] == 1 && t[i] == 1) {
				cxt += 0.75;
				cft += 0.75 * 0.75;
			}
		}
		return (double) cft / ((cxt == 0) ? 1 : cxt);
	}

}

class HammingLCA extends CrowdLapAssignment {

	public HammingLCA(int N, int D, double p1, double epsilon) {
		super(N, D, p1, epsilon);
		// TODO Auto-generated constructor stub
	}

	@Override
	double distance(int[] w, int[] t) {
		return Distance.hamming(w, t);
	}

}