package druid;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;

abstract class CrowdFlipAssignment {

	private int N, D;
	private int[][] W1, T1;
	private int[][] W2, T2;
	private double[][] C1, C2;
	private HungarianAlgorithm H1, H2;
	private int[] M1, M2;
	double Q1, Q2, Q2min, Q2max;

	private Well19937c rng = new Well19937c();
	private BinomialDistribution binom1;
	private BinomialDistribution binom2;
	private BinomialDistribution binom3;

	public CrowdFlipAssignment(int N, int D, double p1, double p2, double p3) {
		this.N = N;
		this.D = D;

		binom1 = new BinomialDistribution(rng, 1, p1);
		binom2 = new BinomialDistribution(rng, 1, p2);
		binom3 = new BinomialDistribution(rng, 1, p3);

		W1 = randomProfiles();
		T1 = randomProfiles();
		C1 = costMatrix(W1, T1);
		H1 = new HungarianAlgorithm(C1);
		M1 = H1.execute();
		Q1 = computeCost(C1, M1);

		W2 = flipProfiles(W1);
		T2 = flipProfiles(T1);
		C2 = costMatrix(W2, T2);
		H2 = new HungarianAlgorithm(C2);
		M2 = H2.execute();
		Q2 = computeCost(C1, M2);
		computeMinMax(C1, M2);
	}

	private double[][] costMatrix(int[][] W, int[][] T) {
		double[][] costMatrix = new double[N][N];

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				costMatrix[i][j] = distance(W[i], T[j]);
			}
		}

		return costMatrix;
	}

	abstract double distance(int[] w, int[] t);

	private int[][] randomProfiles() {
		int[][] profiles = new int[N][D];

		for (int i = 0; i < N; i++) {
			profiles[i] = binom2.sample(D);
		}

		return profiles;
	}

	private int[][] flipProfiles(int[][] profiles) {
		int[][] new_profiles = new int[N][D];

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < D; j++) {
				new_profiles[i][j] = (binom1.sample() > 0.5) ? profiles[i][j] : binom3.sample();
			}
		}

		return new_profiles;
	}

	private double computeCost(double[][] matrix, int[] match) {
		double result = 0;
		for (int i = 0; i < N; i++) {
			if (match[i] == -1) {
				continue;
			}
			result += matrix[i][match[i]];
		}
		return result / N;
	}

	private void computeMinMax(double[][] matrix, int[] match) {
		double min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < N; i++) {
			if (match[i] == -1) {
				continue;
			}
			min = Math.min(min, matrix[i][match[i]]);
			max = Math.max(max, matrix[i][match[i]]);
		}
		Q2min = min;
		Q2max = max;
	}

}
