package druid;

public class Distance {

	public static double dice(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (ctf + cft) / (2.0 * ctt + cft + ctf);
	}

	public static double jaccard(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (ctf + cft) / (ctt + cft + ctf);
	}

	public static double kulsinski(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (ctf + cft - ctt + n) / (cft + ctf + n);
	}

	public static double hamming(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (ctf + cft) / n;
	}

	public static double rogerstanimoto(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (2.0 * (ctf + cft)) / (ctt + cff + 2.0 * (ctf + cft));
	}

	public static double russellrao(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (n - ctt) / n;
	}

	public static double sokalmichener(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (2.0 * (ctf + cft)) / (2.0 * (ctf + cft) + cff + ctt);
	}

	public static double sokalsneath(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (2.0 * (ctf + cft)) / (ctt + 2.0 * (ctf + cft));
	}

	public static double yule(int[] w, int[] t) {
		int n = w.length;
		int i;
		int ctt = 0, cff = 0, cft = 0, ctf = 0;

		for (i = 0; i < n; i++) {
			ctt += (w[i] != 0 && t[i] != 0) ? 1 : 0;
			cff += (w[i] == 0 && t[i] == 0) ? 1 : 0;
			ctf += (w[i] != 0 && t[i] == 0) ? 1 : 0;
			cft += (w[i] == 0 && t[i] != 0) ? 1 : 0;
		}

		return (double) (2.0 * (ctf + cft)) / ((ctt + cff) + (ctf + cft));
	}

}
