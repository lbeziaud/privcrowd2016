package druid4;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;
import druid.Distance;

public class MainLaplaceEpsilon {

	public static void main(String[] args) {
		Well19937c rng = new Well19937c();
		double p = 0.05;
		BinomialDistribution ps = new BinomialDistribution(rng, 1, p);

		int N = 1000;
		int D = 100;

		int[][] A = new int[N][D];
		for (int i = 0; i < N; i++) {
			A[i] = ps.sample(D);
		}

		double[][] C1 = new double[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				C1[i][j] = (double) d(A[i], A[j]);
			}
		}

		HungarianAlgorithm H1 = new HungarianAlgorithm(C1);
		int[] M1 = H1.execute();

		double cost1 = 0;
		for (int i = 0; i < N; i++) {
			if (M1[i] == -1) {
				continue;
			} else {
				cost1 += C1[i][M1[i]];
			}
		}
		System.out.println(cost1);
		System.out.println();

		for (double eps = 100; eps > 0.000001; eps -= 1) {
			LaplaceDistribution lp = new LaplaceDistribution(rng, 0, (double) N / eps);

			double[][] C2 = new double[N][N];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					C2[i][j] = C1[i][j] + lp.sample();
				}
			}

			HungarianAlgorithm H2 = new HungarianAlgorithm(C2);
			int[] M2 = H2.execute();

			double cost2 = 0;
			for (int i = 0; i < N; i++) {
				if (M2[i] == -1) {
					continue;
				}
				cost2 += C1[i][M2[i]];

			}

			cost2 /= N;

			System.out.println("[" + eps + "," + cost2 + "],");
		}

	}

	private static double d(int[] s, int[] r) {
		return Distance.hamming(s, r);
		/*int n = s.length;
		int t = 0;
		int ft = 0;
		for (int i = 0; i < n; i++) {
			if (r[i] == 1) {
				t += 1;
				if (s[i] != 1) {
					ft += 1;
				}
			}
		}
		if (t == 0) {
			t = 1;
		}
		return (double) ft / (double) t;*/
	}

}
