package druid4;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.Well19937c;

import blogspot.software_and_algorithms.stern_library.optimization.HungarianAlgorithm;

public class MainLaplaceN {

	public static void main(String[] args) {
		Well19937c rng = new Well19937c();
		double p = 0.05;
		BinomialDistribution ps = new BinomialDistribution(rng, 1, p);

		double eps = 0.01;

		int Nmax = 3000;
		int D = 100;

		int[][] A = new int[Nmax][D];
		for (int i = 0; i < Nmax; i++) {
			A[i] = ps.sample(D);
		}

		for (int N = 100; N <= Nmax; N += 100) {
			LaplaceDistribution lp = new LaplaceDistribution(rng, 0, eps / (double) N);

			double[][] C1 = new double[N][N];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					C1[i][j] = (double) d(A[i], A[j]);
				}
			}

			double[][] C2 = new double[N][N];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					C2[i][j] = C1[i][j] + lp.sample();
				}
			}

			// HungarianAlgorithm H1 = new HungarianAlgorithm(C1);
			// int[] M1 = H1.execute();

			HungarianAlgorithm H2 = new HungarianAlgorithm(C2);
			int[] M2 = H2.execute();

			// double cost1 = 0, min1 = Double.POSITIVE_INFINITY, max1 =
			// Double.NEGATIVE_INFINITY;
			double cost2 = 0, min2 = Double.POSITIVE_INFINITY, max2 = Double.NEGATIVE_INFINITY;
			for (int i = 0; i < N; i++) {
				// if (M1[i] == -1) {
				// continue;
				// } else {
				// cost1 += C1[i][M1[i]];
				// min1 = Math.min(min1, C1[i][M1[i]]);
				// max1 = Math.max(max1, C1[i][M1[i]]);
				// }
				if (M2[i] == -1) {
					continue;
				} else {
					cost2 += C1[i][M2[i]];
					min2 = Math.min(min2, C1[i][M2[i]]);
					max2 = Math.max(max2, C1[i][M2[i]]);
				}
			}
			// cost1 /= N * D;
			// min1 /= D;
			// max1 /= D;

			cost2 /= N * D;
			min2 /= D;
			max2 /= D;

			System.out.println("[" + N + ","
					+ /* cost1 + "," + min1 + "," + max1 + "," + */ cost2 + "," + min2 + "," + max2 + "],");
		}
	}

	private static double d(int[] s, int[] r) {
		int n = s.length;
		int t = 0;
		int ft = 0;
		
		for (int i = 0; i < n; i++) {
			ft += (r[i] == 1 & s[i] == 0) ? 1 : 0;
			t += (r[i] == 1) ? 1 : 0;
		}

		return (double)ft / (t == 0 ? 1 : t);
	}

}
