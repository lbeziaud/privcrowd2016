package druid2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;

public class MainDBSCAN {

	public static void main(String[] args) {
		int num_participants = 1000;
		int num_skills = 100;

		Collection<Participant> participants = new ArrayList<Participant>(num_participants);
		for (int i = 0; i < num_participants; i++) {
			participants.add(new Participant(num_skills));
		}

		double epsilon = 0.02;
		int minPts = 1;
		DBSCANClusterer<Participant> clusterer = new DBSCANClusterer<Participant>(epsilon, minPts);
		List<Cluster<Participant>> clusters = clusterer.cluster(participants);

		for (Cluster<Participant> c : clusters) {
			System.out.println(c.getPoints().get(0));
		}
	}
}