package druid2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well19937c;

import druid.Distance;

public class MainKMEANS {

	public static void main(String[] args) {
		int num_participants = 1000;
		int num_skills = 100;

		Collection<Participant> participants = new ArrayList<Participant>(num_participants);
		for (int i = 0; i < num_participants; i++) {
			participants.add(new Participant(num_skills));
		}

		int k = 10;
		int maxIterations = 1000;
		DistanceMeasure measure = new CustomMeasure();
		RandomGenerator random = new Well19937c();
		KMeansPlusPlusClusterer<Participant> clusterer = new KMeansPlusPlusClusterer<Participant>(k, maxIterations,
				measure, random);
		List<CentroidCluster<Participant>> clusterResults = clusterer.cluster(participants);

		for (int i = 0; i < clusterResults.size(); i++) {
			System.out.println("Cluster " + i);
			double distance = 0;
			int count = 0;
			for (Participant locationWrapper : clusterResults.get(i).getPoints()) {
				distance += measure.compute(locationWrapper.getPoint(), clusterResults.get(i).getCenter().getPoint());
				count += 1;
			}
			distance /= count;
			System.out.println(count + " points with average distance " + distance);
			System.out.println();
		}
	}
}

class CustomMeasure implements DistanceMeasure {

	@Override
	public double compute(double[] s1, double[] s2) throws DimensionMismatchException {
		int[] is1 = new int[s1.length];
		int[] is2 = new int[s2.length];

		for (int i = 0; i < s1.length; i++) {
			is1[i] = (int) s1[i];
			is2[i] = (int) s2[i];
		}

		return Distance.hamming(is1, is2);
	}

}

class Participant implements Clusterable {

	final static Well19937c rng = new Well19937c();
	final static double p_skill = 0.05;
	final static BinomialDistribution d_skill = new BinomialDistribution(rng, 1, p_skill);

	final private double[] skills;

	public Participant(int num_skills) {
		int[] i_skills = d_skill.sample(num_skills);
		skills = new double[num_skills];
		for (int i = 0; i < num_skills; i++) {
			skills[i] = (double) i_skills[i];
		}
	}

	@Override
	public double[] getPoint() {
		return skills;
	}

}