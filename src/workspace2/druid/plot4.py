#!/usr/bin/python3
# -*- coding: utf-8 -*-

# 1 run, custom dist missing/required

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm
from scipy.interpolate import spline

fig, ax = plt.subplots()

d0 = np.array([
[100000.0,0.0],
[10000.0,0.06650482295482295],
[1000.0,0.9288340187590188],
[100.0,0.9458407287157288],
[10.0,0.9448462731712732],
[1.0,0.9469720057720054],
[0.1,0.943908177933178],
[0.01,0.9492433261183263],
[0.001,0.9448610278610277],
[1.0E-4,0.9430931956931962],
[1.0E-5,0.9383844405594406],
[1.0000000000000002E-6,0.9428560966810974],
])

d1 = np.array([
[100000.0,0.011739999999999983],
[10000.0,0.09251999999999988],
[1000.0,0.09445999999999986],
[100.0,0.09433999999999991],
[10.0,0.09483999999999981],
[1.0,0.09419999999999976],
[0.1,0.09473999999999987],
[0.01,0.09385999999999985],
[0.001,0.0938],
[1.0E-4,0.09411999999999984],
[1.0E-5,0.0941799999999998],
[1.0000000000000002E-6,0.09383999999999981],
])

num_skills = d0[:,0]  
cost0 = d0[:,1]

cost1 = d1[:,1]

ax.set_title('ta with laplace noise on distance ($N=1000, D=100, p_s=0.05$)')

ax.set_xscale("log", nonposy='clip', basex=10)
ax.set_xlabel('epsilon')
ax.set_ylabel('normalized cumulative distance')
ax.grid()

ax.plot(num_skills, cost0, marker='x', color='red', label='pmiss')

ax.plot(num_skills, cost1, marker='x', color='blue', label='hamming')

for item in [fig, ax]:
    item.patch.set_visible(False)

plt.legend()

fig.tight_layout()
plt.show()



