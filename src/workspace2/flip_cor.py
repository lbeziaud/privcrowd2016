#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist, matching

num_agents = 10

@np.vectorize
def flip_item(i):
    return i if np.random.randint(2) else np.random.randint(2)

fig, ax = plt.subplots()

X = range(50, 201, 50)
Y = []

def distance(w, t):
    ny = 0
    yy = 0
    for wi, ti in zip(w, t):
      if wi == 0 and ti == 0:
          ny += 0.75 * 0.25
      if wi == 0 and ti == 1:
          ny += 0.75 * 0.75
      if wi == 1 and ti == 0:
          ny += 0.25 * 0.25
      if wi == 1 and ti == 1:
          ny += 0.75 * 0.75
    return ny

for i, num_skills in enumerate(X):
    agents1 = np.random.binomial(n=1, p=0.05, size=(num_agents, num_skills))
    agents2 = flip_item(agents1)

    cost_matrix1 = cdist(agents1, agents2, metric=distance)
    #cost_matrix1 = 1.0 - cost_matrix1
    
    row_ind1, col_ind1 = linear_sum_assignment(cost_matrix1)
    cost1 = cost_matrix1[row_ind1, col_ind1].sum() / num_agents
    
    Y.append(cost1)


#ax.set_ylim([0, 1])
#ax.set_xlim([50, 200])
ax.grid()

ax.plot(X, Y, marker='x')

for item in [fig, ax]:
    item.patch.set_visible(False)
            
fig.tight_layout()
plt.show()
#fig.savefig('cout{}.png'.format(i))
