#include <vector>
#include <array>
#include <random>
#include <algorithm>
#include "dbscan.h"
#include "multiarray.h"
#include "hungarian.h"

#ifndef NNN
#define NNN 500
#endif

#ifndef DDD
#define DDD 500
#endif

#include "distance.cpp"

int main(int argc, char **argv) {
  distance::distance dist = distance::hamming;
       
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution d(0.25);

  std::array<std::array<bool, DDD>, NNN> P1 /*, T1, P2, T2*/;
    
#pragma omp parallel for
  for (size_t i = 0; i < NNN; i++) {
#pragma omp parallel for
    for (size_t j = 0; j < DDD; j++) {
      P1[i][j] = d(gen);
      //T1[i][j] = d(gen);
      //P2[i][j] = d(gen) ? P1[i][j] : d(gen);
      //T2[i][j] = d(gen) ? T1[i][j] : d(gen);
      }
  }

  DBSCAN::ClusterData cl_d(NNN, NNN);
#pragma omp parallel for
  for (size_t i = 0; i < NNN; i++) {
#pragma omp parallel for
    for (size_t j = 0; j < NNN; j++) {
      cl_d(i, j) = dist(P1[i], P1[j]);
    }
  }
      
  DBSCAN dbs(std::stof(argv[1]), std::stof(argv[2]), 1);
  dbs.fit_precomputed(cl_d);

  //std::cout << dbs << std::endl;

  std::array<uint32_t, NNN> cls = {0};
  for (size_t i = 0; i < NNN; i++) {
    size_t l = dbs.get_labels()[i];
    cls[l] += 1;
  }

  for (size_t i = 0; i < NNN; i++) {
    if (cls[i] > 0)
      std::cout << cls[i] << " ";
  }
  std::cout << std::endl;
  
  /*  
  MultiArray<double, 2> C1(NNN, NNN), C2(NNN, NNN);
  
#pragma omp parallel for
  for (size_t i = 0; i < NNN; i++) {
#pragma omp parallel for
    for (size_t j = 0; j < NNN; j++) {
      C1[i][j] = dist(P1[i], T1[j]);
      C2[i][j] = dist(P2[i], T2[j]);
    }
  }  

  Hungarian H1(C1), H2(C2);
  std::array<uint32_t, NNN> A1, A2;
  
  H1.execute(&A1[0]);
  H2.execute(&A2[0]);
  
  double Q1, Q2;
#pragma omp parallel for
  for (size_t i = 0; i < NNN; i++) {
    Q1 += dist(P1[i], T1[A1[i]]);
    Q2 += dist(P1[i], T1[A2[i]]);
  }
  Q1 /= NNN;
  Q2 /= NNN;
  
  std::cout << NNN << " " << DDD << " " << Q1 << " " << Q2 << std::endl;
  */
  return 0;
}
