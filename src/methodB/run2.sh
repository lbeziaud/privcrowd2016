#!/bin/bash

export N=1000

for D in $(seq 100 50 300); do
    for i in $(seq 4); do
	echo -n "$D "
	make clean
	make D=$D N=$N 
	(./main > "$N-$D.$i.data") 
	(cat "$N-$D.1.data" | cut -d" " -f3 $f | awk -f tst.awk >> "z1-$N-$D.data") 
	(cat "$N-$D.1.data" | cut -d" " -f4 $f | awk -f tst.awk >> "z2-$N-$D.data") 
	(cat "z1-$N-$D.data" | awk '{for (i=1;i<=NF;i++){a[i]+=$i;}} END {for (i=1;i<=NF;i++){printf "%f", a[i]/NR; printf " "};printf "\n"}' > "avg-z1-$N-$D.data") 
	(cat "z2-$N-$D.data" | awk '{for (i=1;i<=NF;i++){a[i]+=$i;}} END {for (i=1;i<=NF;i++){printf "%f", a[i]/NR; printf " "};printf "\n"}' > "avg-z2-$N-$D.data")
    done 
    (echo "$D $(cat avg-z1-$N-$D.data)" >> "z1-$N.data") 
    (echo "$D $(cat avg-z2-$N-$D.data)" >> "z2-$N.data") 
    rm "$N-$D.?.data" "avg-z1-$N-$D.data" "avg-z2-$N-$D.data" "z1-$N-$D.data" "z2-$N-$D.data"
done

#awk '{ total += $2 } END { print total/NR }'
