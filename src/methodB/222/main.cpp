#include <vector>
#include <random>
#include <algorithm>
#include <dlib/optimization/max_cost_assignment.h>
#include "distance.cpp"

int main(int argc, char **argv) {
  std::vector<distance::distance> dists {distance::dice, distance::jaccard, distance::kulsinski,
      distance::hamming, distance::rogerstanimoto,  distance::russellrao,
      distance::sokalmichener,  distance::sokalsneath, distance::yule};
  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution d(0.05);

  constexpr uint32_t num_agents = 1000;
  std::vector<uint32_t> range_num_skills;
  for (size_t i = 100; i <= 300; i+=10) {
    range_num_skills.push_back(i);
  }

  std::cout << "[";

  for (auto const &dist : dists) {
  
    std::cout << "[";
  
    for (size_t i_num_skills = 0; i_num_skills < range_num_skills.size();
	 i_num_skills++) {
      uint32_t num_skills = range_num_skills[i_num_skills];
    
      std::vector<std::vector<bool> > P1(num_agents, std::vector<bool>(num_skills)),
	T1(num_agents, std::vector<bool>(num_skills)),
	P2(num_agents, std::vector<bool>(num_skills)),
	T2(num_agents, std::vector<bool>(num_skills));
    
#pragma omp parallel for
      for (size_t i = 0; i < num_agents; i++) {
#pragma omp parallel for
	for (size_t j = 0; j < num_skills; j++) {
	  P1[i][j] = d(gen);
	  T1[i][j] = d(gen);
	  P2[i][j] = d(gen) ? P1[i][j] : d(gen);
	  T2[i][j] = d(gen) ? T1[i][j] : d(gen);
	}
      }

      dlib::matrix<int> C1(num_agents, num_agents), C2(num_agents, num_agents);
    
#pragma omp parallel for
      for (size_t i = 0; i < num_agents; i++) {
#pragma omp parallel for
	for (size_t j = 0; j < num_agents; j++) {
	  C1(i, j) = (int)(10000 * (1.0 - dist(P1[i], T1[j])));
	  C2(i, j) = (int)(10000 * (1.0 - dist(P2[i], T2[j])));
	}
      }
    
      std::vector<long> A1 = dlib::max_cost_assignment(C1);
      std::vector<long> A2 = dlib::max_cost_assignment(C2);
    
      double Q1 = assignment_cost(C1, A1) / (double)(10000);
      double Q2 = assignment_cost(C1, A2) / (double)(10000);

      double Qmax2 = 0;
      double Qmin2 = 1;
      for (unsigned int i = 0; i < A2.size(); i++)  {
	Qmax2 = std::max(1.0 - dist(P1[i], T1[A2[i]]), Qmax2);
	Qmin2 = std::min(1.0 - dist(P1[i], T1[A2[i]]), Qmin2);
      }
      Qmax2 = Qmax2 - Q2;
      Qmin2 = Q2 - Qmin2;

      std::cout << "[" << num_skills << "," << Q1 << "," << Q2 << "," << Qmin2 << "," << Qmax2 << "],";
    }

    std::cout << "]," << std::endl;

  }

  std::cout << "]" << std::endl;
  
  return 0;
}
