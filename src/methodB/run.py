#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist, matching

titles = ['Dice', 'Jaccard-Needham', 'Kulsinski', 'Matching (Hamming)', 'Rogers-Tanimoto', 'Russell-Rao', 'Sokal-Michener', 'Sokal-Sneath', 'Yule']

metrics = ['dice', 'jaccard', 'kulsinski', 'matching', 'rogerstanimoto', 'russellrao', 'sokalmichener', 'sokalsneath', 'yule']

num_agents = 1000

@np.vectorize
def flip_item(i):
    return i if np.random.randint(2) else np.random.randint(2)

fig, axs = plt.subplots(nrows=3, ncols=3)

X = range(50, 201, 50)
YY1 = np.empty((9, len(list(X))))
YY2 = np.empty((9, len(list(X))))

for i, num_skills in enumerate(X):
    print("i:", i)
    
    agents1 = np.random.binomial(n=1, p=0.05, size=(num_agents, num_skills))
    tasks1 = np.random.binomial(n=1, p=0.05, size=(num_agents, num_skills))
    agents2 = flip_item(agents1)
    tasks2 = flip_item(tasks1)

    for j, metric in enumerate(metrics):
        print("j:", j)
        
        cost_matrix1 = cdist(agents1, tasks1, metric=metric)
        cost_matrix1 = 1.0 - cost_matrix1
        print(".")
        cost_matrix2 = cdist(agents2, tasks2, metric=metric)
        cost_matrix2 = 1.0 - cost_matrix2
        print(".")
        row_ind1, col_ind1 = linear_sum_assignment(cost_matrix1)
        cost1 = cost_matrix1[row_ind1, col_ind1].sum() / num_agents
        print(".")
        row_ind2, col_ind2 = linear_sum_assignment(cost_matrix2)
        cost2 = cost_matrix2[row_ind2, col_ind2].sum() / num_agents
        print(".")
        YY1[j][i] = cost1
        YY2[j][i] = cost2

for i, title, ax in enumerate(zip(titles, axs.reshape(-1))):
    ax.set_title(title)

    ax.set_ylim([0, 1])
    ax.set_xlim([50, 200])
    ax.grid()

    ax.plot(X, YY1[i], marker='x')
    ax.errorbar(X, YY2[i], marker='x', yerr=[Ymin2, Ymax2])

    for item in [fig, ax]:
        item.patch.set_visible(False)
            
fig.tight_layout()
plt.show()
#fig.savefig('cout{}.png'.format(i))
