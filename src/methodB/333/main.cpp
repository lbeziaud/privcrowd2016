#include <vector>
#include <random>
#include <algorithm>
#include "munkres.h"
#include "adapters/boostmatrixadapter.h"
#include "distance.cpp"

int main(int argc, char **argv) {
  std::vector<distance::distance> dists {distance::dice, distance::jaccard, distance::kulsinski,
      distance::hamming, distance::rogerstanimoto,  distance::russellrao,
      distance::sokalmichener,  distance::sokalsneath, distance::yule};
  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution d(0.05);

  constexpr uint32_t num_agents = 1000;
  std::vector<uint32_t> range_num_skills;
  for (size_t i = 100; i <= 300; i+=10) {
    range_num_skills.push_back(i);
  }

  //  std::cout << "[";

#pragma omp parallel for
  for (size_t i = 0; i < dists.size(); i++) {
    distance::distance dist = dists[i];
  
    //    std::cout << "[";

#pragma omp parallel for
    for (size_t i_num_skills = 0; i_num_skills < range_num_skills.size();
	 i_num_skills++) {
      uint32_t num_skills = range_num_skills[i_num_skills];
    
      std::vector<std::vector<bool> > P1(num_agents, std::vector<bool>(num_skills)),
	T1(num_agents, std::vector<bool>(num_skills)),
	P2(num_agents, std::vector<bool>(num_skills)),
	T2(num_agents, std::vector<bool>(num_skills));
    
#pragma omp parallel for
      for (size_t i = 0; i < num_agents; i++) {
#pragma omp parallel for
	for (size_t j = 0; j < num_skills; j++) {
	  P1[i][j] = d(gen);
	  T1[i][j] = d(gen);
	  P2[i][j] = d(gen) ? P1[i][j] : d(gen);
	  T2[i][j] = d(gen) ? T1[i][j] : d(gen);
	}
      }

      Matrix<double> C1(num_agents, num_agents), C2(num_agents, num_agents);
    
#pragma omp parallel for
      for (size_t i = 0; i < num_agents; i++) {
#pragma omp parallel for
	for (size_t j = 0; j < num_agents; j++) {
	  C1(i, j) = dist(P1[i], T1[j]);
	  C2(i, j) = dist(P2[i], T2[j]);
	}
      }

      Munkres<double> M1;
      M1.solve(C1);
      
      Munkres<double> M2;
      M2.solve(C2);

      double Q1, Q2, Qmax2, Qmin2;
      Q1 = 0;
      Q2 = 0;
      Qmax2 = -100000000000;
      Qmin2 = 100000000000;

      double q;
#pragma omp parallel for
      for (size_t i = 0; i < num_agents; i++) {
#pragma omp parallel for
	for (size_t j = 0; j < num_agents; j++) {
	  if (C1(i, j) == 0) {
	    Q1 += dist(P1[i], T1[j]);
	  }
	  if (C2(i, j) == 0) {
	    q = dist(P1[i], T1[j]);
	    Q2 += q;
	    Qmax2 = std::max(Qmax2, q);
	    Qmin2 = std::min(Qmin2, q);
	  }
	}
      }

      Q1 /= num_agents;
      Q2 /= num_agents;	

      Qmax2 = Qmax2 - Q2;
      Qmin2 = Q2 - Qmin2;

      std::cout << i << "," << num_skills << "," << Q1 << "," << Q2 << "," << Qmin2 << "," << Qmax2 << std::endl;
    }

  }
  
  return 0;
}
