#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib import cm

d1 = np.genfromtxt('z1-1000.data', delimiter=' ')
d2 = np.genfromtxt('z2-1000.data', delimiter=' ')

#titles = ['Dice', 'Jaccard-Needham', 'Kulsinski', 'Matching (Hamming)', 'Rogers-Tanimoto', 'Russell-Rao', 'Sokal-Michener', 'Sokal-Sneath', 'Yule']

#fig.suptitle('Normalized cumulative distance of assignment with respect to the number of skills')

for i in range(9):
    fig, ax = plt.subplots()

    ax.set_ylim([0, 1])
    ax.set_xlim([100, 300])
    ax.grid()

    ax.plot(d1[:,0], d1[:,i+1], marker='x')
    ax.plot(d1[:,0], d2[:,i+1], marker='x')

    for item in [fig, ax]:
        item.patch.set_visible(False)

    fig.tight_layout()
    fig.savefig('cout{}.png'.format(i))
