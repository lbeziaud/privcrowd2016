#include "boost/random.hpp"
#include "boost/generator_iterator.hpp"
#include "boost/random/laplace_distribution.hpp"
#include <cassert>
#include <random>
#include <string>
#include <math.h>
#include "multiarray.h"
#include "hungarian.h"

double distance(MultiArrayView<double, 2u, 2u> a, MultiArrayView<double, 2u, 2u> b) {
  assert(a.size() == b.size());
  double d = 0.0;
  for (size_t i = 0; i < a.size(); i++) {
    d += fabs(a[i] - b[i]);
  }
  return d;
}

int main(int argc, char **argv) {
  uint32_t N = std::stoi(argv[1]);
  uint32_t D = std::stoi(argv[2]);
  uint32_t BUD = std::stoi(argv[3]);
  uint32_t eps = std::stof(argv[4]);

  std::random_device rd;
  std::mt19937 gen(rd());

  std::uniform_real_distribution<> d;

  typedef boost::mt19937 RNGType;
  RNGType rng;
  boost::random::laplace_distribution<> lap(0.0, eps/(double)N);
  boost::variate_generator<RNGType, boost::random::laplace_distribution<> > noise(rng, lap);
  
  MultiArray<double, 2> A(N, D);
  MultiArray<double, 2> B(N, D);
  
  for (size_t i = 0; i < N; i++) {
    uint32_t budgeta = BUD;
    uint32_t budgetb = BUD;
    for (size_t j = 0; j < D; j++) {
      double randa = (budgeta > 0) ? d(gen) : 0.0;
      budgeta -= randa;
      double randb = (budgetb > 0) ? d(gen) : 0.0;
      budgetb -= randb;
      
      A[i][j] = randa;
      B[i][j] = randb;
    }
    //std::shuffle(v.begin(), v.end(), g);
  }

  MultiArray<double, 2> C(N, N);
  MultiArray<double, 2> dpC(N, N);

  for (size_t i = 0; i < N; i++) {
    for (size_t j = 0; j < N; j++) {
      double d = distance(A[i], B[j]) / D;
      C[i][j] = d;
      dpC[i][j] = d + noise();
    }
  }

  double dist = 0.0;
  for (size_t i = 0; i < N; i++) {
    double d = distance(C[i], dpC[i]);
    dist += d;
  }
  //  std::cout << dist << std::endl;
  //  return 0;
  
  Hungarian h(C);
  uint32_t match[N];
  h.execute(match);

  Hungarian dph(dpC);
  uint32_t dpmatch[N];
  dph.execute(dpmatch);
  
  double cost = 0.0;
  double dpcost = 0.0;
  for (size_t i = 0; i < N; i++) {
    cost += distance(A[i], B[match[i]]) / D;
    dpcost += distance(A[i], B[dpmatch[i]]) / D;
  }
  cost /= N;
  dpcost /= N;
  
  std::cout << cost << "," << dpcost << "," << dist << std::endl;
  
  return 0;
}
