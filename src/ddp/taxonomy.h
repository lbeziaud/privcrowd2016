#pragma once

#include <cmath>
#include "multiarray.h"

class Taxonomy {
 private:
  uint32_t k, d, n;
  MultiArray<double, 2> *memdist;
  std::vector<std::vector<double> > *memd;

 public:
  Taxonomy(uint32_t k, uint32_t d) : k(k), d(d) {
    n = (pow(k, d + 1) - 1) / (k - 1);
    this->memd = new std::vector<std::vector<double> >(n, std::vector<double>(n));
    
    //    this->memdist = new MultiArray<double, 2>(n, n);

    for (size_t i = 0; i < n; i++)
      for (size_t j = 0; j < n; j++)
	this->memd->at(i).at(j) = (d - height(lca(i, j))) / (double)d;
  }

  uint32_t parent(uint32_t n) { return std::ceil((n - 1) / (double)k); }

  uint32_t height(uint32_t n) {
    return std::ceil(std::log((k - 1) * n + 1) / std::log(k));
  }

  uint32_t size() { return n; }

  uint32_t size(uint32_t d) { return (pow(k, d + 1) - 1) / (k - 1); }

  uint32_t lca(uint32_t n1, uint32_t n2) {
    uint32_t h1 = height(n1);
    uint32_t h2 = height(n2);
    if (h1 > h2) {
      std::swap(h1, h2);
      std::swap(n1, n2);
    }
    uint32_t dh = h2 - h1;
    for (uint32_t h = 0; h < dh; h++) {
      n2 = parent(n2);
    }
    while (n1 > 0 && n2 > 0) {
      if (n1 == n2) return n1;
      n1 = parent(n1);
      n2 = parent(n2);
    }
    return 0;
  }

  double dist(uint32_t n1, uint32_t n2) {    
    //return (d - height(lca(n1, n2))) / (double)d;
    return this->memd->at(n1).at(n2);
  }

  double dist(std::vector<uint32_t> agent, uint32_t task) {
    double min = std::numeric_limits<double>::infinity();
    for (size_t i = 0; i < agent.size(); i++) {
      if (lca(agent[i], task) == task) return 0.0;
      min = std::min(min, dist(agent[i], task));
    }
    return min;
  }
};
