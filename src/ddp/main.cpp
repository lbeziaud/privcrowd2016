#include <limits>
#include <vector>
#include <random>
#include <time.h>
#include <stdlib.h>
#include "multiarray.h"
#include "hungarian.h"
#include "taxonomy.h"

#define K 4 // childs in the taxonomy
#define D 4 // depth of the taxonomy

int main(int argc, char **argv) {
  uint32_t N = std::stoi(argv[1]);
  uint32_t B = std::stoi(argv[2]);

  Taxonomy tax(K, D);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<uint32_t> distN(1, tax.size());
  std::uniform_int_distribution<> dist1(0, 1);
  
  std::vector<std::vector<uint32_t> > W(N);
  std::vector<uint32_t> T(N);

  for (size_t i = 0; i < N; i++) {
    T[i] = distN(gen);

    std::vector<uint32_t> w;
    //uint32_t b = B;
    //while (b > 0) {
    for (size_t j = 0; j < B; j++) {
      uint32_t r = distN(gen);
      //b -= r;
      w.push_back(r);
    }
    W[i] = w;
  }

  MultiArray<double, 2> C(N, N);
  MultiArray<double, 2> pC(N, N);

  for (size_t i = 0; i < N; i++) {
    for (size_t j = 0; j < N; j++) {
      pC[i][j] = tax.dist(W[i], T[j]);
      C[i][j] = tax.dist(W[i], T[j]);
    }
  }

  Hungarian H(C);
  uint32_t A[N];
  H.execute(A);

  Hungarian pH(pC);
  uint32_t pA[N];
  pH.execute(pA);

  double Q = 0.0;
  double pQ = 0.0;
  for (size_t i = 0; i < N; i++) {
    Q += tax.dist(W[i], T[A[i]]);
    Q += tax.dist(W[i], T[pA[i]]);
  }
  Q /= N;
  pQ /= N;
  
  std::cout << N << "," << Q << "," << pQ << std::endl;

  return 0;
}
