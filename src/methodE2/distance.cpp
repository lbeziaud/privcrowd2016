#include <array>

namespace distance {
  typedef double (*distance) (std::array<bool, D> u, std::array<bool, D> v);

double dice(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0, ndiff = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ndiff += (x != y);
  }
  return ndiff / (2. * ntt + ndiff);
}

double jaccard(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  int num = 0, denom = 0;
  size_t i;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    num += (x != y);
    denom += x | y;
  }
  return (double)num / denom;
}

double kulsinski(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0, ndiff = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ndiff += (x != y);
  }
  return ((double)ndiff - ntt + n) / ((double)ndiff + n);
}

double hamming(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i, s = 0;

  for (i = 0; i < n; i++) {
    s += (u[i] != v[i]);
  }
  return (double)s / n;
}

double rogerstanimoto(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0, ndiff = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ndiff += (x != y);
  }
  return (2. * ndiff) / ((double)n + ndiff);
}

double russellrao(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0;

  for (i = 0; i < n; i++) {
    ntt += (u[i] != 0) & (v[i] != 0);
  }
  return (double)(n - ntt) / n;
}

double sokalmichener(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0, ndiff = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ndiff += (x != y);
  }
  return (2. * ndiff) / ((double)ndiff + n);
}

double sokalsneath(std::array<bool, D> u, std::array<bool, D> v) {
  size_t n = u.size();
  size_t i;
  int ntt = 0, ndiff = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ndiff += (x != y);
  }
  return (2. * ndiff) / (2. * ndiff + ntt);
}

double yule(std::array<bool, D> u, std::array<bool, D> v) { 
  size_t n = u.size();
  size_t i;
  int ntt = 0, nff = 0, nft = 0, ntf = 0;

  for (i = 0; i < n; i++) {
    bool x = (u[i] != 0), y = (v[i] != 0);
    ntt += x & y;
    ntf += x & (!y);
    nft += (!x) & y;
  }
  nff = n - ntt - ntf - nft;
  return (2. * ntf * nft) / ((double)ntt * nff + (double)ntf * nft);
}
}
