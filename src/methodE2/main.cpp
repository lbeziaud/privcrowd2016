#include <iostream>
#include <vector>
#include <array>
#include <random>
#include <algorithm>

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "dataanalysis.h"

#define N 10000
#define D 600

#include "distance.cpp"

int main(int argc, char **argv) {
  distance::distance dist = distance::hamming;
       
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution b(0.25);

  std::array<std::array<bool, D>, N> xy;
#pragma omp parallel for
  for (size_t i = 0; i < N; i++) {
#pragma omp parallel for
    for (size_t j = 0; j < D; j++) {
      xy[i][j] = b(gen);
    }
  }

  alglib::real_2d_array d;
  d.setlength(N, N);
#pragma omp parallel for
  for (size_t i = 0; i < N; i++) {
#pragma omp parallel for
    for (size_t j = 0; j < i; j++) {
      d(i, j) = dist(xy[i], xy[j]);
    }
  }  
  
  alglib::clusterizerstate s;
  alglib::ahcreport rep;

  alglib::clusterizercreate(s);
  alglib::clusterizersetdistances(s, d, true);
  alglib::clusterizerrunahc(s, rep);

  printf("%s\n", rep.z.tostring().c_str());
  printf("%s\n", rep.p.tostring().c_str());
  printf("%s\n", rep.pm.tostring().c_str());

  return 0;
}

#undef N
#undef D
