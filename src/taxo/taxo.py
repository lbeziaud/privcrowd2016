from itertools import product
from math import ceil, log
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats, integrate
import seaborn as sns
sns.set(color_codes=True)

@np.vectorize
def height(k, i):
    return ceil(log((k - 1) * i + 1, k))


@np.vectorize
def lca(k, i, j):
    def parent(i):
        return ceil((i - 1) / float(k))

    h1 = height(k, i)
    h2 = height(k, j)

    if h1 > h2:
        h1, h2 = h2, h1
        i, j = j, i

    dh = h2 - h1
    for h in range(dh):
        j = parent(j)

    while i > 0 and j > 0:
        if i == j:
            return i
        i = parent(i)
        j = parent(j)

    return 0


def a():
    k = 4
    h = 4
    n = int((pow(k, h + 1) - 1) / (k - 1))

    a = np.fromfunction(lambda i, j: ((h + 1) - height(k, lca(k, i, j))) / (h + 1), (n, n), dtype=int)
    b = height(k, np.arange(n))

    plt.suptitle('k={}, h={}'.format(k, h))

    plt.subplot(1, 2, 1)
    plt.title('d(v, w)')
    plt.hist(a.ravel(), alpha=0.4)

    plt.subplot(1, 2, 2)
    plt.title('h(v)')
    plt.hist(b.ravel(), alpha=0.4)

    #plt.imshow(a, cmap="viridis", interpolation='none')

    #plt.yscale('log', basey=10)
    #plt.legend(loc='upper left', bbox_to_anchor=(1,1))

    plt.show()


def b():
    kmax = 4
    hmax = 4

    #plt.suptitle('density of the normalized skill distance distribution in a k-ary taxonomy of depth h')
    
    i = 0
    for k in range(2, kmax+1):
        for h in range(2, hmax+1):
            i += 1
            
            n = int((pow(k, h + 1) - 1) / (k - 1))
            a = np.fromfunction(lambda i, j: ((h+1) - height(k, lca(k, i, j)))/(h+1), (n, n), dtype=int).ravel()
            
            plt.subplot(kmax-1, hmax-1, i)
            plt.title('k={}, h={}'.format(k, h))
            #plt.ylim([0,1])
            plt.xlim([0,1])
            #plt.xlabel('distance')
            #plt.ylabel('density')

            #density, bins = np.histogram(a, density=True)
            #unity_density = density / density.sum()
            #widths = bins[:-1] - bins[1:]
            #plt.bar(bins[:-1], unity_density, width=widths, color='black')
            sns.distplot(a, kde=True)

    plt.tight_layout()
    plt.show()


def c():
    kmax = 4
    hmax = 4
 
   fig, axes = plt.subplots(nrows=2, ncols=3)
    fig.suptitle('((h - depth(lca(p, q))) / h) for each (p, q) in a k-ary tree of depth h')
    
    for ax, (k, h) in zip(axes.flat, product(list(range(2, kmax+1)), list(range(2, hmax+1)))):
        n = int((pow(k, h + 1) - 1) / (k - 1))
        a = np.fromfunction(lambda i, j: ((h+1) - height(k, lca(k, i, j)))/(h+1), (n, n), dtype=int)

        ax.set_title('k={}, h={}'.format(k, h))
        im = ax.imshow(a, cmap="viridis", interpolation='none', vmin=0, vmax=1)

    #fig.subplots_adjust(right=0.3, top=0.3)
    #fig.colorbar(im)

    plt.tight_layout()
    plt.show()
    

def d():
    kmax = 4
    h = 4

    #plt.suptitle('density of the normalized skill distance distribution in a k-ary taxonomy of depth h')

    fig, ax = plt.subplots()
    
    i = 0
    for k in range(2, kmax+1):
        i += 1
            
        n = int((pow(k, h + 1) - 1) / (k - 1))
        x = np.fromfunction(lambda i, j: ((h+1) - height(k, lca(k, i, j)))/(h+1), (n, n), dtype=int).ravel()
            
        sns.distplot(x, kde=False, fit=stats.gamma, ax=ax)

    sns.plt.show()
            

k = 2
h = 2

c()
