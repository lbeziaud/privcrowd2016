#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define H 10
#define K 10

#define N ((pow(K, H+1)-1)/(K-1))

#define LOGB(x, b) (log(x) / log(b))

#define CHILD(i, j) (K * i + j)
#define PARENT(i) (ceil(i - 1) / (float)K)

#define HEIGHT(i) (ceil(LOGB((K - 1) * i + 1, K)))

#define DIST(i, j) ((H - HEIGHT(lca(i, j)) + 1) / (float)(H))

int lca(int i, int j) {
  int h1 = HEIGHT(i);
  int h2 = HEIGHT(j);
  if (h1 > h2) { // does not append
    int tmp = h1; h1 = h2; h2 = tmp;
    tmp = i; i = j; j = tmp;
  }
  int dh = h2 - h1;
  for (int h = 0; h < dh;  h++) {
    j = PARENT(j);
  }
  while (i > 0 && j > 0) {
    if (i == j) return i;
    i = PARENT(i);
    j = PARENT(j);
  }
  return 0;
}

int main(int argc, char **argv) {
  for (int i = 1; i <= N; i++) {
    for (int j = i; j <= N; j++) {
      printf("%0.3f ", DIST(i, j));
    }
    printf("\n");
  } 
  return 0;
}
