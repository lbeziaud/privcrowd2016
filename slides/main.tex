\documentclass{beamer}

\usepackage[utf8]{inputenc}

\usepackage[english]{babel}

\usecolortheme{rose}

\usepackage{amsmath,mathtools}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,angles,quotes}
\usetikzlibrary{fit}

\usepackage{subcaption}

\usepackage{wrapfig}
\usepackage{graphicx}

\usepackage{pgfplots}
\pgfplotsset{compat=1.7}

\renewcommand{\epsilon}{\varepsilon}

\AtBeginSection[]
{
  \begin{frame}<beamer>[noframenumbering,plain]
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\title{Task Assignment in Crowdsourcing\\
  under Differential Privacy}
%through Randomized Response}
\author[Louis Béziaud]{Louis Béziaud\inst{1,}\inst{2}\\\vspace{1em}
%  {\footnotesize \url{louis.beziaud@ens-rennes.fr}}\\
  {\footnotesize Supervised by}\\\vspace{1em}
  {Tristan Allard\inst{1,}\inst{3}}\and {David Gross-Amblard\inst{1,}\inst{3}}
}
\institute{
  {\inst{1,} Univ. Rennes 1}\hspace{1em}
  {\inst{2} ENS Rennes}\hspace{1em}
  {\inst{3} IRISA}
}
\date{Mai -- July, 2016}

\setbeamersize{text margin left=10pt,text margin right=10pt}

\usepackage[
  backend=biber,
  style=alphabetic,
]{biblatex}
\bibliography{../junk/refs/main.bib}
\renewcommand*{\bibfont}{\scriptsize}
%\setbeamertemplate{bibliography item}{}
\setbeamertemplate{bibliography item}{\insertbiblabel}

\usepackage{csquotes}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}

\hypersetup{pdfencoding=auto,psdextra}

\begin{document}

\begin{frame}[noframenumbering,plain]
  \maketitle
\end{frame}

\section*{Introduction}

\begin{frame}{Introduction -- Crowdsourcing}
  \begin{block}{Crowdsourcing}
    \textquote[]{Crowdsourcing represents the act of a company or institution taking a function once performed by employees and \textbf{outsourcing} it to an undefined (and generally \textbf{large}) network of people in the form of an \textbf{open call}.}\footnote{Jeff Howe, Wired Magazine, 2006}
    \begin{itemize}
    \item Wikipedia, Galaxy Zoo, Amazon Mechanical Turk, \dots
    \end{itemize}
  \end{block}

  \begin{block}{Knowledge-Intensive Crowdsourcing~\cite{BasuRoy2015}}
    Some tasks require particular \textbf{skills} to be completed
    \begin{itemize}
    \item a translation from English to French
    \end{itemize}
    The platform \textbf{assigns} tasks to workers.
  \end{block}
\end{frame}

\begin{frame}{Introduction -- Privacy}
  \begin{block}{Knowledge \(\implies\) Privacy Issues}
    Features can be \textbf{quasi-identifiers} / \textbf{sensitive data} :
    \begin{itemize}
    \item unique skill, location, availability, \dots
    \end{itemize}
    The platform is \textbf{not} a \textbf{trusted third party}
  \end{block}

  \begin{alertblock}{Example of Data Breach}
    A worker ID on mTurk gives access to the Amazon profile: real name, wish lists, book reviews, tagged products, \dots~\cite{LeaseHullmanBighamEtAl2013}
  \end{alertblock}

  \begin{block}{Privacy-Accuracy Trade-off \cite{Hay:2016:PED:2882903.2882931}}
    Entities want to \textbf{optimize} the assignment in a \textbf{privacy-preserving} way
  \end{block}
\end{frame}

\begin{frame}{Introduction -- Related Work}
  \begin{exampleblock}{Related Work}
    \begin{itemize}
    \item Privacy-aware spatial CS~\cite{ToGhinitaShahabi2014} and surveys~\cite{KandappuSivaramanFriedmanEtAl2014}
      \begin{itemize}
      \item no assignment or skills
      \end{itemize}
    \item Private assignment~\cite{Kajino2016} through homomorphic \textbf{encryption}
      \begin{itemize}
      \item more than \textbf{century} needed to assign 100 tasks to 100 workers
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
  \begin{block}{Observation}
    \begin{itemize}
    \item No general task assignment
    \item Full encryption \(\implies\) unrealistic run-time
    \item What can we achieve \textbf{without encryption}\,?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[noframenumbering,plain]
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Requirements}

\subsection{Crowdsourcing}

\begin{frame}{Crowdsourcing -- Global view}
  \begin{figure}
    \centering
    \includegraphics[height=7cm]{crowdsourcing.png}
  \end{figure}
\end{frame}

\begin{frame}{Crowdsourcing -- Task Assignment}
  \begin{block}{Input}
    \begin{itemize}
    \item workers \(\mathcal{W} = \{w_1, w_2, \dots, w_n\}\), tasks \(\mathcal{T} = \{t_1, t_2, \dots, t_n\}\)
    \item wlog. \(n = |\mathcal{W}| = |\mathcal{T}|\)
    \item \textbf{cost} function \(C\colon \mathcal{W}\times\mathcal{T}\to\mathbb{R}\) 
    \end{itemize}
  \end{block}
  
  \begin{block}{Output}
    \begin{itemize}
    \item assignment \(\mathcal{A} = \{(w_i, f(w_i)) \mid w_i \in \mathcal{W}\}\),
      \(f\) bijection \(\mathcal{W} \to \mathcal{T}\)
    \item \textbf{minimize} \(\sum_{w_i, t_j \in \mathcal{A}} C(w_i, t_j)\) 
    \end{itemize}
  \end{block}

  \begin{block}{Algorithm}
    Hungarian algorithm~\cite{NAV:NAV3800020109}, \textbf{optimal}, \(\mathcal{O}(n^3)\)
  \end{block}
\end{frame}

\begin{frame}{Crowdsourcing -- Skills}
  \begin{figure}
    \centering
    \includegraphics[height=5cm]{task_assignment.png}
  \end{figure}
  
  \begin{block}{Binary Skills}
    \begin{itemize}
    \item skills vector \(s = \langle s_{1}, s_{2}, \dots, s_{m}\rangle \in \{0,1\}^m\)
    \item has skill \(j\) (\(s_{ij}=1\)) or not (\(s_{ij}=0\))
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Differential Privacy}

\begin{frame}{Differential Privacy~\cite{Dwork2006}}
  
  \begin{block}{Idea}
    The current \textbf{gold standard}.
    
    The \textbf{outcome of any analysis is essentially equally likely}, independent of whether an individual joins, or refrains from joining, the dataset
  \end{block}
  \begin{alertblock}{Definition}
    \(M\) gives \(\epsilon\)-differential privacy if for all pairs of data-sets \(x\), \(y\) differing in one element, and all subsets \(S\) of possible outputs, \[Pr[M(x) \in S] \leq e^\epsilon Pr[M(y) \in S]\]
  \end{alertblock}
  \begin{block}{Properties}
    \begin{itemize}
    \item sequential \& parallel \textbf{composition}~\cite{DBLP:conf/sigmod/McSherry09} \(\implies\) budget \(\epsilon\) sharing
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Problem Statement}

\begin{frame}{Problem Statement}
  \begin{alertblock}{Goal}
  We want to compute an \textbf{assignment} \(\mathcal{A}\) between workers \(\mathcal{W}\) and tasks \(\mathcal{T}\), \textbf{minimizing} the cost \(\sum_{w_i, t_j \in \mathcal{A}} C(w_i, t_j)\), being \(\epsilon\)-differentially \textbf{private}, \textbf{without} using encryption
  \end{alertblock}

  \begin{exampleblock}{Contributions}
    \begin{itemize}
    \item choice of a differentially private mechanism: Randomized Response
    \item three protocols for a privacy preserving assignment
    \end{itemize}
  \end{exampleblock}
\end{frame}

\subsection{Randomized Response}

\begin{frame}{Randomized Response~\cite{Warner1965}}
  \begin{block}{Idea}
    Participant can deny having given skill, even if he said the opposite
  \end{block}
  \begin{exampleblock}{Protocol}
    \begin{itemize}
    \item ``Do you have binary attribute \(b\)\,?''
      \begin{itemize}
      \item answer a \textbf{lie} (i.e. \(\neg\) truth) with probability of \(p < 0.5\)
      \item answer \textbf{truthfully} with probability of \(1 - p\)
      \end{itemize}
    \end{itemize}
  \end{exampleblock}

  \begin{block}{\sc Flip}
    \begin{itemize}
    \item apply randomized response to each skill
    \item (\(\#Skills \times \log \frac{1-p}{p}\))-differentially private
    \item no over-cost, participant-side
    \end{itemize}
  \end{block}
\end{frame}

\section{Contribution}

\subsection{{\sc FlipMatch}: Baseline Approach}

\begin{frame}{{\sc FlipMatch} -- Protocol}
  \begin{block}{Protocol}
    \begin{enumerate}
    \item Workers and tasks send a \textbf{flipped} skills vector
    \item The platform computes the assignment
    \end{enumerate}
  \end{block}
  \begin{exampleblock}{Example}
    participant having skills vector \(\langle 1, 0, 1, 0\rangle\) will send \(\langle 1, 1, 1, 0\rangle\)
  \end{exampleblock}
\end{frame}

\subsection{\sc GroupMatch}

\begin{frame}{{\sc GroupMatch} -- \emph{A Priori} Blurring}
  \begin{figure}[h]
    \centering
    \resizebox{!}{0.3\textheight}{%
      \begin{tikzpicture}[taxo/.style = {shape=rectangle,rounded corners,draw,align=center,top color=white,bottom color=black!10},auto,level 1/.style={sibling distance=8em},level 2/.style={sibling distance=4em}]]
    \node[taxo] {$\perp$}
    child { node[taxo] (Programming) {Programming}
      child { node[taxo] (Java) {Java} }
      child { node[taxo] (Haskell) {Haskell} }
    }
    child { node[taxo] (Language) {Language}
      child { node[taxo] (French) {French} }
      child { node[taxo] (English) {English} }
    };
    \node [align=left,anchor=west] at (-5.2, -1.5) (superskills) {\bf super-skills};
    \node [align=left,anchor=west] at (-5.2, -3) (subskills) {\bf sub-skills}; 
    \node [draw,dashed, fit=(subskills) (French) (English) (Java) (Haskell), inner sep=0.15cm] (subskills) {};
    \node [draw, dashed, fit=(superskills) (Language) (Programming), inner sep=0.15cm] (superskills) {};
      \end{tikzpicture}
      }
        \caption{Example of skills taxonomy.}
        \label{fig:taxonomy}
  \end{figure}
%
  \begin{exampleblock}{Example}
    \begin{itemize}
    \item \(\{Java, Haskell\} \to \langle 1, 1, 0, 0\rangle \xrightarrow{Group} \langle 2, 0\rangle \xrightarrow{Threshold} \langle 1, 0\rangle \xrightarrow{Flip} \langle 1,1\rangle\)
    \end{itemize}
  \end{exampleblock}
%
  \begin{block}{\sc GroupFlip}
    \begin{itemize}
    \item (\(\frac{1}{\#SubSkills}\times \log \frac{1-p}{p}\))-differentially private
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{{\sc GroupMatch} -- Protocol}
  \begin{block}{\sc GroupedThenRandomized}
    \begin{enumerate}
    \item Group the skills vector
    \item Apply {\sc Flip} to the grouped vector
    \end{enumerate}
  \end{block}
  \begin{block}{Protocol}
    \begin{enumerate}
    \item Workers and tasks send a {\sc GroupedThenRandomized} skill vector
    \item The platform computes the assignment
    \end{enumerate}
  \end{block}
\end{frame}

\subsection{{\sc ClusterMatch}}

\begin{frame}{{\sc ClusterMatch} -- \emph{A Posteriori} Bluring}
    \begin{block}{\sc RandomizedThenGrouped}
    \begin{enumerate}
    \item Apply {\sc Flip} to the skills vector
    \item Group the randomized skills vector
    \end{enumerate}
  \end{block}
  \begin{block}{Protocol}
    \begin{enumerate}
    \item Workers send a {\sc RandomizedThenGrouped} skill vector
    \item The platform computes \emph{clusters} of similar workers
    \item Tasks are sent to each clusters according to the requirements
    \item Task assignment is performed inside each clusters
    \end{enumerate}
  \end{block}
\end{frame}

\section{Evaluation}

\begin{frame}{Evaluation -- {\sc FlipMatch \& GroupMatch}}
%  \begin{figure}
%    \centering
%    \includegraphics[width=9cm]{benchmark.png}\\
%    dataset: Bernoulli 5\%, cost: Hamming \\
%    1000 workers, 1000 tasks, 100 skills
  %  \end{figure}
  \begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{!}{0.3\textheight}{
      \begin{tikzpicture}
        \begin{axis}[
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/groupmatch10_bern05_n1000_m100.data};
          \addlegendentry{\sc GroupMatch10};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/flipmatch_bern05_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.047159999999999994) (524,0.047159999999999994)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{Bernoulli (\(p=0.05\))}
    \label{fig:bern05}
  \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{!}{0.30\textheight}{
      \begin{tikzpicture}
        \begin{axis}[
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/groupmatch10_jester13_n1000_m100.data};
          \addlegendentry{\sc GroupMatch10};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/flipmatch_jester13_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.014586666666666668) (524,0.014586666666666668)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{\tt Jester 1.3}
    \label{fig:jester13}
  \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{!}{0.30\textheight}{
      \begin{tikzpicture}
        \begin{axis}[
            %          ymax=0.5,
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/groupmatch10_ml100k_n1000_m100.data};
          \addlegendentry{\sc GroupMatch};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {../article/data/flipmatch_ml100k_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.27360000000000006) (524,0.27360000000000006)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{\tt MovieLens 100K}
    \label{fig:ml100k}
  \end{subfigure}
  \end{figure}

  \begin{block}{Parameters}
    \begin{itemize}
    \item distance = Hamming
    \item \(n = 10000\)
    \item \(m_{\tt Jester} = 100\), \(m_{\tt MovieLens} = 1700\)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Evaluation -- {\sc ClusterMatch}}
    \begin{columns}[T]
      \begin{column}{0.45\textwidth}
        \begin{block}{Explored Clustering Algorithms}
          \begin{itemize}
          \item K-Means++~\cite{Arthur:2007:KAC:1283383.1283494}
          \item Fuzzy C-Means~\cite{dunn1973fuzzy}
          \item DBSCAN~\cite{Sander:1998:DCS:593419.593465}
          \item Hierarchicals~\cite{Rokach2005}
          \end{itemize}
        \end{block}
      \end{column}%
      \hfill%
      \begin{column}{0.45\textwidth}%
        \begin{block}{Explored Metrics}
          \begin{itemize}
          \item Jensen–Shannon
          \item Mahalanobis
          \item Cosine
          \item Parsec
          \end{itemize}
        \end{block}
      \end{column}
    \end{columns}
  
  \begin{alertblock}{Drawbacks}
    \begin{itemize}
    \item need a practicable metric for CS
    \item input parameters \(\implies\) needs diagnostic checks 
    \item curse of dimensionality
    \item dependency to data distribution
    \end{itemize}
  \end{alertblock}
\end{frame}

\section{Conclusion \& Future Work}

\begin{frame}{Conclusion}
  \begin{exampleblock}{Contributions}
    \begin{itemize}
    \item \textbf{first skill-aware differentially private task assignment w/o encryption}
    \item exploration show \textbf{utility/privacy trade-off} \& \textbf{data-dependency}
    \end{itemize}
  \end{exampleblock}
  
  \begin{block}{Future Work}
    \begin{itemize}
    \item absence of metric and dataset calls for \textbf{better standards}
    \item cleaning \textbf{flipped} assignment with \textbf{majority voting}\,?
    \item \textbf{encryption+perturbation hybrid} with other DP-mechanism\,?
    \end{itemize}
  \end{block}

  \begin{center}
    \Large\usebeamercolor[fg]{structure} Thank you!
  \end{center}
\end{frame}

\begin{frame}[t,allowframebreaks,noframenumbering,plain]
  \frametitle{References}
  \printbibliography
\end{frame}

\end{document}
