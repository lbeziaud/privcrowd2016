\documentclass[]{sig-alternate}

\usepackage[colorlinks]{hyperref}

\makeatletter
\def\@copyrightspace{\relax}
\makeatother
\setcopyright{acmcopyright}

\newtheorem{example}{Example}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\usepackage{tikz}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}

\title{Title}
\subtitle{Subtitle}

\numberofauthors{3}
\author{
\alignauthor
Louis B\'eziaud\\
    \affaddr{Universit\'e de Rennes 1}\\
    \affaddr{\'ENS Rennes}\\
    \email{louis.beziaud@ens-rennes.fr}
\alignauthor
Tristan Allard\\
    \affaddr{Universit\'e de Rennes 1}\\
    \affaddr{IRISA}\\
    \email{tristan.allard@irisa.fr}
\alignauthor
David Gross-Amblard\\
    \affaddr{Universit\'e de Rennes 1}\\
    \affaddr{IRISA}\\
    \email{david.gross-amblard@irisa.fr}
}

\usepackage[author={Louis B\'eziaud}]{pdfcomment} % draft version only

\begin{document}

\maketitle

\begin{abstract}
  
\end{abstract}

\category{A}{A}{A}

\terms{A}

\keywords{A}

\section{Introduction}

Crowdsourcing is a technique to outsource tasks to a large number of unspecified workers. It is a popular solution for executing large amount of tasks that require human intelligence. One example of crowdsourcing is Galaxy Zoo\footnote{\url{https://www.galaxyzoo.org}}, where workers voluntarily classify large numbers of galaxies by their own eyes to contribute to the development of science. There are also crowdsourcing services designed for general purpose such as Amazon Mechanical Turk\footnote{\url{https://www.mturk.com}}, with a pool of 500.000 workers, and CrowdFlower\footnote{\url{https://www.crowdflower.com}}, where workers get monetary rewards. These examples show that there exist a variety of crowdsourcing services that have different purposes and reward designs.

Many existing platforms are currently dealing with simple tasks, which do not require workers to have special features such as skills and other attributes. Recently, there has been growing interest in how to crowdsource feature-dependent tasks, which can be processed only by workers with specific features. For example, an English-French translation task requires both English and French skills. Since feature-dependent tasks include a number of practical tasks, an increasing number of studies have been conducted to enable us to handle such tasks to be easily processed via crowdsourcing.\pdfcomment{add citation}

One of the vital challenges a crowdsourcing platform specializing in feature-dependent tasks needs to overcome is to improve the throughput of the platform. The current practice of using open call assignments is not appropriate for this purpose in that a worker with a special skill may choose a simple task; thus a feature-dependent task requiring this worker's skill remains unassigned, which can be a sub-optimal assignment. An obvious approach to addressing this issue would be to compute optimal task assignment based on the feature sets of the workers and the feature requirements of the tasks. A platform with such knowledge would be able to globally maximize the throughput of its crowdsourcing system.

In this study, we first point out the privacy issues affecting both the workers and requesters in the task assignment strategy mentioned above. Workers are requested to report their skills such as language abilities and programming skills as well as their attributes such as their locations, minimum wages, and working hours, and they may even have to disclose additional information of a more personal nature. These features can be used to identify the workers, infer sensitive information about them, expose them to physical danger by revealing their location information, and introduce unfairness by excessively favoring highly skilled and hardworking workers. Furthermore, the privacy of requesters will also be compromised. Requesters have to report the corresponding feature requirements, which can be used to identify them and to reveal the contents of their tasks. Although several previous studies have investigated privacy issues surrounding task assignments, their applicability is limited because (i) they focus on location-based tasks and (ii) they aim to preserve the privacy of workers only. This indicates the need for the development of a task assignment system for general tasks in which the privacy of both workers and requesters is preserved.

\section{Related Work}

Although the problem of participants privacy in the crowdsourcing context has been deeply studied, none of the methods is usable in a general context. 

A differentially private platform is proposed in~\cite{KandappuSivaramanFriedmanEtAl2014}. However it is designed to surveys only and does not consider skills. The matching problem under differential privacy is not addressed. Furthermore the privacy of requesters is not taken into account. 

Several differentialy private recommender systems are available, such that~\cite{McSherryMironov2009}, but their context is fundamentally different from ours.

A differentially private framework for spatial crowdsourcing -- where the location of the participant is the main criteria for task assignment -- has been proposed in~\cite{ToGhinitaShahabi2014}.

A semi-distributed protocol based on cryptography is presented in~\cite{Kajino2016}. The problem of a private matching as we define it is fully addressed but the execution of a maximum flow algorithm through homomorphic encryption makes the scheme unusable in real life. A run-time complexity evaluation shows us that more than a century would be needed to compute an assignment between 100 workers and 100 tasks.

\section{Preliminaries}

The work described here is highly dependant of the chosen distance used to evaluate dissimilarities between a worker and a task. \textbf{We consider a slightly modified version of the Hamming distance defined as ???}

The \textit{matching distance} between a skill vector $s$ and a requirement vector $r$ is defined as $n_{diff}/n$ where $n_{diff}$ is the number of skills where $s_i \neq r_i$ and $n$ is the length of $s$ and $r$. The \textit{matching distance} is a normalized version of the Hamming distance.

\begin{definition}{Matching distance}
We consider a slightly modified version of the Hamming distance to evaluate the proximity between a worker $w \in \mathcal{W}$ and a task $t \in \mathcal{T}$ defined as the ratio between missing skills and requester skills. Formally, \[d(w, t) = |\{\neg w_i \land t_i\}| / |\{t_i\}| \]. This definition account for the obvious fact that a worker possessing more skills than asked for by a task is still qualified for this task.
\end{definition}
\begin{example} Consider the skills $\langle Java, Tomatoes, English\rangle$, two workers $w_1 = \langle 1, 0, 1 \rangle$ and $w_2 = \langle 0, 1, 1$, along with a task $t_1 = \langle 0, 0, 1\rangle$. We have $d(w_1, t_1) = \frac{0}{1} = 0 = d(w_2, t_1)$ since both workers possess the $English$ skill.
\end{example}
The defined matching distance is not a proper metric since it does not have the identity of indiscernibles, symmetry or subadditivity properties. 

\begin{definition}{Differential Privacy~\cite{Dwork2006}}
A randomized algorithm $\mathcal{M}$ is $\epsilon$-differentially private if for all $S \subset Range(\mathcal{M})$ and for all $x$, $y$ such that $\|x-y\|_1 \leq 1$
\begin{displaymath}
Pr[\mathcal{M}(x)\in S] \leq \exp(\epsilon) Pr[\mathcal{M}(y)\in S],
\end{displaymath}
where the probability space is over the coin flips of the mechanism $\mathcal{M}$.
\end{definition}

\subsection{Randomized Response}

The randomized response is an anonymization mechanism proposed by Warner in 1965 and later modified by Greenberg in 1969. It allows respondents to respond to sensitive issues while maintaining confidentiality.

We use a modified version of the original protocol which is still equivalent to it.

\begin{enumerate}
\item The interviewer asks a polar question (ie. yes--no question) to a respondent.
\item The respondent secretly flip a biased coin that produces head with probability $p$ and tails with probability $1-p$.
\item If the coin comes up heads, the respondent answers with a lie (ie. the opposite of the truth).
\item If the coin comes up tails, the respondent answers truthfully to the question.
\end{enumerate}

\begin{algorithm}
\DontPrintSemicolon
\KwIn{original skill profile $s$, flip probability $p$}
\KwOut{randomized skill profile $s'$}
$s' \gets [~]$\;
\For{$i \gets 1$ \textbf{to} $D$} {
  \uIf{$random() > p$} {
    $s'[i] \gets s[i]$\;
  }
  \Else{
    $s'[i] \gets \neg s[i]$\;
  }
}
\Return{$s'$}\;
\caption{{\sc Flip} a skill profile}
\label{algo:flip}
\end{algorithm}

\begin{theorem}{}
The randomized response mechanism is $\epsilon$-differentially private.
\end{theorem}

\begin{proof}
We have the following equation
\begin{displaymath}
\frac{Pr[s_i = 1| s'_i = 1]}{Pr[s_i = 1| s'_i = 0]} = \frac{1-q}{q}.
\end{displaymath}
Setting $\epsilon \leq \log\frac{1-q}{q}$ gives us $\epsilon$-differential privacy.

However must account for the number of skills $D$. Using the sequential composition property of the differential mechanism, we have $\epsilon \leq D \times \log\frac{1-q}{q}$.
\end{proof}

\section{Contribution}

\subsection{Lower Bound}

As shown by Kajino in \cite{Kajino2016}, the assignment problem is equivalent to a maximum flow problem, which is reducible to a minimum cut problem using the Max-flow min-cut theorem.

\begin{theorem}{\cite{GuptaLigettMcSherryEtAl2009}}
Any $\epsilon$-differentially private algorithm for min-cut must incur an expected additive $\Omega(\ln n/\epsilon)$ cost over OPT, for any $\epsilon \in (3 \ln n/n, 1/12)$.
\end{theorem}

\subsection{Anonymization level}

\subsubsection{Global anonymization}

The flipping probability $p_{flip} \in [0, 0.5)$ used by the {\sc Flip} is defined globally by the platform and is available to all the participants.

\subsubsection{User-chosen anonymization}

Each user chooses an anonymization level $a_l \in (0, 1)$ which gives the flipping probability $p_{flip} = a_l / 2$.

\subsubsection{User-computed anonymization}

\begin{definition}%{Kullback--Leibler divergence}
The Kullback--Leibler divergence (KLD) of $Q$ from $P$ is the amount of information lost when $Q$ is used to approximate $P$.
Formally, for discrete probability distributions P and Q, the KLD of Q from P is defined to be \[ D_KL(P \| Q) =  \sum_{i} P(i) \log \frac{P(i)}{Q(i)}. \]
The KLD is defined only if $Q(i)=0$ implies $P(i)=0$, for all $i$ (absolute continuity).
\end{definition}

Each user computes its anonymization level $a_l$ as the KLD between its own skills distribution and the global skills distribution of the platform. The more specialized (relatively to the average user) is a participant, the more randomized are its skills.

\subsubsection{Per-skill anonymization}

TODO

\subsection{Flip\&Match}

The {\sc Flip\&Match} is a direct application of the {\sc Flip} algorithm. The protocol follows a simple scheme. Each participant (workers and requesters) sends its randomized skills profile to the platform. The platform computes an assignment between workers and tasks using the Hungarian algorithm with the Hamming distance.  

\subsection{Flip\&Group\&Match}

\section{}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{flip_eps_vs_pb.png}
\caption{Caption}
\label{fig:flip_eps_vs_pb}
\end{figure}

\paragraph{flip with platform $\epsilon$}

% http://arxiv.org/pdf/1402.3329v1.pdf
Commonly accepted values for $\epsilon$ ranges from as little as 0.01 to as much as 7. As shown on figure \ref{fig:flip_eps_vs_pb}, we have to define $p \to \frac{1}{2}$ to provide an acceptable $\epsilon$ (ie. privacy level). A ``good'' lying probability renders the matching equivalent to a random matching

\paragraph{flip with user $\epsilon$}

We propose to let participants choose their flip probability using an anonymization level $a \in [0, 1]$. When $a=1$, the randomization will be maximal and $p \approx 0.5$, when $a=0$, the randomization won't have any effect and $p=0$. Formally, we define $p = \frac{a}{2}$.


\paragraph{flip with distribution $\epsilon$}

To help the user choose a good anonymization level $a$, we propose the following protocol. \begin{enumerate}
\item The workers compute the histogram $H$ of the skills distribution using homomorphic encryption. \textbf{details!}
\item Each worker computes the Jensen-Shannon distance between itself and the global distribution and uses it as the anonymization level $a = jsd(H, s)$.
\end{enumerate}

The intuition is that we propose to anonymize the ``special'' users more than the average ones.

The figure \ref{fig:flip_anon_level} applies these schemes to the Jester dataset. It illustrates the trade-off between privacy and utility.

\subsection{Flip}

This first method is a basic application of the randomized response protocol (flip). 

Each worker sends a randomized skill vector $s' = flip_q(s)$. Requesters sends a randomized requirement vector $r' = flip_p(r)$. The platform computes an assignment $A$ using these vectors and the \textit{matching distance}.

\subsection{HierarchyFlip}

\begin{figure}[h]
\centering
\begin{tikzpicture}[every node/.style = {shape=rectangle, rounded corners,
    draw, align=center, top color=white, bottom color=blue!20},auto,
    level 1/.style={sibling distance=10em},
    level 2/.style={sibling distance=5em}]]
  \node {$\perp$}
    child { node {Gardening} 
        child { node {Tomatoes} }
        child { node {Apples} }
    }
    child { node {Programming}
        child { node {Ocaml} }
        child { node {Haskell} }
    };
\end{tikzpicture}
\caption{Caption}
\label{fig:taxonomy}
\end{figure}

We improve the Flip with a grouping phase. An underlying skills taxonomy is used a hierarchy to group the skills in generalized categories.

\begin{example}{Generalization}
Consider the taxonomy described in \ref{fig:taxonomy}, and a worker having skills \textit{Haskell} and \textit{Tomatoes}. His skills profile will be encoded as the vector $\langle 1, 0, 0, 1\rangle$. The corresponding grouped vector will be $\langle 0.5, 0.5\rangle$.
\end{example}

\begin{example}{Correction}
\textbf{This is not working for now...}
\end{example}

\subsection{Flip\&Stack}

Each worker $w_i$ sends its randomized skill vector $s_i'$ to the platform which build a network as follow.

\begin{enumerate}
\item Let the set of stacks be $\mathcal{G} = {g_k | k \in [1, d]}$, where $g_k$ contains all workers having skill $k$ in its randomized skill vector.
\item Let the set of vertices be $V = \{s,t\} \cup \mathcal{T} \cup \mathcal{W} \cup \mathcal{G}$, where $s$ is the source and $t$ is the sink.
\item Let the set of edges be $E = E_1 \cup E_2 \cup E_3 \cup E_4$, in which \begin{align*}
E_1 &= \{(s, w_j) \mid w_j \in \mathcal{W}\}, \\
E_2 &= \{(w_j, g_k) \mid w_j \in \mathcal{W}, g_k \in \mathcal{G}\}, \\
E_3 &= \{(g_k, t_i) \mid g_k \in \mathcal{G}, t_i \in \mathcal{T}\}, \\
E_4 &= \{(t_i, t) \mid t_i \in \mathcal{T}\}.
\end{align*}
\item We set the capacity of each edge $e \in E$ as \begin{align*}
E_1 \ni c_{s, w_j} &= k, \\
E_2 \ni c_{w_j, g_k} &= \begin{cases}
    \frac{k}{out(w_j)} &(w_j \in g_k) \\
    0 &(w_j \not\in g_k)
\end{cases}, \\
E_3 \ni c_{g_k, t_i} &= \begin{cases}
    \frac{in(g_k)}{out(g_k)} &(g_k \geq t_i) \\
    0 &(g_k < t_i)
\end{cases}, \\
E_4 \ni c_{t_i, t} &= k,
\end{align*}
where $out(e)$ (resp. $in(e)$) is the number of edges going out (resp. in) from vertex $v$.
\item Let $f_{u, v}$ be the maximum flow of the described network between $s$ and $t$.
\item Let $\mathcal{A}' = \{f_{w_j, g_k} > 0, f_{g_k, t_i} > 0\}$ be a temporary assignment. 
\item A set of ``gold questions'' is sent to each worker assigned to as specific task by the requester.
\item The answers to these questions is used by the requester to choose one worker $w_j$ to execute the task $t_i$. The final assignment is $\mathcal{A} = \{(w_j, t_i)\}$.
\end{enumerate}

\begin{algorithm}
\DontPrintSemicolon
\KwIn{integer flow $F$}
\KwOut{assignment $\mathcal{A}$}
$\mathcal{A} \gets \emptyset$\;
\For{\textbf{each} $w_j \in \mathcal{W}$ \textbf{and} $t_i \in \mathcal{T}$} {
  \If{$f_{w_j, t_i} > 0$} {
    $\mathcal{A} \gets \mathcal{A} \cup \{(w_j, t_i)\}$\;
  }
}
\Return{$max$}\;
\caption{{\sc Max} finds the maximum number}
\label{algo:max}
\end{algorithm}

\subsection{Generalization}

\subsection{Clustering}

\subsection{Laplace noise}

\subsection{Post-correction}

\section{Experimental Setup}

\subsection{Datasets}

We use four datasets to evaluate our algorithms. Two real datasets, {\tt jester13} and {\tt ml100k}, and two synthetic datasets, {\tt bern05} and {\tt bern50}.

The {\tt jester13} dataset is a modified version of the Jester{\bf source} ratings dataset of 24.938 users who have rated between 15 and 35 jokes among 100 jokes. We view rated jokes as a skill (and non-rated jokes as the absence of it).

\begin{figure}[h]
\centering
\includegraphics[width=0.49\linewidth]{jester_histogram_skills.png}
\includegraphics[width=0.49\linewidth]{jester_histogram_users.png}
\label{fig:jester_histograms}
\caption{Jester dataset}
\end{figure}

\subsection{Algorithms}

\subsection{Implementation Details}

\section{Evaluation}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{flip_anon_level.png}
\caption{Jester, $N=1000$}
\label{fig:flip_anon_level}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{flipmatch_bernoulli.png}
\caption{Caption}
\label{fig:flipmatch_bernoulli}
\includegraphics[width=\linewidth]{flipmatch_jester.png}
\caption{Caption}
\label{fig:flipmatch_jester}
\includegraphics[width=\linewidth]{flipmatch_movielens.png}
\caption{Caption}
\label{fig:flipmatch_movielens}
\end{figure}

\section{Discussion}

We have proposed multiple schemes to approximate the assignment problem under differential privacy in a general crowdsourcing context.

Using background knowledge, it is possible to deploy better techniques. Indeed, the quality evaluation is deeply connected to both the underlying distribution of skills on the platform and to the chosen metric.

\section{Acknowledgments}

\bibliographystyle{abbrv}
\bibliography{refs}

\appendix
\section{Kajino}
\subsection{}

\end{document}