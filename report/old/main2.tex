\documentclass[draft]{sig-alternate}

\makeatletter
\def\@copyrightspace{\relax}
\makeatother
\setcopyright{acmcopyright}

\AtBeginDocument{%
\let\phi\varphi
\let\epsilon\varepsilon
}

\title{Title}
\subtitle{Subtitle}

\numberofauthors{3}
\author{
\alignauthor Louis~B\'eziaud\\
    \affaddr{Univ.~Rennes~1}\\
    \affaddr{ENS~Rennes}\\
    \email{louis.beziaud@ens-rennes.fr}
\alignauthor Tristan~Allard\\
    \affaddr{Univ.~Rennes~1}\\
    \affaddr{IRISA}\\
    \email{tristan.allard@irisa.fr}
\alignauthor David~Gross-Amblard\\
    \affaddr{Univ.~Rennes~1}\\
    \affaddr{IRISA}\\
    \email{david.gross-amblard@irisa.fr}
}

\newdef{definition}{Definition} 

\usepackage{tikz}

\usepackage[linesnumbered,ruled,vlined]{algorithm2e}

\begin{document}

\maketitle

\section{Introduction}

Crowdsourcing is a technique to outsource tasks to a large number of unspecified workers. It is a popular solution for executing large amount of tasks that require human intelligence. One example of crowdsourcing is Galaxy Zoo\footnote{\url{https://www.galaxyzoo.org}}, where workers voluntarily classify large numbers of galaxies by their own eyes to contribute to the development of science. There are also crowdsourcing services designed for general purpose such as Amazon Mechanical Turk\footnote{\url{https://www.mturk.com}}, with a pool of 500.000 workers, and CrowdFlower\footnote{\url{https://www.crowdflower.com}}, where workers get monetary rewards. These examples show that there exist a variety of crowdsourcing services that have different purposes and reward designs.

Many existing platforms are currently dealing with simple tasks, which do not require workers to have special features such as skills and other attributes. Recently, there has been growing interest in how to crowdsource feature-dependent tasks, which can be processed only by workers with specific features. For example, an English-French translation task requires both English and French skills. Since feature-dependent tasks include a number of practical tasks, an increasing number of studies have been conducted to enable us to handle such tasks to be easily processed via crowdsourcing.

One of the vital challenges a crowdsourcing platform specializing in feature-dependent tasks needs to overcome is to improve the throughput of the platform. The current practice of using open call assignments is not appropriate for this purpose in that a worker with a special skill may choose a simple task; thus a feature-dependent task requiring this worker's skill remains unassigned, which can be a sub-optimal assignment. An obvious approach to addressing this issue would be to compute optimal task assignment based on the feature sets of the workers and the feature requirements of the tasks. A platform with such knowledge would be able to globally maximize the throughput of its crowdsourcing system.

In this study, we first point out the privacy issues affecting both the workers and requesters in the task assignment strategy mentioned above. Workers are requested to report their skills such as language abilities and programming skills as well as their attributes such as their locations, minimum wages, and working hours, and they may even have to disclose additional information of a more personal nature. These features can be used to identify the workers, infer sensitive information about them, expose them to physical danger by revealing their location information, and introduce unfairness by excessively favoring highly skilled and hardworking workers. Furthermore, the privacy of requesters will also be compromised. Requesters have to report the corresponding feature requirements, which can be used to identify them and to reveal the contents of their tasks. Although several previous studies have investigated privacy issues surrounding task assignments, their applicability is limited because (i) they focus on location-based tasks and (ii) they aim to preserve the privacy of workers only. This indicates the need for the development of a task assignment system for general tasks in which the privacy of both workers and requesters is preserved.

\section{Related Work}

However, these solutions are not applicable since they (i) do not protect both the requesters and workers, and (2) are focused on specific crowdsourcing applications such as such as a \textit{privacy-aware framework for spatial crowdsourcing} (ToGhinitaShahabi2014) which enables the participation of workers without compromising their location privacy, or a \textit{platform for crowdsourcing surveys in a privacy conscious way} (KandappuSivaramanFriedmanEtAl2014).

A \textit{private task assignment protocol} (Kajino2016) was proposed by Kajino in his thesis published in 2016. The full assignment protocol is executed inside a Pallier cryptosystem to avoid any privacy leak. While this method allow Kajino to avoid the utility-privacy trade-off, the homomorphic encryption cause the method to be unsuable in real-life. A simple run-time analysis shows that more than a century would be needed to compute an assignment between 100 workers and tasks.

A \textit{privacy-preserving recommender system} (McSherryMironov2009).

\section{Preliminaries}

\subsection{Data Model}
We have a set of workers $\mathcal{W}=\{w_1,w_2,\dots,w_n\}$, a set of skills $\mathcal{S}=\{s_1,s_2,\dots,s_m\}$, and a set of tasks $\mathcal{T}=\{t_1,t_2,\dots,t_l\}$.

\paragraph{Skills} A skill is the knowledge on a particular topic, quantified as a binary relation of membership. It is associated with workers and tasks. When associated with a worker, it represents the worker's expertise of a topic. When associated with a task, a skill represents the minimum requirement for that task. A value of 0 for a skill reflects no expertise of a worker for that skill. For a task, 0 reflects no requirement for that skill. 

\paragraph{Workers} Each worker $w\in\mathcal{W}$ has a profile, that is, a vector, $\langle w_{s_1},w_{s_2},\dots,w_{s_m}\rangle$, of length $m$ describing its $m$ skills in $\mathcal{S}$. Skill $w_{s_i}\in\{0,1\}$ is the ownership indicator of worker $u$ for skill $s_i$.

\paragraph{Tasks} A task $t\in\mathcal{T}$ is characterized by a vector, $\langle r_{s_1},r_{s_2},\dots,r_{s_m}\rangle$ of length $m$, which reflects the task's required skills.

\subsection{Objective}
Given a set $\mathcal{T}$ of tasks and a set $\mathcal{W}$ of workers, the objective is to perform worker-to-task assignment for all tasks in $\mathcal{T}$, such that the overall task quality is maximized and the cost is minimized.

\subsection{Differential Privacy}

Differential privacy, surveyed in, is a relatively recent privacy definition based on the principle that the output of a computation should not allow inference about any record's presence in or absence from the computation's input. Formally, it requires that for any outcome of a randomized computation, that outcome should be nearly equally likely with and without any one record. We say two data sets $A$ and $B$ are adjacent, written $A\approx B$, if there is exactly one record in one but not in the other.

\begin{definition}{Differential Privacy}
A randomized mechanism $M$ satisfies $\epsilon$-differential privacy if for any adjacent data sets $A$ and $B$, and any subset $S$ of possible outcomes $\mathop{Range}(M)$, \[ \mathop{Pr}[M(A)\in S] \leq \exp(\epsilon) \times \mathop{Pr}[M(B)\in S]. \]
\end{definition}

When $A\approx B$, differential privacy bounds the update to the prior by a factor of $\exp(\epsilon)$, limiting the degree of inference possible about slight differences in the input data sets. Specifically, inference about the presence or absence (and consequently the value of) any single record is bounded by a factor of $\exp(\epsilon)$.

\subsection{Anonymization Mechanisms}

Randomize response is an anonymization mechanism proposed by Warner in 1965 and later modified by Greenberg in 1969. It allows respondents to respond to sensitive issues while maintaining confidentiality.

We use a modified version of the original protocol which is still equivalent to it.

\begin{enumerate}
\item The interviewer asks a polar question (ie. yes--no question) to a respondent.
\item The respondent secretly flip a biased coin that produces head with probability $p$ and tails with probability $1-p$.
\item If the coin comes up heads, the respondent answers with a lie (ie. the opposite of the truth).
\item If the coin comes up tails, the respondent answers truthfully to the question.
\end{enumerate}

\begin{algorithm}[h]
\SetKwFunction{random}{random}
\DontPrintSemicolon
\KwIn{skills profile $s=\langle s_1,s_2,\dots,s_m\rangle$, privacy parameter $\epsilon\in\mathbb{R}_{>0}$}
\KwOut{randomized skills profile $s'$}
$p \gets (1 + \exp(a/d))^{-1}$\;
$s'\gets \langle 0\rangle^m$\;
\For{$i\gets 1$ \KwTo $m$}{
    \uIf{$\random() > p$}{
        $s_i'\gets s_i$
    }
    \Else{
        $s_i'\gets \neg s_i$
    }
}
\Return{$s'$}\;
\caption{{\sc Flip}}
\label{algo:flip}
\end{algorithm}

\begin{table}[h]
\centering 
\caption{Skills profile randomization example} 
\begin{tabular}{|r|c|c|c|c|} 
\hline
Skill & Tomatoes & Apples & Ocaml & Python\\
\hline
Original & 1 & 0 & 1 & 0\\
\hline
Randomized & 1 & 1 & 1 & 0\\
\hline
\end{tabular} 
\end{table} 

\subsection{Distances Functions}

\begin{definition}{Hamming distance}
The Hamming distance between two vectors $a=\langle a_1,a_2,\dots,a_m\rangle$ and $b=\langle b_1,b_2,\dots,b_m\rangle$ of lengths $m$ is defined as
\end{definition}

\section{Privacy Level}

The choice of a value for $\epsilon$ has a direct impact on both the utility and privacy of the data. We propose three different schemes to select this value.

\subsection{Global} The classic approach is to set the $\epsilon$ globally, on the platform level, and to make it publicly available. While this approach is well fitted for data publication, it is disputable in our specific context. A public $\epsilon$ can be used in data mining to compute error bounds and estimate distributions. This feature is not interesting in our case. A global and unique value ???

\subsection{User} We can let each user choose its privacy level. The global $\epsilon$ of the platform is then the maximal value but this is only accounting for the worst case. 

\subsection{Computed} 

\subsection{Random} The privacy level can be randomly chosen from a random distribution. Using a random flipping probability $p\in(0,1)$ will provide a user privacy level $\epsilon_u\in(m\times\log\frac{1-p}{p})$. 
% uniform + distribution

\section{FlipMatch} This first protocol is a straightforward usage of the randomized response. Each user anonymize its skills profile using the Flip algorithm before sending it to the platform. The platform then computes an assignment using the Hungarian algorithm.

\begin{enumerate}
    \item Each worker $w_i\in\mathcal{W}$ computes its randomized skills profile $w_i'={\tt Flip}(w_i)$ and sends it to the platform.
    \item Each task $t_i\in\mathcal{T}$ computes its randomized skills profile $t_i'$ using the {\tt Flip} algorithm and sends it to the platform.
    \item The platform computes the cost matrix $C=(c_{ij})^{N\times N}$ where $c_{ij}=\mathop{Hamming}(w_i', t_j')$.
    \item The platform computes the assignment $\mathcal{A}$ using the {\tt Hungarian} algorithm.
    \item The assignment $\mathcal{A}$ is sent to both the workers and tasks using the {\tt Publish} protocol.
\end{enumerate}

\section{GroupMatch} This second protocol uses an underlying taxonomy to group related skills and reduce the noise effect.

\begin{figure}[h]
\centering
\begin{tikzpicture}[every node/.style = {shape=rectangle, rounded corners,
    draw, align=center, top color=white, bottom color=white!20},auto,
    level 1/.style={sibling distance=10em},
    level 2/.style={sibling distance=5em}]]
  \node {$\perp$}
    child { node {Gardening} 
        child { node {Tomatoes} }
        child { node {Apples} }
    }
    child { node {Programming}
        child { node {Ocaml} }
        child { node {Python} }
    };
\end{tikzpicture}
\caption{Example of a skills hierarchy}
\label{fig:taxonomy}
\end{figure}

\begin{algorithm}[h]
\SetKwFunction{Flip}{Flip}
\SetKwFunction{Normalize}{Normalize}
\DontPrintSemicolon
\KwIn{randomized skills profile $s'$, skills groups $G=\langle g_1,g_2,\dots,g_m\rangle$}
\KwOut{grouped randomized skills profile $s''$}
$s''\gets \langle 0\rangle^m$\;
\For{$i\gets 1$ \KwTo $m$}{
    \ForEach{$s'_i \in g_i$}{
        \If{$s'_i$}{
            $s'' \gets s'' + 1$
        }
    }
}
\Normalize(s'')\;
\Return{$s''$}\;
\caption{{\sc Flip}}
\label{algo:groupflip}
\end{algorithm}

\section{ClustMatch} 

\section{Evaluation}

\begin{figure}[h]
\centering
\includegraphics[width=0.49\linewidth]{empty.png} % distribution #workers having #skills
\includegraphics[width=0.49\linewidth]{empty.png} % distribution #skills having #workers
\caption{Jester dataset decription}
\label{fig:jester_descr}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.49\linewidth]{empty.png} % distribution #workers having #skills
\includegraphics[width=0.49\linewidth]{empty.png} % distribution #skills having #workers

\includegraphics[width=0.49\linewidth]{empty.png} % distribution #categories having #workers
\includegraphics[width=0.49\linewidth]{empty.png} % distribution #workers having #categories
\caption{MovieLens dataset decription}
\label{fig:movielens_descr}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{empty.png} % flip hung priv uniq, priv rand, priv, (priv data?)
\caption{Privacy level}
\label{fig:priv_lvl}
\end{figure}

\begin{figure*}[h]
\centering
\includegraphics[width=0.24\linewidth]{empty.png} % flip hung jester
\includegraphics[width=0.24\linewidth]{empty.png} % flip hung jester
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\caption{FlipMatch}
\label{fig:flipmatch}
\end{figure*}

\begin{figure*}[h]
\centering
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\caption{GroupMatch}
\label{fig:groupmatch}
\end{figure*}

\begin{figure*}[h]
\centering
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\includegraphics[width=0.24\linewidth]{empty.png}
\caption{ClustMatch}
\label{fig:clustmatch}
\end{figure*}

\section{Discussion}

\section{Conclusion}

\bibliographystyle{abbrv}
\bibliography{refs}

\end{document}