import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 0.5, num=500)
#y = 1/(np.exp(x+1))
y = np.log((1-x)/x)

#print(x)

fig, ax = plt.subplots()

#ax.set_xscale("log", nonposx='clip')
#ax.set_yscale("log", nonposy='clip')

ax.set_xlabel("flip probability")
ax.set_ylabel("privacy budget")

ax.plot(x, y, color='black')

plt.show() 
