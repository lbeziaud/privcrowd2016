\documentclass[english,11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{fullpage}
%\usepackage[subtle]{savetrees}

\usepackage[dvipsnames]{xcolor}
\usepackage{wrapfig}
\usepackage{graphicx}
%\usepackage{msc}
\usepackage{pgfplots}
\pgfplotsset{compat=1.7}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,angles,quotes}
\usetikzlibrary{fit}

\usepackage[hidelinks]{hyperref}

\usepackage{amsmath,amssymb,amsthm,amsfonts}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{claim}{Claim}
\newtheorem{problem}{Problem}
\newtheorem{protocol}{Protocol}
\renewcommand{\epsilon}{\varepsilon}

\usepackage{subcaption}

\usepackage{textcomp} % \textquotesingle

\usepackage{csquotes}

\usepackage[backend=biber,firstinits=true,doi=false,isbn=false,url=false]{biblatex}
\bibliography{../junk/refs/main.bib}

\providecommand{\keywords}[1]{\noindent\textbf{\textit{Index terms---}} #1}

\title{Task~Assignment in Crowdsourcing under Differential~Privacy}
\date{}
\author{%
  Louis B\'eziaud \\
  {\small \tt \url{louis.beziaud@ens-rennes.fr}}\\
  {Univ. Rennes 1, ENS Rennes}
  \and
  Tristan Allard \\
  {\small \tt \url{tristan.allard@irisa.fr}}\\
  {Univ. Rennes 1, IRISA}
  \and
  David Gross{-}Amblard\\
  {\small \tt \url{david.gross-amblard@irisa.fr}}\\
  {Univ. Rennes 1, IRISA}
}

%\overfullrule=5pt

\begin{document}

\maketitle

\begin{abstract}
  Crowdsourcing is a technique that engages individuals in the act of completing outsourced tasks. Crowdsourcing platforms propose more and more tasks requiring specific skills. However, current task assignment solutions requires that the participant discloses its skills to untrustworthy entities. In this paper, we introduce a framework to compute task assignments in a privacy-preserving way. We investigate multiples strategies based on the randomized response perturbation scheme to provide differential privacy.
\end{abstract}

\keywords{crowdsourcing, task~assignment, differential~privacy, randomized~response}

\section{Introduction}

Crowdsourcing is a technique to outsource tasks to a large number of unspecified workers. It is a popular solution for executing large amount of tasks that require human intelligence. One example of crowdsourcing is Galaxy Zoo\footnote{\url{https://www.galaxyzoo.org}}, where workers voluntarily classify large numbers of galaxies with their own eyes to contribute to the development of Science. There are also crowdsourcing services designed for general purpose such as Amazon Mechanical Turk\footnote{\url{https://www.mturk.com}} (with a pool of 500.000 workers) and CrowdFlower\footnote{\url{https://www.crowdflower.com}}, where workers get monetary rewards. These examples show that there exists a variety of crowdsourcing services that have different purposes and reward designs.

Many existing platforms are currently dealing with simple tasks, which do not require workers to have special skills. Recently, there has been growing interest in how to crowdsource skill-dependent tasks~\cite{MavridisGross-AmblardMiklos2016,RoyLykourentzouThirumuruganathanEtAl2015,BasuRoy2015}, which can be processed only by workers with specific skills. For example, an English-French translation task requires both English and French skills. Since skill-dependent tasks include a number of practical tasks, studies have been conducted to handle such tasks to be easily processed via crowdsourcing \cite{BasuRoy2015}.

One of the vital challenges a crowdsourcing platform specializing in skill-dependent tasks needs to overcome is to improve the throughput of the platform. The current practice of using open call assignments is not appropriate for this purpose in that a worker with a special skill may choose a simple task; thus a skill-dependent task requiring this worker's skill remains unassigned, which can be a sub-optimal assignment. An obvious approach to addressing this issue would be to compute optimal task assignment based on the skill sets of the workers and the skill requirements of the tasks. A platform with such knowledge would be able to globally maximize the throughput of its crowdsourcing system.

The privacy issues affecting both the workers and requesters in the task assignment strategy has been pointed out by a study~\cite{LeaseHullmanBighamEtAl2013} on the crowdsourcing platform Amazon Mechanical Turk where a worker ID can be linked to the real name, book reviews, or ``wish-list'' of a worker. Since workers are requested to report their skills such as language abilities and programming skills as well as their attributes such as their locations, minimum wages, and working hours, and they may even have to disclose additional information of a more personal nature. These skills can be used to identify the workers, infer sensitive information about them, expose them to physical danger by revealing their location information, and introduce unfairness by excessively favoring highly skilled and hardworking workers. Furthermore, the privacy of requesters will also be compromised. Requesters have to report the corresponding skill requirements, which can be used to identify them and to reveal the contents of their tasks. This indicates the need for the development of a task assignment system for general tasks in which the privacy of both workers and requesters is preserved.

The rest of the paper is organized as follows. Section \ref{sec:related} discusses related work. Section \ref{sec:prelim} formally presents the definitions used in the protocols instantiations introduced in Section \ref{sec:contrib}. Section \ref{sec:eval} provides the results of the evaluation. We conclude in Section \ref{sec:disc}.

\section{Related Work}\label{sec:related}

Although several previous studies have investigated privacy issues surrounding task assignments, their applicability is limited either because they focus on location-based tasks or other specific context, or they require unrealistic cryptographic protocol. 

A part of our objective share similarities with \textit{privacy-preserving recommender system}~\cite{McSherryMironov2009} but there is fundamental differences such as the dynamicity of tasks opposed to the staticity of items.

Differential privacy has been used to build a \textit{privacy-aware framework for spatial crowdsourcing}~\cite{ToGhinitaShahabi2014}, which enables the participation of workers without compromising their location privacy. The location is generalized according to a spatial grid obtained through spatial decomposition. Another similar work is a \textit{platform for crowdsourcing surveys in a privacy conscious way}~\cite{KandappuSivaramanFriedmanEtAl2014} where workers can answer surveys without risking to be identified. These works focus on the privacy of workers only and are not skills-based.

A \textit{private task assignment protocol}~\cite{Kajino2016} was proposed which produce an optimal task assignment while respecting the privacy of workers and requester. However, the protocol is executed inside a Pallier cryptosystem~\cite{Paillier1999} to avoid any privacy leak. Thus, the utility-privacy trade-off is avoided but the homomorphic encryption cause the method to be unusable in real-life. A theoretical run-time analysis shows that more than a century would be needed to compute an assignment between 100 workers and tasks.

\section{Preliminaries}\label{sec:prelim}

In this section we introduce notation for crowdsourcing and reviews basic privacy definitions.

\subsection{Crowdsourcing Problem}

\paragraph{Model} A requester uses crowdsourcing to have a task processed, which consists of a job instruction and a list of required skills. Assume for simplicity that each requester submits one task, i.e., each task is associated with one requester. \\
Let $\mathcal{T} = \{t_1, t_2, \dots, t_n\}$ be a set of tasks, where $t_i$ is a task ID. We abuse the notation to represent the requester who has task $t_i$ as requester $t_i$ . We further assume that requester $t_i$ has a requirement vector $r_i \in \{0, 1\}^D$ ($D \geq 1$). The $d$-th dimension of $r_i$ indicates whether task $t_i$ requires skill $d$ for completion ($r_{i,d}= 1$) or not ($r_{i,d} = 0$). \\
A worker performs the assigned tasks in exchange for reward. Let $\mathcal{W} = \{w_1, w_2, \dots, w_n\}$ be a set of workers, where $w_j$ is a worker ID. Assume that each worker $w_j$ is associated with a skill vector $s_j \in \{0, 1\}^D$. The $d$-th element of $s_j$ indicates whether worker $w_j$ has skill $d$ ($s_{j,d} = 1$) or not ($s_{j,d} = 0$). Note that requirement and skill vectors share the semantics of each dimension.

\paragraph{Objective} We make the assumption that $\mathcal{T} = \mathcal{W} = n$ without loss of generality. Given a set $\mathcal{T}$ of tasks and a set $\mathcal{W}$ of workers, the objective of the crowdsourcing platform is to perform worker-to-task assignment for all tasks in $\mathcal{T}$, such that the overall task quality is maximized and the cost is minimized. Assuming \(|\mathcal{T}| = |\mathcal{W}|\), the problem can be expressed as a standard linear problem~\cite{NAV:NAV3800020109}.

\begin{problem}[Assignment problem]
  Given two sets, \(A\) and \(T\), of equal size, together with a weight function \(C\colon A\times T\to\mathbb{R}\), the assignment problem is to find a bijection \(f\colon A\to T\) such that the cost function \(\sum_{a\in A}C(a,f(a))\) is minimized.
\end{problem}

We use the Hungarian algorithm~\cite{NAV:NAV3800020109} to compute an optimal assignment. It was developed and published in 1955 by Harold Kuhn and achieve an \(\mathcal{O}(n^3)\) running time.

\subsection{Differential-Privacy}

Differential privacy~\cite{DBLP:conf/icalp/Dwork06}, surveyed in \cite{Dwork2008}, is a relatively recent privacy definition based on the principle that the output of a computation should not allow inference about any record's presence in or absence from the computation's input. It ensure that the outcome of any analysis is essentially equally likely, independent of whether an individual joins, or refrains from joining, the dataset. Formally, it requires that for any outcome of a randomized computation, that outcome should be nearly equally likely with and without any one record.

\begin{definition}[Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}]
  A randomized mechanism \(M\) gives \(\epsilon\)-differential privacy if for all data sets \(D\) and \(D'\) differing at most one row, and all set of outcomes \(S \subseteq Range(M)\),
  \begin{equation}
    Pr[M(D) \in S] \leq  e^\epsilon \times Pr[M(D') \in S] .
  \end{equation}
\end{definition}

When \(A\) and \(B\) are adjacent, differential privacy bounds the update to the prior by a factor of $\exp(\epsilon)$, limiting the degree of inference possible about slight differences in the input data sets. Specifically, inference about the presence or absence (and consequently the value of) any single record is bounded by a factor of $\exp(\epsilon)$.

There are several useful properties that make  differential privacy a robust definition. For instance, differential privacy composes gracefully.

\begin{theorem}[Sequential Composition~\cite{DBLP:conf/sigmod/McSherry09}]
  Let \(M_i\) be a randomized mechanism which provides \(\epsilon_i\)-differential privacy. Then, a sequence of \(M_i(D)\) over the database \(D\) provides \((\sum_i \epsilon_i)\)-differential privacy.
\end{theorem}

\begin{theorem}[Parallel Composition~\cite{DBLP:conf/sigmod/McSherry09}]
  Let \(M_i\) be a randomized mechanism which provides \(\epsilon_i\)-differential privacy. Then, a sequence of \(M_i(D)\) over a disjoint subsets of database \(D\) provides \((\max_i \epsilon_i)\)-differential privacy.
\end{theorem}

A differentially private mechanism can be composed with any other mechanism (providing that it does not add information) and still be differentially private.

\begin{theorem}[Post-Processing~\cite{DworkRoth2014}]
  Let \(M\) be a randomized mechanism which provides \(\epsilon\)-differential privacy. Let $f$ be an arbitrary randomized mapping. Then $f \circ M$ provides \(\epsilon\)-differential privacy.
\end{theorem}

\subsection{Mechanisms}

\paragraph{Laplace Mechanism}

A common mechanism to ensure differential privacy is the Laplace mechanism. \(\epsilon\)-differential privacy is achieved by the addition of random noise whose magnitude is chosen as a function of the largest change a single participant could have on the output to the query function; this quantity is refereed as the sensitivity of the function.

\begin{definition}[Laplace Mechanism~\cite{dwork2016noise}]
  Let \(f(D)\) denote a function on \(D\) that outputs a vector in \(\mathbb{R}^d\). The Laplace mechanism \(\mathcal{L}\) is defined as \(\mathcal{L}(D) = f(D) + z\), where \(z\) is a \(d\)-length vector of random variables such that \(z_i\sim Laplace(\Delta f / \epsilon)\).

  The constant \(\Delta f\) is called the sensibility of \(f\) and is the maximum difference in \(f\) between any two databases that differ only by a single record, \(\Delta f = \max_{D, D'}\| f(D) - f(D')\|_1\).
\end{definition}

\paragraph{Randomized Response}

\begin{wrapfigure}{l}{0.5\textwidth}
%\begin{figure}[ht]
  \centering
  \resizebox{0.5\textwidth}{!}{%
      \begin{tikzpicture}
        \begin{axis}[
            axis x line=left,
            axis y line=left,
            xmode=log,
            clip=true,
            xmajorgrids=true,
            ymajorgrids=true,
            ylabel={Flipping probability},
            xlabel={Privacy budget},
            ymin=0,
            ymax=0.5,
          ]
          \addplot+[draw=black,mark=none,samples=500,domain=0.001:5,] {1/(exp(x)+1)};
          \draw [thick,decoration={brace,raise=7pt},decorate] (axis cs:0.001,0) -- node[above=9pt] {common \(\epsilon\)} (axis cs:0.1,0);
          \legend{$y=$};
        \end{axis}
      \end{tikzpicture}
  }
  \caption{Privacy budget \(\epsilon\) with respect to the flipping probability \(p = p_{01} = p_{10}\).}
  \label{fig:prob_eps}
\end{wrapfigure}


Randomize response~\cite{Warner1965} is an anonymization mechanism proposed by Warner in 1965 and later modified by Greenberg in 1969. It allows respondents to respond to sensitive issues while maintaining confidentiality.

Suppose a individual has a private binary value \(x \in \{0,1\}\) regarding a sensitive binary attribute \(X\). To ensure privacy, the individual sends to the untrusted server a modified version \(y\) of \(x\). Using the randomized response, the server can collect perturbed data from the individual.

A randomized response scheme on a binary attribute \(X\) follows a \(2\times 2\) design matrix: \begin{equation} \label{eq:design_matrix}
  \mathbf{P} = \begin{pmatrix}
    p_{00} & p_{01} \\
    p_{10} & p_{11}
  \end{pmatrix}
\end{equation}
where \(p_{uv} = P[y = u | x = v]\) (\(u,v \in \{0,1\}\)) denotes the probability that the random output is \(u\) when the real attribute value \(x\) is \(v\); here \(p_{uv}\in (0,1)\). In the design matrix, the sum of probabilities of each column is 1.

\begin{claim} For a given differential privacy parameter \(\epsilon\). the randomized response scheme following the design matrix \(\mathbf{P}\) in equation~\ref{eq:design_matrix} satisfies \(\epsilon\)-differential privacy if \(\max\{\frac{p_{00}}{p_{01}}, \frac{p_{11}}{p_{10}}\}\leq e^\epsilon)\).

  In order to maximize \(p_{00} + p_{11}\) while satisfying \(\epsilon\)-differential privacy, the design matrix should have the following pattern, \begin{equation} \label{eq:max_design_matrix}
    \mathbf{P}_{rr} = \begin{pmatrix}
      \frac{e^\epsilon}{1 + e^\epsilon} & \frac{1}{1 + e^\epsilon} \\
      \frac{1}{1 + e^\epsilon} & \frac{e^\epsilon}{1 + e^\epsilon}
    \end{pmatrix}
  \end{equation}
\end{claim}

\begin{proof}
  Assume \(\frac{p_{00}}{p_{01}} = p\), \(\frac{p_{11}}{p_{10}} = q\). In order to satisfy \(\epsilon\)-differential privacy, we have \(1 < p \leq e^\epsilon\) and \(1 < q \leq e^\epsilon\). In this case, the distortion matrix will have the general form: \begin{equation}
    \mathbf{P}_{rr} = \begin{pmatrix}
      \frac{p(q-1)}{pq-1} && \frac{q-1}{pq-1} \\
      \frac{p-1}{pq-1} && \frac{(p-1)q}{pq-1}
    \end{pmatrix}
  \end{equation}
  We denote \(func(p,q) = \mathbf{P}_{rr}(1,1) + \mathbf{P}_{rr}(2,2) = \frac{p(q-1)}{pq-1} + \frac{(p-1)q}{pq-1}\). Since \(\frac{\partial func}{\partial p} = \frac{(q-1)^2}{(pq-1)^2} > 0\) and \(\frac{\partial func}{\partial q} = \frac{(p-1)^2}{(pq-1)^2} > 0\) when \(p,q \in (1, e^\epsilon]\), thus \(func\) will achieve maximum value if and only if \(p=q=e^\epsilon\). In this way, we get the form in equation~\ref{eq:max_design_matrix}.
\end{proof}
  
We will refer to \(p = p_{01} = p_{10}\) as the flipping probability. Figure \ref{fig:prob_eps} illustrates the privacy budget according to \(p\). 

\section{Contribution}\label{sec:contrib}

In this section we first argue that the randomized response mechanism is the most suitable scheme with respect to our crowdsourcing specification, then we propose an overview of the global protocol for crowdsourcing, and finally we suggest three privacy-preserving task-assignment protocols.

\subsection{Mechanism}

The randomized response mechanism is based on two basic operations, thus it does not produce additional cost. It is designed for binary values such as the possession of a skill. Furthermore, it is a client-side perturbation mechanism which allow the user (worker or tasker) to anonymize its data without trusting the platform.

\subsection{Scenario}\label{sec:scenario}

The overall scenario that our approaches will follow is straightforward and is close to a non-private task assignment. It is abstracted on figure \ref{fig:scenario}. Workers and tasks send a perturbed version of their profile which is used by the platform to compute the task assignment. The resulting assignment is then privately published to the participants. This last step is not in the scope of this paper but a possible approach could use an anonymous network. For example, each participant's ID can be used to generate a temporary web page on a Tor node\footnote{\url{https://www.torproject.org/}} containing the contact information needed to establish a contact with the assigned participant. The worker-task participant could be kept private and secure with a specific communication suite such as GnuPG\footnote{\url{https://www.gnupg.org/}}.

\begin{figure*}[ht]
  \centering
  \includegraphics[width=1\linewidth]{figs/scenario.eps}
  \caption{Privacy-preserving task assignment flow diagram.}
  \label{fig:scenario}
\end{figure*}

\subsection{\sc Flip}

The {\sc Flip} algorithm applies the randomized response mechanism to a skills profile. We give an example of the anonymization process. Consider a worker with skills profile \(\langle 1, 0, 1, 0 \rangle\). The flipping process could transform it into the vector \(\langle 1, 1, 1, 0\rangle\). The result of the {\sc Flip} algorithm depends of the user's privacy budget.

\subsection{FlipMatch}

We choose the Hamming distance as a base distance since it is a common choice to quantify the divergence between two binary vectors such as our skills profiles.

\begin{definition}[Hamming distance~\cite{6772729}]
  The Hamming distance \(dist_{Ha}(u, v)\) between the binary vectors \(u=(u_1,u_2,\dots,u_n)\) and \(v=(v_1,v_2,\dots,v_n)\) is the number of indices \(i\) such that \(1\leq i\leq n\) and \(u_i\neq v_i\) divided by the length \(n\).
\end{definition}

There is two drawbacks to the Hamming distance, that is (i) large skills profiles will reduce the impact of the differences (because of the division by the length), and (ii) it does not reflect the semantic of a ``requirement''. We propose the following distance which only take into account the number of skills that a worker miss regarding of the ones required by a task.

\begin{definition}[MissingSkill distance]
  The MissingSkill distance \(dist_{MS}(w, t)\) between a worker's skill vector \(s=(s_1,s_2,\dots,s_n)\) and a task's requirement vector \(r=(r_1,r_2,\dots,r_n)\) is the number of indices \(i\) such that \(s_i = 0 \land t_i = 1\) divided by the number of indices \(j\) such that \(t_j = 1\).
\end{definition}

\begin{figure}[ht]
  \centering
  \begin{minipage}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{flipmatch.pdf}
    \caption{FlipMatch protocol.}
    \label{fig:flipmatch}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{groupmatch.pdf}
    \caption{GroupMatch protocol.}
    \label{fig:groupmatch}
  \end{minipage}
\end{figure}

This first protocol is a straightforward utilization of the randomized response. Each participant anonymize its skills profile using the Flip algorithm before sending it to the platform which computes a task assignment. The figure \ref{fig:flipmatch} is a diagram of the interactions between the entities.

\begin{protocol}[\sc FlipMatch]\hfill
\begin{enumerate}
\item Each worker (\(w_j\in\mathcal{W}\)) computes its flipped skill vector \(s_j' = \textsc{Flip}(s_j)\). Each task (\(t_i\in\mathcal{T}\)) computes its flipped requirement vector \(r_i' = \textsc{Flip}(r_i)\).
\item Each worker (\(w_i\)) and task (\(t_i\)) sends its flipped skills profile to the platform (\(P\)).
\item The platform (\(P\)) computes a task assignment using the {\sc Hungarian algorithm}.
\item The platform (\(P\)) publish the task assignment as described in the section \ref{sec:scenario}.
\end{enumerate}
\end{protocol}

\subsection{GroupMatch}

\begin{wrapfigure}[9]{r}{0.4\linewidth}
%\begin{figure}[h]
  \centering
  \resizebox{\linewidth}{!}{%
    \begin{tikzpicture}[taxo/.style = {shape=rectangle,rounded corners,draw,align=center,top color=white,bottom color=black!10},auto,level 1/.style={sibling distance=8em},level 2/.style={sibling distance=4em}]]
  \node[taxo] {$\perp$}
  child { node[taxo] (Language) {Language}
    child { node[taxo] (French) {French} }
    child { node[taxo] (English) {English} }
  }
  child { node[taxo] (Programming) {Programming}
    child { node[taxo] (Caml) {Caml} }
    child { node[taxo] (Haskell) {Haskell} }
  };
  \node [align=left,anchor=west] at (-5.2, -1.5) (superskills) {\bf super-skills};
  \node [align=left,anchor=west] at (-5.2, -3) (subskills) {\bf sub-skills}; 
  \node [draw,dashed, fit=(subskills) (French) (English) (Caml) (Haskell), inner sep=0.15cm] (subskills) {};
  \node [draw, dashed, fit=(superskills) (Language) (Programming), inner sep=0.15cm] (superskills) {};
    \end{tikzpicture}
    }
      \caption{Example of skills taxonomy.}
      \label{fig:taxonomy}
%\end{figure}
\end{wrapfigure}

This second protocol uses an underlying taxonomy to group related skills and reduce the noise effect. Similar nodes belong to the same group. The figure \ref{fig:groupmatch} is a diagram of the interactions between the entities.

For example, consider the skill hierarchy represented on figure \ref{fig:taxonomy}. ``Caml'' and ``Haskell'' are both programming languages and thus are grouped under the ``Programming'' super-skill.

Each participant anonymize its skills profile using the Flip algorithm and groups the result according to the hierarchy. This last step is the key allowing to reduce the effect of noise addition on the information. 

\begin{protocol}[\sc GroupMatch]\hfill
\begin{enumerate}
\item Each participant groups its skills: each super-skill is set to 1 if at least one of the sub-skills is possessed by the participant, or to 0 if the participant has none of the skills. For example, a participant having skills profile \(\{French, English\}\) will have the grouped profile \(\{1, 0\}\).
\item Each participant computes the flipped version of its grouped profile and sends it to the platform (\(P\)).
\item The platform (\(P\)) computes a task assignment using the {\sc Hungarian algorithm}.
\item The platform (\(P\)) publish the task assignment as described in the section \ref{sec:scenario}.
\end{enumerate}
\end{protocol}

\subsection{ClusterMatch}

We propose another protocol which uses clusters to group similar participants. We will need to compare distributions of skills to compute the clusters. The Jensen--Shannon distance is a common choice.

\begin{definition}[Kullback--Leibler divergence]
  The Kullback--Leibler (KL) divergence is a measure of how different two probability distributions (over the same event space) are. The KL divergence \(dist_{KL}\) of the probability distributions \(P\), \(Q\) on a finite set \(X\) is defined as \begin{equation}
    div_{KL}(P \| Q) = \sum_{x\in X} P(x) \log\frac{P(x)}{Q(x)}.
  \end{equation}
\end{definition}

\begin{definition}[Jensen--Shannon divergence]
  The Jensen--Shannon divergence \(M_{+}^{1}(A)\times M_{+}^{1}(A)\rightarrow [0,\infty) M_{+}^{1}(A)\times M_{+}^{1}(A)\rightarrow [0,\infty)\) is a symmetrized and smoothed version of the Kullback–Leibler divergence. It is defined by \begin{equation}
      div_{JS}(P\parallel Q)={\frac {1}{2}}D(P\parallel M)+{\frac {1}{2}}D(Q\parallel M)
    \end{equation}
    where \(M={\frac {1}{2}}(P+Q)\).
\end{definition}

\begin{definition}[Jensen--Shannon distance]
  The Jensen--Shannon distance is a metric defined as the square root of the Jensen--Shannon divergence \(dist_{JS}(P\| Q) = \sqrt{div_{JS}(P \| Q)}\).
\end{definition}


\begin{protocol}[\sc ClusterMatch]\hfill

\begin{enumerate}
\item Each participant flips its skills profile, then groups it, before sending it to the platform (\(P\)).
\item The platform (\(P\)) clusterizes the set of workers and the set of tasks.
\item An assignment is computer between the clusters of tasks and workers.
\item Some technique is used to compute the assignment between workers and tasks belonging to a pair of clusters.
\end{enumerate}
  
Each participant builds its randomized and grouped skills profile as described in the previous section. The platform collects the vectors of workers (resp. tasks) and builds clusters of similar workers (resp. tasks) using the JSD. An assignment between clusters of workers and clusters of tasks is then computed.
\end{protocol}

%\begin{figure}[ht]
%%  \centering
%  \includegraphics[width=\linewidth]{4102cf8a.png}
%  \label{fig:4102cf8a}
%  \caption{ClustMatch}
%\end{figure}

\section{Evaluation}\label{sec:eval}

We define the assignment quality as the normalized cost of the assignment, that is \(\sum_{(w_j,t_i)\in\mathcal{A}}C(w_j, t_i)\). A quality of 0 (resp. 1) corresponds to a perfect (resp. worst) assignment.

\subsection{Data sets}

We evaluate our protocols on a synthetic data set where workers and tasks have 05\% of skills ({\tt bern05}), along with real data sets.
We are not aware of any crowdsourcing-specific data set. We chooses two famous recommender system data sets, that is Jester\footnote{\url{http://eigentaste.berkeley.edu/dataset/}} and MovieLens\footnote{\url{http://grouplens.org/datasets/movielens/}}.
We use the version 1.3 from the Jester data set ({\tt jester13}). It is a collection of ratings from 24938 users who have rated between 15 and 35 jokes. We use the jokes as skills. We consider that a user has a skill if the corresponding joke is rated. We use an arbitrary taxonomy on the dataset to group the skills.
The MovieLens dataset is a movie ratings data set. We use the genre of the movie as a skills hierarchy. We use the version 100K ({\tt ml100k}). It contains 100000 ratings from 943 users who have rated at least 20 movies on 1682 movies distributed among 19 genres. We use the movies as skills. We consider that a user has a skill if the corresponding movie is rated. The skills are grouped according to the genre of the movie.

\subsection{Implementation details}

Each task assignment is performed between a perturbed data set and the original dataset. This allow a null divergence when \(\epsilon\to\infty\). We note {\sc GroupMatch10} the {\sc GroupMatch} protocol using a taxonomy of 10 super-skills.

We use implementations from Kevin Stern\footnote{\url{https://github.com/KevinStern/software-and-algorithms}} for the Hungarian algorithm and from Apache Commons\footnote{\url{http://commons.apache.org/}} for the clustering algorithms and . The code was written in Java and compiled with the Java 8 (1.8.0.92) Oracle's compiler. All experiments are conducted on a Linux machine running Debian 8 (64-bit) with an Intel(R) Core(TM) i5-5300U CPU @ 2.30GHz with 16G of RAM.

We compare multiple clustering algorithms for the {\sc ClusterMatch} protocol.
\begin{itemize}
\item {\sc K-Means++}~\cite{Arthur:2007:KAC:1283383.1283494} is a variation of the {\sc K-Means} algorithm which provide a heuristic to choose the initial values (or ``seeds'').
\item {\sc Fuzzy C-Means}~\cite{dunn1973fuzzy} is a soft version of {\sc K-Means}, where each data point has a fuzzy degree of belonging to each cluster. 
\item {\sc DBSCAN}~\cite{Sander:1998:DCS:593419.593465} is a clustering algorithm based on density. It does not require one to specify the number of clusters in the data a priori, as opposed to {\sc K-Means}.
\item Hierarchical~\cite{Rokach2005} is a method of cluster analysis which seeks to build a hierarchy of clusters.
\end{itemize}

\subsection{Results}

The following figure illustrates the effect of the aggregation. We can also see that a a small privacy budget makes the task assignment no better than a random assignment. The variance between the results highlights the data-dependency of the task assignment.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{\linewidth}{!}{
      \begin{tikzpicture}
        \begin{axis}[
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/groupmatch10_bern05_n1000_m100.data};
          \addlegendentry{\sc GroupMatch10};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/flipmatch_bern05_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.047159999999999994) (524,0.047159999999999994)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{\tt bern05}
    \label{fig:bern05}
  \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{\linewidth}{!}{
      \begin{tikzpicture}
        \begin{axis}[
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/groupmatch10_jester13_n1000_m100.data};
          \addlegendentry{\sc GroupMatch10};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/flipmatch_jester13_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.014586666666666668) (524,0.014586666666666668)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{\tt jester13}
    \label{fig:jester13}
  \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
    \centering
    \resizebox{\linewidth}{!}{
      \begin{tikzpicture}
        \begin{axis}[
            %          ymax=0.5,
            xmode=log,
            ylabel={Divergence},
            xlabel={Privacy Budget},
            legend style={draw=none},
            legend cell align=left,
          ]
          \addplot[
            color=black,
            thick,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/groupmatch10_ml100k_n1000_m100.data};
          \addlegendentry{\sc GroupMatch};
          \addplot[
            color=red,
            thick,
            xshift=1pt,
            error bars/.cd,
            y dir = both,
            y explicit,
            error bar style={opacity=0.4},
          ] table [col sep=comma,x index=0,y index=1,y error minus index=2, y error plus index=3] {data/flipmatch_ml100k_n1000_m100.data};
          \addlegendentry{\sc FlipMatch};
          \addplot[color=black,dashed] coordinates {(1,0.27360000000000006) (524,0.27360000000000006)};
          \addlegendentry{\sc Random}
        \end{axis}
      \end{tikzpicture}
    }
    \caption{\tt ml100k}
    \label{fig:ml100k}
  \end{subfigure}
  \caption{Evaluation of {\sc FlipMatch} and {\sc GroupMatch}.}
\end{figure}

\begin{wrapfigure}[13]{r}{0.4\linewidth}
  \centering
  \resizebox{\linewidth}{!}{%
    \begin{tikzpicture}
      \begin{axis}[
          xtick=data,
          symbolic x coords={K-Means++,C-Means,DBSCAN,Hierarchical},
          ylabel=Divergence,
          enlargelimits=0.05,
          legend style={at={(0.5,-0.15)},anchor=north,legend columns=-1},
          ybar
        ]
        \addplot coordinates {(K-Means++,0.81) (C-Means,0.73) (DBSCAN,0.87) (Hierarchical,0.63)};
        \addplot coordinates {(K-Means++,0.79) (C-Means,0.68) (DBSCAN,0.77) (Hierarchical,0.53)};
        \legend{\(\epsilon=1\),\(\epsilon=100\)}
      \end{axis}
    \end{tikzpicture}
  }
  \caption{Evaluation of {\sc ClusterMatch} on the {\tt bern05} data set.}
\end{wrapfigure}

We evaluate the average divergence inside each cluster of the {\sc ClusterMatch} protocol. As we can observe on the following figure, there is a high dissimilarity between the skills of each cluster.

\section{Conclusion}\label{sec:disc}

We have proposed three protocols to compute a privacy-preserving task assignment in crowdsourcing under the strong condition of differential privacy. The {\sc GroupMatch} algorithm is much better than both the basic {\sc FlipMatch} algorithm and the random assignment, thanks to the aggregation scheme. The {\sc ClusterMatch} is not viable with the clustering algorithms used.

We showed a hudge trade-off between privacy and utility. However, the data-dependency make it less pronounced on some datasets. This property could lead to a new protocol, based on a random assignment when the data set structure allows it.

The elimination of the noise could be greatly improved by aggregating before adding the noise. Indeed, individual profiles quickly becomes random under the noise. Using cryptography to construct the groups would reduce this effect and thus improve the quality of the assignment.

We explored a large specter of clustering algorithms trying to reduce the noise impact while aggregating skills and have confirmed the observation that differential privacy algorithms data-dependent and hard to evaluate on the average case~\cite{Hay:2016:PED:2882903.2882931}.

We add that the absence of a crowdsourcing-specific metrics and the absence of a real dataset calls are a deadlock to deeper studies and call for better standards.

\printbibliography

%\appendix

%\section{Private Task Assignment}
%\label{app:kajino}

%{\color{Red} {\bf TODO:} PTA flow diagram, PTA net diagram, PTA algorithm, PTA time.}

\end{document}
